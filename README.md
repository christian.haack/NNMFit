NNMFit
======

Theano-based analysis framework for diffuse NuMu analyses

Installation
------------

Download and 
```python setup.py install```

First Steps
------------

The default- and development-case for this tool is the multiyear analysis of upgoing muon-neutrino tracks, see the [paper](http://iopscience.iop.org/article/10.3847/0004-637X/833/1/3/meta ) for more details. Everything is set up for this case. 

To run your own analysis, you need to adjust a few things in config files, which live in ```/resources/configs/```: 

In the file ```default.cfg```, change the entries _install_dir_, _caching_dir_ and _condor_dir_ to locations where you have write-access. This _main-config_ -file sets the overall paths and settings for your analysis. 

Secondly, change _output_dir_ and  _plot_dir_  in the file  ```/resources/configs/analysis_configs/asimov_singlePL.yaml```, which sets the more detailed aspects of your analysis (in this case: asimov dataset, IC86-2011 season MC and lifetime, PoissonLLH and usage of background fits which were performed beforehand). 

That's it! You can run your first fit with the script ```/NNMFit/analysis/run_fit.py```: Simply call ``` python run_fit.py path-to-configs/default.cfg --analysis_config path-to-configs/analysis_configs/asimov_singlePL.yaml -o my_first_fit.pickle ```.

