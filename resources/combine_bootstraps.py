#! /bin/env python
import cPickle as pickle
from argparse import ArgumentParser
import numpy as n

parser = ArgumentParser()
parser.add_argument("-i", nargs="+", required=True, dest="infiles")
parser.add_argument("-o", required=True, dest="outfile")
args = parser.parse_args()

all_rates = []

for f in args.infiles:
    rates = pickle.load(open(f))
    all_rates.append(rates)

all_errors = n.std(all_rates, axis=0)
print len(all_errors)

pickle.dump((None, all_errors), open(args.outfile, "w"))
