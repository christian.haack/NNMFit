# Calculates the differentials for Jacobian matrix wrt. particle
# production uncertainty.
#
# Author: Anatoli Fedynitch
# August 2017
#

#modified by Joeran Stettner, Feb 2019

import os, sys, gzip, bz2
import numpy as np
import cPickle as pickle

from scipy.interpolate import RectBivariateSpline
import argparse
import numpy as np

#import solver related modules
from MCEq.core import MCEqRun
from MCEq.misc import normalize_hadronic_model_name
from mceq_config import config, mceq_config_without

#import primary model choices
import CRFluxModels as pm

# Global Barr parameter table
# format (x_min, x_max, E_min, E_max) | x is x_lab= E_pi/E, E projectile-air interaction energy
barr = {
    'a': [(0.0, 0.5, 0.00, 8.0)],
    'b1': [(0.5, 1.0, 0.00, 8.0)],
    'b2': [(0.6, 1.0, 8.00, 15.0)],
    'c': [(0.2, 0.6, 8.00, 15.0)],
    'd1': [(0.0, 0.2, 8.00, 15.0)],
    'd2': [(0.0, 0.1, 15.0, 30.0)],
    'd3': [(0.1, 0.2, 15.0, 30.0)],
    'e': [(0.2, 0.6, 15.0, 30.0)],
    'f': [(0.6, 1.0, 15.0, 30.0)],
    'g': [(0.0, 0.1, 30.0, 1e11)],
    'h1': [(0.1, 1.0, 30.0, 500.)],
    'h2': [(0.1, 1.0, 500.0, 1e11)],
    'i': [(0.1, 1.0, 500.0, 1e11)],
    'w1': [(0.0, 1.0, 0.00, 8.0)],
    'w2': [(0.0, 1.0, 8.00, 15.0)],
    'w3': [(0.0, 0.1, 15.0, 30.0)],
    'w4': [(0.1, 0.2, 15.0, 30.0)],
    'w5': [(0.0, 0.1, 30.0, 500.)],
    'w6': [(0.0, 0.1, 500., 1e11)],
    'x': [(0.2, 1.0, 15.0, 30.0)],
    'y1': [(0.1, 1.0, 30.0, 500.)],
    'y2': [(0.1, 1.0, 500., 1e11)],
    'z': [(0.1, 1.0, 500., 1e11)],
    'ch_a': [(0.0, 0.1, 0., 1e11)],
    'ch_b': [(0.1, 1.0, 0., 1e11)],
    'ch_e': [(0.1, 1.0, 800., 1e11)],
}

def barr_unc(xmat, egrid, pname, value):
    """Implementation of hadronic uncertainties as in Barr et al. PRD 74 094009 (2006)

    The names of parameters are explained in Fig. 2 and Fig. 3 in the paper."""

    # Energy dependence
    u = lambda E, val, ethr, maxerr, expected_err: val*min(
        maxerr/expected_err,
        0.122/expected_err*np.log10(E / ethr)) if E > ethr else 0.

    modmat = np.ones_like(xmat)
    modmat[np.tril_indices(xmat.shape[0], -1)] = 0.

    for minx, maxx, mine, maxe in barr[pname]:
        eidcs = np.where((mine < egrid) & (egrid <= maxe))[0]
        for eidx in eidcs:
            xsel = np.where((xmat[:eidx + 1, eidx] >= minx) &
                            (xmat[:eidx + 1, eidx] <= maxx))[0]
            if not np.any(xsel):
                continue
            if pname in ['i', 'z']:
                modmat[xsel, eidx] += u(egrid[eidx], value, 500., 0.5, 0.122)
            elif pname in ['ch_e']:
                modmat[xsel, eidx] += u(egrid[eidx], value, 800., 0.3, 0.25)
            else:
                modmat[xsel, eidx] += value

    return modmat


def compute_abs_derivatives(mceq_run, pid, barr_param, zenith_list,
                            fs_ids=["conv_numu", "conv_antinumu"]):
    mceq_run.unset_mod_pprod(dont_fill=False)

    barr_pars = [p for p in barr if p.startswith(barr_param) and 'ch' not in p]
    print 'Parameters corresponding to selection', barr_pars
    dim_res = len(zenith_list), etr.shape[0]
    gs = mceq_run.get_solution

    flux_per_fsid = {}
    for fluxkey in fs_ids:
        flux_per_fsid[fluxkey] = np.zeros(dim_res)
    #numu, anumu = (np.zeros(dim_res), np.zeros(dim_res))

    for iz, zen_deg in enumerate(zenith_list):
        mceq_run.set_theta_deg(zen_deg)
        mceq_run.solve()
        for fluxkey in fs_ids:
            flux_per_fsid[fluxkey][iz] = gs(fluxkey, 0)[tr]
        #numu[iz] = gs(fs_ids[0], 0)[tr]
        #anumu[iz] = gs(fs_ids[1], 0)[tr]

    mceq_run.unset_mod_pprod(dont_fill=True)
    for p in barr_pars:
        mceq_run.set_mod_pprod(2212, pid, barr_unc, (p, delta))
        #mceq_run.y.print_mod_pprod()
    mceq_run._init_default_matrices(skip_D_matrix=True)

    #numu_up, anumu_up = (np.zeros(dim_res), np.zeros(dim_res))
    up_flux_per_fsid = {}
    for fluxkey in fs_ids:
        up_flux_per_fsid[fluxkey] = np.zeros(dim_res)
    for iz, zen_deg in enumerate(zenith_list):
        mceq_run.set_theta_deg(zen_deg)
        mceq_run.solve()
        for fluxkey in fs_ids:
            up_flux_per_fsid[fluxkey][iz] = gs(fluxkey, 0)[tr]
        #numu_up[iz] = gs(fs_ids[0], 0)[tr]
        #anumu_up[iz] = gs(fs_ids[1], 0)[tr]

    mceq_run.unset_mod_pprod(dont_fill=True)
    for p in barr_pars:
        mceq_run.set_mod_pprod(2212, pid, barr_unc, (p, -delta))
        #mceq_run.y.print_mod_pprod()
    mceq_run._init_default_matrices(skip_D_matrix=True)

    #numu_down, anumu_down = (np.zeros(dim_res), np.zeros(dim_res))
    down_flux_per_fsid = {}
    for fluxkey in fs_ids:
        down_flux_per_fsid[fluxkey] = np.zeros(dim_res)
    for iz, zen_deg in enumerate(zenith_list):
        mceq_run.set_theta_deg(zen_deg)
        mceq_run.solve()
        for fluxkey in fs_ids:
            down_flux_per_fsid[fluxkey][iz] = gs(fluxkey, 0)[tr]

    fd_derivative = lambda up, down: (up - down) / (2. * delta)

    grad_dict = {}
    for fluxkey in fs_ids:
        grad_dict[fluxkey] = fd_derivative(up_flux_per_fsid[fluxkey],
                                           down_flux_per_fsid[fluxkey])
    #dnumu = fd_derivative(numu_up, numu_down)
    #danumu = fd_derivative(anumu_up, anumu_down)
    #grad_dict = {fs_ids[0]: dnumu,
    #             fs_ids[1]: danumu}
    return grad_dict

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--outdir", "-o", help= "Directory" , required=True)
    parser.add_argument("--cosz_steps",  help= "steps in cosZen" , default=32)
    parser.add_argument("--dropEnergies",  help= "discard the last n Bins energy" ,
                        default=1, type=int)
    parser.add_argument("--interaction-model",  help= "hadr. int. model",
                        default='SIBYLL2.3c',
                        dest="interactions")
    args = parser.parse_args()

    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    interaction_model = normalize_hadronic_model_name(args.interactions)

    CRModel_string = "H4a"
    CRModel = (pm.HillasGaisser2012, CRModel_string)
    print 'Running with', CRModel, "##", interaction_model
    #idjob = 0  # int(os.path.expandvars('$SGE_TASK_ID')) - 1

    mceq_run = MCEqRun(
        #provide the string of the interaction model
        interaction_model=interaction_model,
        #primary cosmic ray flux model
        #support a tuple (primary model class (not instance!), arguments)
        primary_model=CRModel,
        # Zenith angle in degrees. 0=vertical, 90=horizontal
        theta_deg=0.,
        #GPU device id
        **config)

    # Some global settings. One can play around with them, but there
    # is currently no good reason why

    # Primary proton projectile (neutron is included automatically
    # vie isosping symmetries)
    p = 2212
    # The parameter delta for finite differences computation
    delta = 0.001
    # Energy grid will be truncated above this value (saves some
    # memory and interpolation speed, but not really needed, I think)
    E_tr = 1e9
    cosz_steps = args.cosz_steps
    zenith_grid = np.arccos(np.linspace(0.2,-1.,cosz_steps))
    theta_deg_grid = 180./np.pi*zenith_grid

    pidx = mceq_run.pdg2pref[2212].lidx()
    nidx = mceq_run.pdg2pref[2112].lidx()
    tr = np.where(mceq_run.e_grid < E_tr)
    etr = mceq_run.e_grid[tr]

    final_fluxes = ["conv_numu", "conv_antinumu", "pr_numu", "pr_antinumu"]
    #final_fluxes = ["conv_numu", "conv_antinumu"]
    density_profiles = [
                        #('CORSIKA', ('BK_USStd', None)),
                        ('MSIS00_IC',('SouthPole','January')),
                        #('MSIS00_IC',('SouthPole','February')),
                        #('MSIS00_IC',('SouthPole','March')),
                        #('MSIS00_IC',('SouthPole','April')),
                        #('MSIS00_IC',('SouthPole','May')),
                        #('MSIS00_IC',('SouthPole','June')),
                        ('MSIS00_IC',('SouthPole','July')),
                        #('MSIS00_IC',('SouthPole','August')),
                        #('MSIS00_IC',('SouthPole','September')),
                        #('MSIS00_IC',('SouthPole','October')),
                        #('MSIS00_IC',('SouthPole','November')),
                        #('MSIS00_IC',('SouthPole','December')),
                        ]
    result_per_density = {}
    for density in density_profiles:
        mceq_run.set_density_model(density)

        if density[1][1] is None:
            dens_str = density[0]+density[1][0]
        else:
            dens_str = density[0]+density[1][0]+density[1][1]
        result_per_density[dens_str] = {}
        # Some technical shortcuts
        #gs = mceq_run.get_solution
        # Barr variables related to pions
        #barr_pivars = ['h']
        barr_pivars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i']
        # Barr variables related to kaons
        barr_kvars = ['w', 'y', 'z']
        barr_chvars = ["ch_a", "ch_b"]

        ## loop over Pion vars and modify during MCEq run
        for bp in barr_pivars:
            result_per_density[dens_str][bp + "+"] = compute_abs_derivatives(
                mceq_run, 211, bp, theta_deg_grid, fs_ids = final_fluxes)
            result_per_density[dens_str][bp + "-"] = compute_abs_derivatives(
                mceq_run, -211, bp, theta_deg_grid, fs_ids = final_fluxes)
        ## loop over Kaon vars and modify during MCEq run
        for bp in barr_kvars:
            result_per_density[dens_str][bp + "+"] = compute_abs_derivatives(
                mceq_run, 321, bp, theta_deg_grid, fs_ids = final_fluxes)
            result_per_density[dens_str][bp + "-"] = compute_abs_derivatives(
                mceq_run, -321, bp, theta_deg_grid, fs_ids = final_fluxes)
        ## loop over charm vars and modify during MCEq run
        for bp in barr_chvars:
            result_per_density[dens_str][bp + "+"] = compute_abs_derivatives(
                mceq_run, 411, bp, theta_deg_grid, fs_ids = final_fluxes)
            result_per_density[dens_str][bp + "-"] = compute_abs_derivatives(
                mceq_run, -411, bp, theta_deg_grid, fs_ids = final_fluxes)


    #average over densities:
    averaged_grads = {}
    averaged_grad_splines = {}
    for bpar_key in result_per_density[dens_str].keys():
        averaged_grads[bpar_key] = {}
        averaged_grad_splines[bpar_key] = {}
        for flux_key in final_fluxes:
            averaged_grads[bpar_key][flux_key] = np.zeros_like(
                result_per_density[dens_str][bpar_key][flux_key])
            for density in result_per_density.keys():
                averaged_grads[bpar_key][flux_key] +=\
                        result_per_density[density][bpar_key][flux_key]
            averaged_grads[bpar_key][flux_key] /= (1.*len(density_profiles))
    ## generate splines for later evaluation
    spline_kwargs = dict(kx=1, ky=1, s=0)#.2)
    for bpar_key in result_per_density[dens_str].keys():
        for flux_key in final_fluxes:
            averaged_grad_splines[bpar_key][flux_key] =\
                    RectBivariateSpline(theta_deg_grid,
                                        np.log10(etr),
                                        averaged_grads[bpar_key][flux_key],
                                        **spline_kwargs)
    pickle.dump(averaged_grad_splines,
                open(args.outdir+"/SA_superfast_gradients_PRI-{0}_INT-{1}.pickle".\
                     format(CRModel_string, interaction_model), "wc"),
                protocol=-1)Pion
