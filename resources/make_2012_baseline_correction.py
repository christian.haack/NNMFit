import os
from glob import glob
import dashi as d
import numpy as n
import scipy.special
import cPickle as pickle

from NNMFit.analysis_config import AnalysisConfig
from NNMFit import loaders
from NNMFit import Dataset
from NNMFit import nnm_logger

det_config = "IC86-2012-16"
config_hdl = AnalysisConfig("default.cfg",[det_config])
key_mapping, key_mapping_mc = config_hdl.get_key_mapping(det_config)
baseline_ds_path = glob(os.path.join(
    config_hdl[det_config]['baseline_dataset'],
    "*.hdf"))
if len(baseline_ds_path) != 1:
    raise RuntimeError("Could not find baseline dataset")
dataset_path = baseline_ds_path[0]
oversampling = None
dataset_mc_base = Dataset(dataset_path, key_mapping_mc,
                     oversampling, cache=None)

ds100_path = os.path.join(config_hdl[det_config]['datasets_path'], "DOM100")
ds_path_dom100 =  glob(os.path.join(ds100_path, "*.hdf"))
if len(ds_path_dom100) != 1:
    raise RuntimeError("Could not find baseline dataset")
dataset_mc_dom100 = Dataset(ds_path_dom100[0], key_mapping_mc,
                     oversampling, cache=None)

binning = config_hdl.make_binning(det_config)

models = config_hdl.get_fluxmodels()
fit_params = {}
for model in models:

    h_base = d.factory.hist1d(dataset_mc_base.reco_energy,
                              bins= binning[0][0],
                              weights=dataset_mc_base.weight(model))

    h_base_sys = d.factory.hist1d(dataset_mc_dom100.reco_energy,
                                  bins= binning[0][0],
                                  weights=dataset_mc_dom100.weight(model))

    ratio = d.histfuncs.histratio(h_base, h_base_sys)

    fit_mask = (ratio.x >= 1E2) & (ratio.x <= 1E4)
    f = lambda x,a,b,c,d: a * scipy.special.erfc(b*(x - c)) + d
    mod = d.model(f)
    fit = d.leastsq(n.log10(ratio.x[fit_mask]),
                    ratio.y[fit_mask],
                    mod,
                    error=ratio.yerr[fit_mask],
                    verbose=True)
    fit_params[model] = fit.params

bs_settings = "{}_baseline_correction".format(config_hdl[det_config]["name"])
pickle.dump(fit_params,
            open(config_hdl[bs_settings]["corr_file"], "w"))
