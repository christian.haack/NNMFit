import sys
from NNMFit import AnalysisConfig
from NNMFit import loaders
import numpy as n
from functools import reduce
import operator
from collections import defaultdict
import scipy
from tabulate import tabulate
det_config = sys.argv[1]

config = AnalysisConfig(
    ["default.cfg",
     "det_systematics.cfg",
     "IC59.cfg",
     "IC79.cfg",
     "IC86-2011.cfg",
     "IC86-2012-13-14.cfg"]
)
ds_tuple = loaders.load_data_for_config(config, det_config)

flux_models = config.parse_list(config["main"]["fluxmodels"])
ds_mc, ds_data, bin_edges, bin_centers, _, _, _ = ds_tuple

total_bins = reduce(operator.mul, [len(be)-1 for be in bin_edges], 1)

ts_values = n.empty(total_bins)
n_sim_events = n.empty_like(ts_values)
weights = ds_mc.weight(flux_models[0])
container = defaultdict(list)

if len(ds_mc.flattened_indices.shape) == 2:
    # Oversampling!
    for i in xrange(ds_mc.flattened_indices.shape[1]):
        for k in xrange(ds_mc.flattened_indices.shape[0]):
            container[ds_mc.flattened_indices[k, i]].append(weights[i])
else:
    for i in xrange(ds_mc.flattened_indices):
        container[ds_mc.flattened_indices[i]].append(weights[i])


for i in xrange(total_bins):
    data = container[i]
    std = n.std(data)
    if len(data) > 0:
        ts_values[i] = (max(data) - min(data))/std
    else:
        ts_values[i] = 0
    n_sim_events[i] = len(data)

binned_data = scipy.stats.binned_statistic_dd((ds_data.reco_energy,
                                               ds_data.reco_zenith,
                                               ds_data.reco_ra),
                                              None,
                                              "count",
                                              bins=bin_edges)[0]
ratio = n_sim_events / binned_data.ravel()
ratio[binned_data.ravel() == 0] = 0


critical_bins = (ratio < 10) & (binned_data.ravel() > 0)
if n.any(critical_bins):
    crtcl_nonzero = critical_bins.nonzero()[0]
    print "Found critical bins: "
    data = n.transpose([binned_data.ravel()[critical_bins],
                        n_sim_events[critical_bins]])
    print tabulate(data,
                   headers=["Data", "MC"])
