#!/bin/env python
##!/bin/sh /cvmfs/icecube.opensciencegrid.org/py2-v2/icetray-start
##METAPROJECT /data/user/chaack/software/icerec/V05-01-02-parasite/build

"""
Script for creating analysis hdf files
"""
from __future__ import print_function
import os
import logging
from collections import defaultdict
import argparse
from glob import glob
from NNMFit.data_handling.hdf_handler import HDFICData2
from NNMFit.analysis_config import AnalysisConfig, RawDatasetConfig
from NNMFit import nnm_logger
from icecube import astro
from icecube.weighting.weighting import from_simprod, NeutrinoGenerator
from icecube import NewNuFlux
from icecube.icetray import I3Units
import pandas as pd
import numpy as n
import yaml
import cPickle as pickle
logger = logging.getLogger(__name__)
parse_list = AnalysisConfig.parse_list

class SplineHandler(object):
    """
    Class implementing the flux weight calculation from a
    spline file created before
    (adjusted for MCEq)
    """

    IS_SYS_PARAM = False

    def __init__(self, spline_file, flux_keys, barr_key=None):
        self.spline_file = spline_file
        self.flux_keys = parse_list(flux_keys)
        self.barr_key = barr_key
        self.Ecut = 5e8 ## force highE weights to zero
        if self.barr_key is not None:
            self.spline_dict = pickle.load(open(spline_file,"r"))[self.barr_key]
            self.mag = 0
            self.spline_in_log = False
        else:
            self.spline_dict = pickle.load(open(spline_file,"r"))
            self.mag = self.spline_dict[0]["Emultiplier"]
            self.spline_in_log = True

        self._pid_dict = {"conv_numu" : 14,
                          "conv_antinumu" : -14,
                          "conv_nue" : 12,
                          "conv_antinue" : -12,
                          "conv_nutau" : 16,
                          "conv_antinutau" : -16,
                          "k_numu" : 14,
                          "k_antinumu" : -14,
                          "k_nue" : 12,
                          "k_antinue" : -12,
                          "pi_numu" : 14,
                          "pi_antinumu" : -14,
                          "pi_nue" : 12,
                          "pi_antinue" : -12,
                          "numu" : 14,
                          "antinumu" : -14,
                          "nue" : 12,
                          "antinue" : -12,
                          "conv_numuMSIS00_ICSouthPoleJanuary": 14,
                          "conv_antinumuMSIS00_ICSouthPoleJanuary": -14,
                          "conv_numuMSIS00_ICSouthPoleJuly": 14,
                          "conv_antinumuMSIS00_ICSouthPoleJuly": -14,

                          "pr_antinumu" : -14,
                          "pr_numu" : 14,
                          "pr_nue" : 12,
                          "pr_antinue" : -12,
                          "pr_antinutau" : -16,
                          "pr_nutau": 16}

    def resolve_pid(self, flux_key):
        return self._pid_dict[flux_key]

    def return_weight(self, pid_ints, energys, cosZs):
        """
        Return weight from spline. Correct for the E**mag factor that was
        applied during creation.

        Args: _particleID, coszenith, energy
        """

        theta_deg = 180./n.pi*n.arccos(cosZs)
        logenergy = n.log10(energys)
        weights = n.zeros_like(cosZs)
        logger.debug("Calculating MCEq weights from spline %s",
                     self.spline_file)

        for flux_key in self.flux_keys:
            pid_idcs = n.argwhere(pid_ints==self.resolve_pid(flux_key))
            if self.barr_key is None:
                weights[pid_idcs] = 10**self.spline_dict[1][flux_key](theta_deg[pid_idcs],
                                                                      logenergy[pid_idcs],
                                                                      grid=False)
            else:
                #special treatment for barr-splines (were built slightly diff.)
                weights[pid_idcs] = self.spline_dict[flux_key](
                    #n.cos(theta_deg[pid_idcs]),
                    theta_deg[pid_idcs],
                    logenergy[pid_idcs],
                    grid=False)
            ##hard fix to remove the highE madness of MCEq gradients
            print("Forcing atmospheric weights for super highE weights to zero for numerical stability..")
            weights[n.argwhere(logenergy>n.log10(self.Ecut))] = 0.
            ## check for NaN
            #print(" Found {} events with NaN weights.".format(len(weights[n.argwhere(n.isnan(weights))])))
            #weights[n.argwhere(n.isnan(weights))] = 0.

            ##correct for the E**mag factor from MCEq
            weights[pid_idcs] /= energys[pid_idcs]**self.mag
        return weights

# pylint: disable=too-many-locals, invalid-name
def calc_astro_coords(data, is_mc):
    """
    Calculate astro_coordinates for reco & mc_truth directions
    and insert into dataframe.

    Args:
        data: Dataframe object
        is_mc: True if data holds MC datasets
    """
    def insert_vars(new_vars, new_names, zen_var, repl, dframe):
        """
        Helper function for inserting the new astro vars into
        the dataframe

        The new variable new is obtained by replacing string `repl` in
        zen_var by the respective prefix in `new_names`.

        Args:
            new_vars: list of new arrays to insert
            new_names: list of name prefixes of new vars
            zen_var: name of the zenith column in dframe
            repl: substr in zen_var to replace
            dframe: Dataframe
        """
        for new_var, new_name in zip(new_vars, new_names):
            dframe[zen_var.replace(repl, new_name)] = new_var

    if not "mjd" in data:
        mjd = data["mjd_time_start_mjd_day"] +\
                data["mjd_time_start_mjd_sec"]/86400. +\
                data["mjd_time_start_mjd_ns"]*1e-9/86400.
        data["mjd"] = mjd

    zen_vars = [col for col in data.columns if "zenith" in col]
    azi_vars = [col for col in data.columns if "azimuth" in col]

    if is_mc:
        zen_vars.append("MCPrimaryZenith")
        azi_vars.append("MCPrimaryAzimuth")

    logger.info("Calculating coordinates")
    for zen_var, azi_var in zip(zen_vars, azi_vars):
        zen, azi = data[zen_var], data[azi_var]

        ra, dec = astro.dir_to_equa(zen, azi, data["mjd"])
        lon, lat = astro.equa_to_gal(ra, dec)

        if zen_var == "MCPrimaryZenith":
            zenith_repl = "Zenith"
            new_vars = ("RA", "Dec", "Lon", "Lat")
        else:
            zenith_repl = "zenith"
            new_vars = ("ra", "dec", "lon", "lat")

        insert_vars((ra, dec, lon, lat),
                    new_vars,
                    zen_var,
                    zenith_repl,
                    data)

def read_dataset(ds_config, feature_dict, feature_dict_mc):
    """
    Read hdf files belonging to dataset

    Args
    ----
    ds_config: dict
        Config section for this dataset
    feature_dict: dict
        Features common in MC and data to be extracted from hdf
    feature_dict_mc: dict
        Features that are MC only
    """
    is_mc = ds_config["type"] == "MC"
    f_dict = dict(feature_dict)
    if is_mc:
        f_dict.update(feature_dict_mc)

    if ds_config.getboolean("new_mjd_style",False):
        del f_dict["mjd_time_start_mjd_day"]
        del f_dict["mjd_time_start_mjd_ns"]
        del f_dict["mjd_time_start_mjd_sec"]
        f_dict["mjd"] = ("I3EventHeader.time_start_mjd", None)

    files = glob(ds_config["files"])
    data = None
    for f in sorted(files):
        logger.debug("File: %s", f)
        if os.stat(f).st_size < 3e3:
            logger.debug("Empty file. Skipping.")
            continue
        logger.debug("Reading hdf: %s", f)
        data += HDFICData2(f, f_dict, None)
    data = data.data_frame
    calc_astro_coords(data, is_mc)

    if is_mc:
        if "generator" in ds_config:
            # TODO: Remove hardcoding
            with open("configs/custom_generators.yaml") as hdl:
                custom_gens = yaml.load(hdl)
                generator= NeutrinoGenerator(
                    **custom_gens[ds_config["generator"]])*int(ds_config["nfiles_gen"])
        else:
            generator = from_simprod(int(ds_config["ds_num"])) *\
                    int(ds_config["nfiles_gen"])
    else:
        generator = float(ds_config["livetime"])

    return data, generator

def combine_datasets(data_dict):
    """
    Combine dataframes

    Args
    ----
    data_dict: dict
        Dict of tuples (dataframe, generator)
    """
    data_dict_comb = {}
    for key in data_dict:
        dfs = []
        gens = []
        ds_idents = []
        for dframe, gen, ds_ident in data_dict[key]:
            dfs.append(dframe)
            gens.append(gen)
            ds_idents.append(ds_ident)
        df_combined = pd.concat(dfs)
        if len(gens) > 1:
            gen_combined = sum(gens[1:], gens[0])
        else:
            gen_combined = gens[0]
        data_dict_comb[key] = df_combined, gen_combined, "_".join(ds_idents)
    return data_dict_comb

def apply_hacks(data_dict):
    for key, (dframe, _, _) in data_dict.iteritems():
        dframe.replace({"MCPrimaryType": {68: 14,
                                          69: -14,
                                          66: 12,
                                          67: -12,
                                          133: 16,
                                          134: -16}},
                          inplace=True)

# pylint: disable=too-many-locals
def calc_flux_weights(flux_config, data_dict_comb):
    """
    Calculate flux weights

    Args
    ----
    flux_config: dict
        Config section for flux
    data_dict_comb: dict
        Dict of combined Dataframes
    """
    flux_model = str(flux_config["flux"])
    if "powerlaw" in flux_model:
        const = float(flux_config["const"])
        index = float(flux_config["index"])
        flux = lambda _, E, __: const*E**(-index)
        flux_name = "powerlaw"
    elif "barr" in flux_model:
        flux_name = flux_model
        #barr_key = flux_config["barr_key"]
        barr_keys = parse_list(flux_config["barr_key"])
        flux_keys = flux_config["mceq_keys"]
        #spline_object = SplineHandler(flux_config["spline_file"],
        #                              flux_keys, barr_key=barr_key)
        spline_objects = {}
        for barr_key in barr_keys:
            spline_objects[barr_key] = SplineHandler(flux_config["spline_file"],
                                                     flux_keys,
                                                     barr_key=barr_key)
        def flux(ptype, energy, cosZ):
            tot_weights = 0
            for barr_key in barr_keys:
                tot_weights += spline_objects[barr_key].return_weight(ptype, energy, cosZ)
            return tot_weights
    elif "mceq" in flux_model:
        flux_name = flux_model
        flux_keys = flux_config["mceq_keys"]
        spline_object = SplineHandler(flux_config["spline_file"],
                                      flux_keys)
        flux = spline_object.return_weight
    else:
        flux = NewNuFlux.makeFlux(flux_model)
        flux.knee_reweighting_model = str(flux_config["knee"])
        flux = flux.getFlux
        flux_name = flux_model+"_"+flux_config["knee"]

    unit = I3Units.cm2/I3Units.m2
    for ident in data_dict_comb:
        dframe, gen, _ = data_dict_comb[ident]
        if "MCPrimaryType" in dframe:
            flux_w = flux(dframe["MCPrimaryType"].astype(n.int32),
                          dframe["MCPrimaryEnergy"].values,
                          n.cos(dframe["MCPrimaryZenith"].values))
            flux_w *= dframe["TIntProbW"] / unit
            flux_w /= gen(dframe["MCPrimaryEnergy"].values,
                          dframe["MCPrimaryType"].values,
                          n.cos(dframe["MCPrimaryZenith"]).values)
            dframe[flux_name] = flux_w

def main():
    """
    Main entry point
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--det-config", help="Detector Config",
                        dest="det_config", required=True)
    parser.add_argument("-t", "--tag", help="Dataset tag (ie. baseline)",
                        dest="tag", required=False, default=None)
    parser.add_argument("-o", "--outfile_name", help="Name additional to the dd\
                        scheme", required=False, default=None)


    args = parser.parse_args()
    raw_data_config = RawDatasetConfig()
    det_config = args.det_config
    feature_dict, feature_dict_mc =\
            raw_data_config.get_feature_dicts(det_config)
    data_dict = defaultdict(list)
    for dataset in raw_data_config.datasets(det_config):
        section = raw_data_config[dataset]
        idents = parse_list(section["identifier"])
        if args.tag is not None and not args.tag in idents:
            continue
        logger.info("Reading dataset %s", dataset)
        dframe, gen = read_dataset(section, feature_dict, feature_dict_mc)
        for ident in idents:
            data_dict[ident].append((dframe, gen, dataset))
    data_dict_comb = combine_datasets(data_dict)
    apply_hacks(data_dict_comb)
    del data_dict

    for _, flux_secs in raw_data_config["fluxes"].iteritems():
        for flux_sec in parse_list(flux_secs):
            section = raw_data_config[flux_sec]
            calc_flux_weights(section, data_dict_comb)
    for ident, dd_comb in data_dict_comb.iteritems():
        if args.outfile_name is not None:
            fname = "dataset_"+args.outfile_name+"_"+dd_comb[2]
        else:
            fname = "dataset_"+dd_comb[2]
        outdir = os.path.join(raw_data_config[det_config]["directory"],
                              ident)
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        out_file_name = os.path.join(outdir, fname+".hdf")
        dframe = data_dict_comb[ident][0]
        dframe.to_hdf(out_file_name, fname, mode="w", format="table",
                      complevel=5, complib="blosc:lz4")
        logger.info("Dataset written to %s", out_file_name)

if __name__ == "__main__":
    import time
    start_time = time.time()
    main()
    print("--- %s seconds ---" % (time.time() - start_time))
