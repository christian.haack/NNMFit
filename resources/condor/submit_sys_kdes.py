import os
import time
from itertools import product
import argparse
import pydag
from NNMFit import AnalysisConfig


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--config", help="main config",
                    dest="main_config", default="default.cfg")
parser.add_argument("-s", "--sys-type", help="Systematic type to be submitted",
                    dest="sys_type", default=None)
parser.add_argument("-f", "--flux", help="Flux identifier",
                    dest="flux", default=None)
parser.add_argument("-d", "--det", help="detector",
                    dest="det", default=None)
parser.add_argument("--only_combine", action="store_true", help="detector",
                    dest="only_combine")
args = parser.parse_args()
config = AnalysisConfig([args.main_config],
                       [args.det])

date_str = time.strftime("%d_%m_%Y_%H_%M_%S", time.gmtime())

condor_dir = os.path.join(config["condor"]["condor_dir"], date_str)
log_dir = os.path.join(condor_dir, "logs")
os.makedirs(log_dir)

script = os.path.join(config["main"]["install_dir"],
                      "resources/make_reco_kde.py")

dag_file = os.path.join(condor_dir, "submit.dag")
arguments = "--det_config $(CONFIG) --dataset $(DATASET) --flux $(FLUX) "\
        "$(BOOTSTRAP)"
submit_file = os.path.join(condor_dir, "job.sub")
submit_file_content = {"getenv": True,
                       "IWD": "$ENV(HOME)",
                       "universe": "vanilla",
                       "notification": "Error",
                       "log": "$(LOGFILE).log",
                       "output": "$(LOGFILE).out",
                       "error": "$(LOGFILE).err",
                       "request_memory": "3.5GB",
                       "request_gpus": "1",
                       "requirements": "CUDACapability",
                       "arguments": arguments}

submit_file = pydag.htcondor.HTCondorSubmit(submit_file,
                                            script,
                                            **submit_file_content)
submit_file.dump()

script = os.path.join(config["main"]["install_dir"],
                      "resources/combine_bootstraps.py")
submit_file_combine = os.path.join(condor_dir, "job_combine.sub")
arguments = "-i $(INFILE) -o $(OUTFILE)"
submit_file_content = {"getenv": True,
                       "IWD": "$ENV(HOME)",
                       "universe": "vanilla",
                       "notification": "Error",
                       "log": "$(LOGFILE).log",
                       "output": "$(LOGFILE).out",
                       "error": "$(LOGFILE).err",
                       "arguments": arguments}

submit_file_combine = pydag.htcondor.HTCondorSubmit(submit_file_combine,
                                            script,
                                            **submit_file_content)
submit_file_combine.dump()

fluxes = config.get_fluxmodels()
nodes = []

i = 0
dependencies = []
for cfg in [args.det]:#config["detector_configs"].keys():
    #det_cfg = config["detector_configs"][cfg]
    det_cfg = config[cfg]
    #if args.det is not None and args.det != det_cfg:
    #    continue
    sys_key = det_cfg["systematics"]
    out_path = os.path.join(config[sys_key]["systematics_path"],
                            "kde")
    for sys_type in config.parse_list(config[sys_key]["systematics"]):
        if args.sys_type is not None and args.sys_type != sys_type:
            continue
        #sys_datasets, _ = config.get_datasets_for_sys(det_cfg, sys_type)
        sys_datasets, _ = config.get_datasets_for_sys(cfg, sys_type)
        for (dataset, flux) in product(sys_datasets,
                                       fluxes):
            print flux
            if args.flux is not None and args.flux not in flux:
                continue
            log_file = os.path.join(log_dir, "{}_{}_{}".format(i,
                                                               dataset,
                                                               flux))
            dag_args = pydag.dagman.Macros(LOGFILE=log_file,
                                           DATASET=dataset,
                                           FLUX=flux,
                                           CONFIG=cfg,
                                           BOOTSTRAP="")

            node = pydag.dagman.DAGManNode(str(i), "job.sub")
            node.keywords["VARS"] = dag_args
            if not args.only_combine:
                nodes.append(node)
            i += 1

            bootstrap_nodes = []
            outnames = []
            for bootstrap_iter in xrange(10):
                log_file = os.path.join(log_dir, "{}_{}_{}_BS".format(
                    i, dataset, flux))
                bootstrap = "--bootstrap {}".format(bootstrap_iter)
                dag_args = pydag.dagman.Macros(LOGFILE=log_file,
                                               DATASET=dataset,
                                               FLUX=flux,
                                               CONFIG=cfg,
                                               BOOTSTRAP=bootstrap)
                node = pydag.dagman.DAGManNode(str(i), "job.sub")
                node.keywords["VARS"] = dag_args
                bootstrap_nodes.append(str(i))
                if not args.only_combine:
                    nodes.append(node)
                i += 1

                outnames.append(os.path.join(
                    out_path,
                    "reco_kde_bootstrap_eval_{}_{}_{}.pickle".format(
                        dataset, flux, bootstrap_iter)))


            log_file = os.path.join(log_dir, "{}_{}_{}_BS_COMBINE".format(
                i, dataset, flux))
            combined_bs_out = os.path.join(out_path,
                                          "reco_kde_bootstrap_eval_{}_{}.pickle".format(
                                          dataset, flux))
            dag_args = pydag.dagman.Macros(LOGFILE=log_file,
                                           INFILE=" ".join(outnames),
                                           OUTFILE=combined_bs_out)
            node = pydag.dagman.DAGManNode(str(i), "job_combine.sub")
            node.keywords["VARS"] = dag_args
            nodes.append(node)
            if not args.only_combine:
                dependencies.append((tuple(bootstrap_nodes), tuple([str(i)])))
            i += 1

dag = pydag.dagman.DAGManJob(dag_file, nodes)
for dep in dependencies:
    dag.add_dependency(*dep)
dag.dump()
print dag_file
