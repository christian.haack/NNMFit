"""Script to create the DAG for scanning two parameters"""
"""
Example call (2D Scan of the astro params:
python submit_llhscan.py --main_config Gen2_asimov.cfg --input_config
asimov_stdparams.cfg --override_config IC86-2012-13-14_scaled10a.cfg
--output_dir /data/user/jstettner/DiffuseExtensions/fitdata/AstroScan/Gen2
--param1 gamma_astro 1.5 2.7 6 --param2 astro_norm 0.1 2.5 7

"""
import os
import time
from itertools import product
import argparse
import pydag
import numpy as n
import yaml
from NNMFit import AnalysisConfig


parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output_dir", help="Directory for the output-file \
                    written by run_fit.py", dest="output_dir", required=True)
parser.add_argument("-s", "--scan", help="Scan parameter", nargs=4,
                    action="append", required=True)
parser.add_argument("--each_scan_1d", help="Each scan in the list of par\
                    will be in 1d", default=False, action="store_true",
                    required=False)
parser.add_argument("--main_config", help="main_config file to be used in \
                    run_fit.py", default="default.cfg")
parser.add_argument("--analysis_config", help="analysis config", required=True)
parser.add_argument("--override_configs", help="override_configs",
                    required=False)
parser.add_argument("--fix_default", help="List parameters which will be fixed\
                    to their default value in the fit.", required=False,
                    nargs='*', dest="fixed_defaults")
parser.add_argument("--fix", help="Fix parameter to value", required=False,
                    nargs=2, action="append", dest="fix")
parser.add_argument("--do_bestfit", help="Perform additionaly the bestfit",
                    action="store_true")


args = parser.parse_args()
analysis_config = yaml.load(open(args.analysis_config))
config = AnalysisConfig([args.main_config],
                        analysis_config["detector_configs"])
date_str = time.strftime("%d_%m_%Y_%H_%M_%S", time.gmtime())

condor_dir = os.path.join(config["condor"]["condor_dir"], date_str)
log_dir = os.path.join(condor_dir, "logs")
os.makedirs(log_dir)

output_dir = args.output_dir
if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

script = os.path.join(config["main"]["install_dir"],
                      "NNMFit/analysis/run_fit.py")

dag_file = os.path.join(condor_dir, "submit.dag")
arguments = " $(CONFIG) $(FIX) -o $(OUT) "
arguments += "--analysis_config $(ANALYSISCONF) "
fix_str_global = " "

if args.override_configs is not None:
    arguments += "--override_configs $(OVERRIDE) "

if args.fix is not None:
    for param, fix_value in args.fix:
        fix_str_global+= " --fix {} {} ".format(param, fix_value)
        #arguments +=" --fix {} {} ".format(param, fix_value)
print arguments
if args.fixed_defaults is not None:
    det_configs = config.get_det_configs()
    comps = config.get_components()
    params, defaults, bounds, alignment = config.get_params_and_bounds(comps, det_configs)#, return_dict=True)
    for p in args.fixed_defaults:
        print "Fixing parameter {} to default value".format(p)
        try:
            arguments += " --fix {} {} ".format(p,defaults[p])
        except KeyError:
            raise KeyError('Parameter <<{}>> not enabled, choose from these params:\
                       {}'.format(p,defaults))

##copy the configs to the out-folder:
main_config_file = os.path.join(output_dir, 'main_config.cfg')
os.system("cp {} {} ".format(args.main_config, main_config_file))
analysis_config_file = os.path.join(output_dir, 'analysis_config.yaml')
os.system("cp {} {} ".format(args.analysis_config, analysis_config_file))
if args.override_configs is not None:
    override_file = os.path.join(output_dir, "override_config.cfg")
    os.system("cp {} {} ".format(args.override_configs, override_file))
else:
    override_file=None

submit_file = os.path.join(condor_dir, "job.sub")
submit_file_content = {"getenv": True,
                       "IWD": "$ENV(HOME)",
                       "universe": "vanilla",
                       "notification": "Error",
                       "log": "$(LOGFILE).log",
                       "output": "$(LOGFILE).out",
                       "error": "$(LOGFILE).err",
                       "request_memory": "5.0 GB", #"8.0 GB",
                       "arguments": arguments}

submit_file = pydag.htcondor.HTCondorSubmit(submit_file,
                                            script,
                                            **submit_file_content)
submit_file.dump()

nodes = []
dependencies = []

param_list = {}
for (par, plow, pup, pn) in args.scan:
    param_list[par] = n.linspace(float(plow), float(pup), int(pn))

if args.each_scan_1d:
    for ijob, param in enumerate(param_list.keys()):
        for jjob, par_value in enumerate(param_list[param]):
            fix_str = fix_str_global
            outfile = "FitRes_"
            fix_str += " --fix {} {} ".format(param, par_value)
            outfile += "{}_{}".format(param, int(par_value*10000))
            log_file = os.path.join(log_dir, "{}_{}_{}".format(param,ijob,jjob))
            outfile = os.path.join(output_dir, outfile+".pickle")
            dag_args = pydag.dagman.Macros(LOGFILE=log_file,
                                           CONFIG=main_config_file,
                                           ANALYSISCONF=analysis_config_file,
                                           FIX=fix_str,
                                           OUT=outfile,
                                           OVERRIDE=override_file)
            node = pydag.dagman.DAGManNode(str(ijob)+"_"+str(jjob), "job.sub")
            node.keywords["VARS"] = dag_args
            nodes.append(node)
else:
    for ijob, params in enumerate(product(*param_list.values())):
        fix_str = fix_str_global
        outfile = "FitRes_"
        for par, value in zip(param_list.keys(), params):
            fix_str += " --fix {} {} ".format(par, value)
            outfile += "{}_{}".format(par, int(value*100))
        log_file = os.path.join(
            log_dir, "{}_{}".format(ijob, "_".join(param_list.keys())))
        outfile = os.path.join(output_dir, outfile+".pickle")
        dag_args = pydag.dagman.Macros(LOGFILE=log_file,
                                       CONFIG=main_config_file,
                                       ANALYSISCONF=analysis_config_file,
                                       FIX=fix_str,
                                       OUT=outfile,
                                       OVERRIDE=override_file)
        node = pydag.dagman.DAGManNode(str(ijob), "job.sub")
        node.keywords["VARS"] = dag_args
        nodes.append(node)
if args.do_bestfit:
    #perform also a free fit:
    fix_str = fix_str_global
    outfile = "Freefit"
    log_file = os.path.join(log_dir, "Freefit")
    outfile = os.path.join(output_dir, outfile+".pickle")
    dag_args = pydag.dagman.Macros(LOGFILE=log_file,
                                       CONFIG=main_config_file,
                                       ANALYSISCONF=analysis_config_file,
                                       FIX=fix_str,
                                       OUT=outfile,
                                       OVERRIDE=override_file)
    node = pydag.dagman.DAGManNode(str(ijob+1), "job.sub")
    node.keywords["VARS"] = dag_args
    nodes.append(node)

dag = pydag.dagman.DAGManJob(dag_file, nodes)
dag.dump()
print dag
print condor_dir

