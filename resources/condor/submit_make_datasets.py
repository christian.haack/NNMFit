import os
import time
import argparse
import pydag
from NNMFit.analysis_config import (AnalysisConfig, RawDatasetConfig)


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--main-config", help="main config",
                    dest="main_config", default="default.cfg")
parser.add_argument("-d", "--det-config", help="detector config",
                    dest="det", default=None, required=True)
parser.add_argument("-t", "--tag", help="dataset identifier (ie. baseline)",
                    dest="tag", default=None)
args = parser.parse_args()

date_str = time.strftime("%d_%m_%Y_%H_%M_%S", time.gmtime())
config = AnalysisConfig([args.main_config], [args.det])
condor_dir = os.path.join(config["condor"]["condor_dir"], date_str)
log_dir = os.path.join(condor_dir, "logs")
os.makedirs(log_dir)
print "Condor dir: ",condor_dir
script = os.path.join(config["main"]["install_dir"],
                      "resources/make_datasets.py")

dag_file = os.path.join(condor_dir, "submit.dag")
arguments = "-d $(DETCONFIG) -t $(TAG) "
submit_file = os.path.join(condor_dir, "job.sub")
submit_file_content = {"getenv": True,
                       "IWD": os.path.join(config["main"]["install_dir"],
                                           "resources"),
                       "universe": "vanilla",
                       "notification": "Error",
                       "log": "$(LOGFILE).log",
                       "output": "$(LOGFILE).out",
                       "error": "$(LOGFILE).err",
                       "request_memory": "8GB",#"2.5GB",
                       "arguments": arguments}

submit_file = pydag.htcondor.HTCondorSubmit(submit_file,
                                            script,
                                            **submit_file_content)
submit_file.dump()

raw_data_config = RawDatasetConfig()
nodes = []

idents = raw_data_config.idents(args.det)
for i, ident in enumerate(idents):
    print i,ident
    if args.tag is not None and args.tag != ident:
        continue

    log_file = os.path.join(log_dir, "{}_{}".format(args.det,
                                                    ident))
    dag_args = pydag.dagman.Macros(LOGFILE=log_file,
                                   DETCONFIG=args.det,
                                   TAG=ident)
    node = pydag.dagman.DAGManNode(str(i), "job.sub")
    node.keywords["VARS"] = dag_args
    nodes.append(node)
dag = pydag.dagman.DAGManJob(dag_file, nodes)
dag.dump()
print dag_file
