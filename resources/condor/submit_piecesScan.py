"""Script to create the DAG for scanning two parameters"""
"""
Example call (2D Scan of the astro params:
python submit_llhscan.py --main_config Gen2_asimov.cfg --input_config
asimov_stdparams.cfg --override_config IC86-2012-13-14_scaled10a.cfg
--output_dir /data/user/jstettner/DiffuseExtensions/fitdata/AstroScan/Gen2
--param1 gamma_astro 1.5 2.7 6 --param2 astro_norm 0.1 2.5 7

"""
import os
import time
#from itertools import product
import argparse
import pydag
import numpy as n
from NNMFit import AnalysisConfig
import cPickle as pickle
import yaml

parser = argparse.ArgumentParser()
parser.add_argument("-o", "--output_dir", help="Directory for the output-file \
                    written by run_fit.py", dest="output_dir", required=True)
parser.add_argument("--main_config", help="main_config file to be used in \
                    run_fit.py", default="default.cfg")
parser.add_argument("--analysis_config", help="analysis config", required=True)
parser.add_argument("--override_config", help="override_config", required=False)
parser.add_argument("--nScanpoints", help="number of scanpoints", required=True)
parser.add_argument("--fix", help="Fix parameters", nargs=2,
                    action="append", dest="fix", required=False,
                    default=[])
parser.add_argument("--fix_default", help="List parameters which will be fixed\
                    to their default value in the fit.", required=False,
                    nargs='*', dest="fixed_defaults")
parser.add_argument("--bestfit_file", help="best-fit to extract piece-values",
                    required=True)
parser.add_argument("--fix_other_pieces", help="Perform full 1d scan or fix\
                    all other pieces during scan?",
                    action="store_true")
args = parser.parse_args()

analysis_config = yaml.load(open(args.analysis_config))
config = AnalysisConfig([args.main_config],
                        analysis_config["detector_configs"])


date_str = time.strftime("%d_%m_%Y_%H_%M_%S", time.gmtime())

condor_dir = os.path.join(config["condor"]["condor_dir"], date_str)
log_dir = os.path.join(condor_dir, "logs")
os.makedirs(log_dir)

output_dir = args.output_dir
if not os.path.isdir(output_dir):
    os.makedirs(output_dir)

script = os.path.join(config["main"]["install_dir"],
                      "NNMFit/analysis/run_fit.py")
pieces = yaml.load(open("/data/user/jstettner/DiffuseExtensions/"\
                        +"NNMFit/resources/configs/components.yaml"))\
        ["piecewise_astro"]["parameters"]
print pieces
#config.get_flux_params(["piecewise_astro"])[0]["piecewise_astro"].keys()
pieces_bf = {}
with open(args.bestfit_file,"r") as f:
    bestfit = pickle.load(f)
    for p in pieces:
        pieces_bf[p] = bestfit["fit-result"][1][p]

dag_file = os.path.join(condor_dir, "submit.dag")
arguments = " $(CONFIG) $(FIX1) -o $(OUT) --analysis_config $(ANALYSIS) "

if args.override_config is not None:
    arguments += "--override_configs $(OVERRIDE) "
if args.fixed_defaults is not None:
    det_configs = config.get_det_configs()
    comps = config.get_components()
    params, defaults, bounds, alignment = config.get_params_and_bounds(comps, det_configs)#, return_dict=True)
    for p in args.fixed_defaults:
        print "Fixing parameter {} to default value".format(p)
        try:
            arguments += " --fix {} {}".format(p,defaults[p])
        except KeyError:
            raise KeyError('Parameter <<{}>> not enabled, choose from these params:\
                       {}'.format(p,defaults))

submit_file = os.path.join(condor_dir, "job.sub")
submit_file_content = {"getenv": True,
                       "IWD": "$ENV(HOME)",
                       "universe": "vanilla",
                       "notification": "Error",
                       "log": "$(LOGFILE).log",
                       "output": "$(LOGFILE).out",
                       "error": "$(LOGFILE).err",
                       "request_memory": "5. GB", #"8.0 GB",
                       "arguments": arguments}

submit_file = pydag.htcondor.HTCondorSubmit(submit_file,
                                            script,
                                            **submit_file_content)
submit_file.dump()

nodes = []

i = 0
for piece in pieces:

    fixstr_rest = " "
    if args.fix is not None:
        for par_name, par_value in args.fix:
            print "Fixing: {} = {} ".format(par_name, par_value)
            fixstr_rest += " --fix {} {} ".format(par_name, par_value)

    print "Performing 1D scan of ", piece
    if args.fix_other_pieces:
        print "(other pieces fixed at bf)"
        for p in pieces:
            if p == piece:
                continue
            fixstr_rest += " --fix {} {} ".format(p,\
                                              bestfit["fit-result"][1][p])
    for p1 in n.linspace(max(0.0,pieces_bf[piece]-2.2), pieces_bf[piece]+3.6 , args.nScanpoints):
    #for p1 in n.linspace(max(0.0,pieces_bf[piece]-1.2), pieces_bf[piece]+1.3 , args.nScanpoints):
        log_file = os.path.join(log_dir, "{}_{}".format(i, piece))
        fix1str = fixstr_rest + " --fix {} {} ".format(piece, p1)
        outfile = "{}/FitRes_{}-{}.pickle".format(output_dir, piece, p1)
        dag_args = pydag.dagman.Macros(LOGFILE=log_file,
                                       CONFIG=args.main_config,
                                       ANALYSIS=args.analysis_config,
                                       FIX1=fix1str,
                                       OUT=outfile,
                                       OVERRIDE=args.override_config)
        node = pydag.dagman.DAGManNode(str(i), "job.sub")
        node.keywords["VARS"] = dag_args
        nodes.append(node)
        i += 1

dag = pydag.dagman.DAGManJob(dag_file, nodes)
dag.dump()

for name, item in zip(["analysis_config.yaml","bestfit_file.pickle",
                       "parameters_conf.yaml"],
                      [args.analysis_config, args.bestfit_file,
                       "/data/user/jstettner/DiffuseExtensions/NNMFit/resources/configs/parameters.yaml"]):
    os.system("cp {} {} ".format(item, output_dir+name))

print dag
print condor_dir


