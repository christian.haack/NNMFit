import os

from glob import glob
import dashi as d
import numpy as n
import scipy.special
import cPickle as pickle
from scipy import interpolate

#import matplotlib.pyplot as plt
from NNMFit.analysis_config import AnalysisConfig
from NNMFit import loaders
from NNMFit import Dataset
from NNMFit import nnm_logger
import yaml

plot_dir = "/home/jstettner/public_html/NNMFit/nutau_splines/"
det_configs = ["IC59"]
#"IC86_pass2_oldMC"]#"IC59", "IC86-2012-16"] # , "IC79", "IC86-2011", "IC86-2012-16"]
config_hdl = AnalysisConfig("default_IC86pass2_oldMC.cfg",
                            det_configs)


for det_config in det_configs:
    print det_config
    if det_config in ["IC79", "IC86-2011"]:
        print "No nutau datasets available"
        continue
    key_mapping, key_mapping_mc = config_hdl.get_key_mapping(det_config)

    baseline_ds_path = glob(os.path.join(
        config_hdl[det_config]['baseline_dataset'],"*.hdf"))
    if len(baseline_ds_path) != 1:
        raise RuntimeError("Could not find baseline dataset")
    dataset_path = baseline_ds_path[0]
    dataset_mc_base = Dataset(dataset_path, key_mapping_mc,
                              ra_oversampling=None, cache=None)

    nutau_path = os.path.join(config_hdl[det_config]['datasets_path'], "nu_tau")
    ds_path_nutau =  glob(os.path.join(nutau_path, "*.hdf"))
    if len(ds_path_nutau) != 1:
        raise RuntimeError("Could not find nutau dataset")
    dataset_mc_nutau = Dataset(ds_path_nutau[0], key_mapping_mc,
                               ra_oversampling=None, cache=None)

    bins, bincenters = config_hdl.make_binning(det_config)
    ## reduce the number of bins to make sure every bin is filled
    binsE = bins[0]
    binsZ = bins[1]

    binsE = n.logspace(n.log10(binsE[0]), n.log10(binsE[-1]), 16)
    if "IC86" in det_config:
        binsE = binsE[:-1]
    binsZ = n.linspace(binsZ[0], binsZ[1], 4)
    #if det_config in ["IC86-2012-16"]:
    #binning[0] = binning[0][:-1]

    models = config_hdl.get_fluxmodels()
    splines = {}
    for model in ["powerlaw"]:#models:
        print "Reweighting for model: ", model
        #h_base = d.factory.hist1d(dataset_mc_base.reco_energy,
        #                      bins=binsE,
        #                      weights=dataset_mc_base.weight(model))
        #h_base_sys = d.factory.hist1d(dataset_mc_nutau.reco_energy,
        #                          bins=binsE,
        #                          weights=dataset_mc_nutau.weight(model))
        #ratio_energy = d.histfuncs.histratio(h_base, h_base_sys)

        #h_base = d.factory.hist1d(dataset_mc_base.reco_zenith,
        #                      bins=binsZ,
        #                      weights=dataset_mc_base.weight(model))
        #h_base_sys = d.factory.hist1d(dataset_mc_nutau.reco_zenith,
        #                              bins= binsZ,
        #                              weights=dataset_mc_nutau.weight(model))
        #ratio_zenith = d.histfuncs.histratio(h_base, h_base_sys)

        h2d_base = d.factory.hist2d((dataset_mc_base.reco_zenith,
                                     dataset_mc_base.reco_energy),
                                     bins=(binsZ,binsE),
                                     weights = dataset_mc_base.weight(model))

        h2d_nutau = d.factory.hist2d((dataset_mc_nutau.reco_zenith,
                                      dataset_mc_nutau.reco_energy),
                                     bins=(binsZ,binsE),
                                     weights = dataset_mc_nutau.weight(model))
        if max(len(h2d_nutau.bincontent[h2d_nutau.bincontent==0]),
               len(h2d_base.bincontent[h2d_base.bincontent==0]))>0:
            #h2d_nutau.bincontent[h2d_nutau.bincontent==0]= 1.0
            #h2d_base.bincontent[h2d_base.bincontent==0]=1.0
            raise RuntimeError("The histograms contain empty bins. Exiting..")
        spline_kwargs = dict(kx=1, ky=3, s=0.2)
        spline_base = interpolate.RectBivariateSpline(h2d_base.bincenters[0],
                                                      h2d_base.bincenters[1],
                                                      n.log10(h2d_base.bincontent),
                                                      **spline_kwargs)
        spline_nutau = interpolate.RectBivariateSpline(h2d_nutau.bincenters[0],
                                                       h2d_nutau.bincenters[1],
                                                       n.log10(h2d_nutau.bincontent),
                                                      **spline_kwargs)

        #print 10**spline_base(h2d_base.bincenters[0], h2d_base.bincenters[1])/h2d_base.bincontent
        #print 10**spline_nutau(h2d_nutau.bincenters[0],h2d_nutau.bincenters[1])/h2d_nutau.bincontent

        spline_ratio = lambda x,y: 10**(spline_nutau(x,y)-spline_base(x,y))
        zen_fine = n.linspace(binsZ[0], binsZ[-1], 1001)
        E_fine = n.logspace(n.log10(binsE[0]), n.log10(binsE[-1]), 1001)

        splines[model] = interpolate.RectBivariateSpline(zen_fine,
                                                         E_fine,
                                                         spline_ratio(zen_fine,
                                                                      E_fine),
                                                         kx=1, ky=1, s=0)


    nutau_settings = "{}_nutau_correction".format(config_hdl[det_config]["name"])
    print "Dumping spline to file " , config_hdl[nutau_settings]["spline_file"]
    pickle.dump(splines,
                open(config_hdl[nutau_settings]["spline_file"], "w"))

