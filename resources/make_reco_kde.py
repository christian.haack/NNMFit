#! /bin/env python
import cPickle as pickle
import os
import sys
import argparse
import numpy as n
from NNMFit import AnalysisConfig, Dataset
from NNMFit import kde_tools

def make_bootstrap_kde(dataset_mc_baseline,
                       dataset_mc_sys,
                       flux,
                       niter,
                       alpha):

    kde_maker = kde_tools.make_kde(dataset_mc_sys.reco_energy,
                                   dataset_mc_sys.reco_zenith,
                                   dataset_mc_sys.weight(flux),
                                   bounds = (None, (-1, n.cos(n.radians(85)))),
                                   thresholds = (None, (-1.3, 0.487)))
    kde_maker.make_kernel(use_cuda=True, bootstrap=True, niter=niter,
                         alpha=alpha)
    xeval = [n.log10(dataset_mc_baseline.reco_energy),
             n.cos(dataset_mc_baseline.reco_zenith)]

    evaled = kde_maker.func_rate_density(n.asarray(xeval))
    return evaled


def make_and_eval_kde(dataset_mc_baseline,
                      dataset_mc_sys,
                      flux,
                      alpha):
    xeval = [n.log10(dataset_mc_baseline.reco_energy),
             n.cos(dataset_mc_baseline.reco_zenith)]
    kde_maker = kde_tools.make_kde(dataset_mc_sys.reco_energy,
                                   dataset_mc_sys.reco_zenith,
                                   dataset_mc_sys.weight(flux),
                                   bounds = (None, (-1, n.cos(n.radians(85)))),
                                   thresholds = (None, (-1.3, 0.487)))
                                   # bounds = (None, (None, None)),
                                   # thresholds = (None, (None, None)))

    kde_maker.make_kernel(use_cuda=True, bootstrap=False, alpha=alpha)
    evaled = kde_maker.func_rate_density(n.asarray(xeval))
    spline = kde_maker.make_spline(nsteps=100)
    return evaled, spline

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--main_config", help="main config",
                        dest="main_config", default="default.cfg")
    parser.add_argument("--dataset", help="Dataset used for KDE",
                        dest="dataset", required=True)
    parser.add_argument("--det_config", help="Detector config", type=str,
                        dest="det_config")
    parser.add_argument("--flux", help="Fluxmodel", dest="flux", required=False)
    parser.add_argument("--alpha", help="Adaptive KDE parameter", dest="alpha",
                       default=0.5, type=float)
    parser.add_argument("--bootstrap", help="Bootstrap", dest="bootstrap",
                        default=None)
    args = parser.parse_args()

    if ".cfg" in args.det_config:
        det_config = args.det_config.strip(".cfg")
    else:
        det_config = args.det_config

    config = AnalysisConfig([args.main_config],
                            [det_config])

    systematics_key = config[det_config]["systematics"]
    out_path = os.path.join(config[systematics_key]["systematics_path"],
                            "kde")
    key_mapping, key_mapping_mc = config.get_key_mapping(det_config)
    binning, bin_centers = config.make_binning(det_config)

    dataset_name = os.listdir(config[det_config]["baseline_dataset"])[0]
    inv_key_mapping = {v: k for k, v in key_mapping.iteritems()}
    cols_to_read = [inv_key_mapping["reco_energy"],
                    inv_key_mapping["reco_zenith"],
                    args.flux,
                    inv_key_mapping["reco_energy_exists"],
                    inv_key_mapping["reco_energy_fit_status"],
                    "reco_dir_fit_status",
                    "reco_dir_exists"]

    dataset_mc_baseline = Dataset(
        os.path.join(config[det_config]["baseline_dataset"],
                     dataset_name),
        key_mapping_mc,
        cols_to_read=cols_to_read)

    dataset_mc_baseline.set_standard_mask()
    # dataset_mc_baseline.set_energy_mask(binning)
    dataset_sys_path = os.path.join(config[det_config]["datasets_path"],
                                    args.dataset)
    dataset_sys = os.listdir(dataset_sys_path)[0]

    dataset_mc_sys = Dataset(
        os.path.join(dataset_sys_path, dataset_sys),
        key_mapping_mc,
        cols_to_read=cols_to_read)

    dataset_mc_sys.set_standard_mask()
    # dataset_mc_sys.set_energy_mask(binning)
    if args.bootstrap is not None:
        outname = "reco_kde_bootstrap_eval_{}_{}_{}.pickle".format(
            args.dataset,
            args.flux,
            args.bootstrap)
        if not os.path.exists(os.path.join(out_path, outname)):
            print("Building and evluating KDE (bootstrap)..")
            evaled, spline = make_bootstrap_kde(dataset_mc_baseline,
                                        dataset_mc_sys,
                                        args.flux,
                                        niter=1,
                                        alpha=args.alpha)
            print("Done. Pickling to ", out_path)
            pickle.dump(evaled, open(os.path.join(out_path, outname), "wb"),
                        protocol=2)
        else:
            print "Skipping, sys reco kde file exists already"
    else:
        outname = "reco_kde_eval_{}_{}.npz".format(args.dataset, args.flux)
        if not os.path.exists(os.path.join(out_path, outname)):
            print("Building and evluating KDE..")
            evaled, spline = make_and_eval_kde(dataset_mc_baseline,
                                               dataset_mc_sys,
                                               args.flux,
                                               alpha=args.alpha)
            if n.all(n.isnan(evaled)):
                raise RuntimeError("All of the evaluated points are nan. KDE is\
                                   fucked")
            n.savez_compressed(os.path.join(out_path, outname),
                               rates=evaled)
            print("Done. Pickling to ", out_path)
            outname_pickle = "reco_kde_eval_spline_{}_{}.pickle".format(args.dataset,
                                                                 args.flux)
            pickle.dump(spline, open(os.path.join(out_path, outname_pickle), "w"),
                        protocol=2)
