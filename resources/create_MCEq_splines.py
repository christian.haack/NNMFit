import os
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt
import numpy as np
#os.chdir('..')
from scipy import interpolate

#import solver related modules
from MCEq.core import MCEqRun
from mceq_config import config
#import primary model choices
import CRFluxModels as pm
import argparse
import pandas as pd
import itertools
import cPickle as pickle

def get_solutions(mceq_run, particle_ids, mag=3):
        return_fluxes = {}
        for pid in particle_ids:
            return_fluxes[pid] = mceq_run.get_solution(pid, mag)
        return return_fluxes

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--outdir", "-o", help= "Directory" , required=True)
    parser.add_argument("--cosz_steps",  help= "steps in cosZen" , default=4)
    parser.add_argument("--dropEnergies",  help= "discard the last n Bins energy" ,
                        default=1, type=int)
    parser.add_argument("--interaction-models",  help= "list of interactions",
                        nargs="+", default=['SIBYLL2.3c', "EPOS-LHC",
                                            "DPMJETIII20171", "QGSJETII04" ],
                        dest="interactions")
    parser.add_argument("--Emultiplier", type=int, help= "E**x spectrum to flatten" ,
                        default=3)
    parser.add_argument("--average_zenith_spline1D", help="Average over zenith.",
                        action="store_true")
    parser.add_argument("--sum_zenith_spline1D", help="sum over zenith.",
                        action="store_true")
    args = parser.parse_args()


    if not os.path.exists(args.outdir):
        os.makedirs(args.outdir)

    interaction_models = args.interactions
    primary_models = [
                      [(pm.HillasGaisser2012,'H4a'),"Gaisser-H4a"],
                      [(pm.HillasGaisser2012,'H3a'),"Gaisser-H3a"],
                      [(pm.GlobalSplineFitBeta, None),"GSF-spline"],
                      [(pm.GaisserStanevTilav,'4-gen'),"GST-4gen"],
                    ]

    density_profiles = [
                       #('CORSIKA', ('USStd', None)),
                       #]
                        ('MSIS00_IC',('SouthPole','January')),
                        #('MSIS00_IC',('SouthPole','February')),
                        #('MSIS00_IC',('SouthPole','March')),
                        ('MSIS00_IC',('SouthPole','April')),
                        #('MSIS00_IC',('SouthPole','May')),
                        #('MSIS00_IC',('SouthPole','June')),
                        ('MSIS00_IC',('SouthPole','July')),
                        #('MSIS00_IC',('SouthPole','August')),
                        #('MSIS00_IC',('SouthPole','September')),
                        ('MSIS00_IC',('SouthPole','October')),
                        #('MSIS00_IC',('SouthPole','November')),
                        #('MSIS00_IC',('SouthPole','December')),
                        ]
    #Power of energy to scale the flux (the results will be returned as E**mag * flux)
    mag = args.Emultiplier
       
    ## steps in cos(zenith)
    cosz_steps = args.cosz_steps
    ## drop the highest energy bins to avoid NaNs
    dropEnergyBins = args.dropEnergies
    #Define equidistant grid in cos(theta)
    theta_grid = np.arccos(np.linspace(1., -1., cosz_steps))
    #theta_grid = np.arccos(np.linspace(0.2, -1., cosz_steps))
    theta_grid *= 180./np.pi
    particle_ids = []
    #for key1,key2 in itertools.product(["k_", "pi_", "conv_", ""],
    #for key1,key2 in itertools.product(["k_", "pi_", "conv_", "", "pr_"],
    for key1,key2 in itertools.product(["conv_", "", "pr_"],
                                       ["numu", "antinumu", "nue", "antinue"]):
        particle_ids.append(key1+key2)
    particle_ids.append("mu-")
    particle_ids.append("mu+")
    ## loop over all combinations of interaction models and primary models:
    for (inter,primary) in list(itertools.product(interaction_models, primary_models)):
        print "Current combination: ", inter, "--" , primary[1]
        outfile = args.outdir+"/MCEq_splines_PRI-{0}_INT-{1}".format(
            primary[1], inter.replace(".",""))
        #obtain energy grid (fixed) of the solution for the x-axis of the plots
        #Dictionary for results
        splines_conv = {}
        flux_for_density = {}
        for density in density_profiles:
            print("Current atmosphere model", density)
            config["density_model"] = density
            if density[1][1] is not None:
                density_str = density[0]+density[1][0]+density[1][1]
            else:
                density_str =  density[0]+density[1][0]
            mceq_run = MCEqRun(interaction_model=inter,
                               primary_model=primary[0],
                               theta_deg=0.0,
                               **config)
            e_grid = mceq_run.e_grid
            flux_for_density[density_str] = {}
            for flux_str in particle_ids:
                flux_for_density[density_str][flux_str] = np.zeros((len(theta_grid),
                                                 len(e_grid)-dropEnergyBins))

            for theta_id, theta in enumerate(theta_grid):
                #Set the zenith angle
                mceq_run.set_theta_deg(theta)
                #Run the solver
                mceq_run.solve()

                flux_solutions = get_solutions(mceq_run, particle_ids, mag=mag)
                for flux_str in particle_ids:
                    flux_for_density[density_str][flux_str][theta_id,:] = flux_solutions[flux_str][:-dropEnergyBins]
                    #print flux_for_density[density_str][flux_str][theta_id,:]
                    if np.any(flux_for_density[density_str][flux_str][theta_id,:]==0.0):
                        print(flux_for_density[density_str][flux_str][theta_id,:])
                        if inter in ["EPOS-LHC","QGSJETII04"]:
                            if "pr" in flux_str:
                                print "EPOS and QGSJET have no Prompt! Skipping"
                                continue
                        raise ValueError("Found zeros in flux %s", flux_str)

        # average density models and create splines per temperature:
        fluxes = {}
        for flux_str in particle_ids:
            fluxes[flux_str] = np.zeros((len(theta_grid), len(e_grid)-dropEnergyBins))

        spline_kwargs = dict(kx=1, ky=1, s=0)#.2)
        log_e_grid = np.log10(e_grid)[:-dropEnergyBins]
        flux_for_density_splines = {}
        for flux_str in particle_ids:
            for density in flux_for_density.keys():
                fluxes[flux_str] += flux_for_density[density][flux_str]
                spline_tmp = interpolate.RectBivariateSpline(theta_grid,
                                                             log_e_grid,
                                                             np.log10(flux_for_density[density][flux_str]),
                                                             **spline_kwargs)
                flux_for_density_splines[flux_str+density] = spline_tmp
            fluxes[flux_str] /= len(flux_for_density.keys())*1.

        #create splines:
        if args.average_zenith_spline1D:
            for flux_str in fluxes.keys():
                ##average fluxes in zenith:
                flux_energy = fluxes[flux_str].sum(axis=0)/len(theta_grid)
                ##"This is wrong?! needs a factor of cos(theta) to correct for solid angle?!")
                spline_tmp = interpolate.InterpolatedUnivariateSpline(log_e_grid,
                                                                      np.log10(flux_energy),
                                                                      ext=2)
                splines_conv[flux_str] = spline_tmp
            outfile += "_1DEnergy_ZenAveraged"
        elif args.sum_zenith_spline1D:
            for flux_str in fluxes.keys():
                ##average fluxes in zenith:
                flux_energy = fluxes[flux_str].sum(axis=0)
                spline_tmp = interpolate.InterpolatedUnivariateSpline(log_e_grid,
                                                                      np.log10(flux_energy),
                                                                      ext=2)
                splines_conv[flux_str] = spline_tmp
            outfile += "_1DEnergy"
        else:
            for flux_str in fluxes.keys():
                spline_tmp = interpolate.RectBivariateSpline(
                                                          theta_grid,
                                                          log_e_grid,
                                                          np.log10(fluxes[flux_str]),
                                                          **spline_kwargs)
                splines_conv[flux_str] = spline_tmp

        spline_dscr = {}
        spline_dscr["log_e_grid"] = log_e_grid
        spline_dscr["theta_grid"] = theta_grid
        spline_dscr["Emultiplier"] = mag
        spline_dscr["description"] ="Spline created like this: spline("\
                                    "theta, log10(energy), log10(flux). \n "\
                                    "-->  Usage: flux = 10**spline(theta/deg,"\
                                    "log10(energy))"
        outf_all = outfile+"_allfluxes.pickle"
        outf_seasonal = outfile+"_conv_seasonal.pickle"
        with open(outf_all, "w") as out:
            print("Pickling the conv flux splines to file: ", outf_all)
            pickle.dump([spline_dscr, splines_conv], out)
        with open(outf_seasonal, "wc") as out:
            print("Pickling the conv flux splines per season to file: ", outf_seasonal)
            pickle.dump([spline_dscr, flux_for_density_splines], out)

if __name__=="__main__":
    main()
