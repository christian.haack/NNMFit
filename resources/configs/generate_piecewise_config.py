""" Ugly but fast solution to generate the config file for piecewise_astro\
        Args : Either the binEdges or the range and the number of bins equally\
        spaced in log10(E_true)"""

import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("--binEdges", help="Energy bin edges for spectral fit",
                    nargs="+", dest="binedges", type=float, required=False)
parser.add_argument("--logBins", help="range and number of log-bins \
                    in the high energy part",
                    nargs = 3, dest="logbins", required=False)
parser.add_argument("--cfgname", help="naming scheme for that binning", 
                    dest="cfgname", required=False)
parser.add_argument("--index_per_bin", help="spectral index per bin", 
                    default=2.0, required=False)
parser.add_argument("--add_lowE_bin", help="Add a lowE bin? give edge.", 
                    default=False, required=False)
parser.add_argument("--add_highE_bin", help="Add a highE bin? give edge.", 
                    default=False, required=False)
args = parser.parse_args()

if args.cfgname is not None:
    piecewise_file = args.cfgname
else:
    piecewise_file = "piecewise_astro.cfg"

cfg_f = open(piecewise_file,"w")

if args.binedges is not None:
    BinEdges = np.array(args.binedges)

elif args.logbins is not None:
    low = np.log10(float(args.logbins[0]))
    up = np.log10(float(args.logbins[1]))
    #BinEdges = [10] + 
    BinEdges = list(10**np.linspace(low,
                               up,
                               int(args.logbins[2])))
    #BinEdges = [2e2]+ BinEdges + [5e7]
    if args.add_lowE_bin:
        BinEdges = [float(args.add_lowE_bin)] + BinEdges
    if args.add_highE_bin:
        BinEdges = BinEdges + [float(args.add_highE_bin)]
else:
    print "No binedges defined"

print "BinEdges : " , BinEdges

for n,p in enumerate(range(1,len(BinEdges))):
    cfg_f.write("piece{}:\n".format(p))
    cfg_f.write("  default: {}\n".format((n+1)*0.7))
    cfg_f.write("  range: [0., null]\n")
    cfg_f.write("  interpolate: False\n  class: PiecewiseNorm\n")
    cfg_f.write("  additional:\n")
    cfg_f.write("    binedge_low: {}\n    binedge_up: {}\n".\
                format(BinEdges[p-1], BinEdges[p]))
    cfg_f.write("    per_flavor_norm: False\n")
    cfg_f.write("    reference_index: 2.0\n")
    cfg_f.write("    index_in_bin: {}\n".format(args.index_per_bin))

cfg_f.close()
print "Config file dumped to: ", piecewise_file
