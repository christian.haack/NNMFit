from misc import UnifiedFigureStyle as ufs
from scipy import ndimage, stats
import os
from glob import glob
import dashi as d
import numpy as n
import scipy.special
import cPickle as pickle
from scipy import interpolate
import pandas as pd

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from NNMFit.analysis_config import AnalysisConfig
from NNMFit import loaders
from NNMFit import Dataset
from NNMFit import nnm_logger
import yaml

plot_dir = "/home/jstettner/public_html/NNMFit/nutau_splines/"
det_configs = ["IC86_pass2"]
config_hdl = AnalysisConfig("default_IC86pass2.cfg",
                            det_configs)

##read exp data:
ds_exp = pd.read_hdf("/data/ana/analyses/diffuse/diffuse-numu/data/dataset_data_IC2010toIC2018_pass2afterReprocessing.hdf")

#outfile = "/data/user/jstettner/DiffuseExtensions/data/MC//nutau_nue_corrections/nutau_correction_IC86_pass2_buggyMCds21033.pickle"
#nutau_path = "/data/user/jstettner/DiffuseExtensions/data/MC/IC86_pass2_tmp/datasets/baseline_nutau/dataset_pass2_baseline_nutau_ds21033.hdf"
nutau_path = None

bins_ptype = {"nu_e": [5,4],
              "nu_tau": [25,12]}
ptype = "nu_tau"

for det_config in det_configs:
    nutype_str = "{}_{}_correction".format(config_hdl[det_config]["name"],
                                           ptype)

    key_mapping, key_mapping_mc = config_hdl.get_key_mapping(det_config)

    baseline_ds_path = glob(os.path.join(
        config_hdl[det_config]['baseline_dataset'],"*.hdf"))
    if len(baseline_ds_path) != 1:
        print(baseline_ds_path)
        raise RuntimeError("Could not find baseline dataset")
    dataset_path = baseline_ds_path[0]
    dataset_mc_base = Dataset(dataset_path, key_mapping_mc,
                              ra_oversampling=None, cache=None)


    bins, bincenters = config_hdl.make_binning(det_config)
    binsE = bins[0]
    binsZ = bins[1]

    #binsE = n.logspace(n.log10(binsE[0]), n.log10(binsE[-1]),
    #                   bins_ptype[ptype][0])# 16)
    binslogE = n.linspace(n.log10(binsE[0]), n.log10(binsE[-1]), bins_ptype[ptype][0])
    binsZ = n.arccos(n.linspace(n.cos(binsZ[0]), n.cos(binsZ[-1]),
                                bins_ptype[ptype][1]))
    print("Bin-Edges: ",  binslogE, binsZ)
    splines = {}
    for model in ["powerlaw"]:#models:
        if nutau_path is None:
            outfile = config_hdl[nutype_str]["spline_file"]
            nutau_path = os.path.join(config_hdl[det_config]['datasets_path'],
                                      "baseline_nu_tau")
            ds_path_nutau = glob(os.path.join(nutau_path, "*.hdf"))
        else:
            ds_path_nutau = [nutau_path]
        print("Reading nutau file", ds_path_nutau)
        if len(ds_path_nutau) != 1:
            raise RuntimeError("Could not find nutau dataset")
        dataset_mc_nutau = Dataset(ds_path_nutau[0], key_mapping_mc,
                                   ra_oversampling=None, cache=None)

        print "Reweighting for model: ", model
        h2d_base = d.factory.hist2d((dataset_mc_base.reco_zenith,
                                     n.log10(dataset_mc_base.reco_energy)),
                                     bins=(binsZ,binslogE),
                                     weights = dataset_mc_base.weight(model))

        h2d_nutau = d.factory.hist2d((dataset_mc_nutau.reco_zenith,
                                      n.log10(dataset_mc_nutau.reco_energy)),
                                     bins=(binsZ,binslogE),
                                     weights = dataset_mc_nutau.weight(model))

        h2d_correction = (h2d_base.bincontent + h2d_nutau.bincontent) / h2d_base.bincontent

        fig_nutau, ax = ufs.newfig(1., 1.2, nrows=1, ncols=1, ratioplots=False)

        sigma_E, sigma_cosZ = 2.0, 1.8
        h2d_correction = ndimage.gaussian_filter(h2d_correction,
                                                 sigma=[sigma_E, sigma_cosZ],
                                                 mode="reflect")


        im = plt.imshow(h2d_correction, interpolation='nearest', origin='low',
                    extent=[h2d_base.binedges[1][0],
                            h2d_base.binedges[1][-1],
                            h2d_base.binedges[0][0],
                            h2d_base.binedges[0][-1]],
                    vmin=1., vmax=1.5)

        plt.scatter(n.log10(ds_exp["energy_truncated"]),
                    ds_exp["zenith_MPEFit"],
                    alpha=0.12, s=3, c="white")

        plt.xlim(h2d_base.binedges[1][0], h2d_base.binedges[1][-1])
        plt.ylim(h2d_base.binedges[0][0], h2d_base.binedges[0][-1])
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)

        cb = plt.colorbar(im, cax=cax)
        cb.set_label("Ratio of Expectation")

        ax.tick_params(axis='both', which='major')
        ax.set_xlabel(r"log$_{10}$(Muon Energy Proxy / GeV)")
        #ax.set_xscale("log")
        ax.set_ylabel("Reconstructed Zenith")
        plt.tight_layout()


        base_zero_indices = (h2d_base.bincontent==0.)
        if n.sum(h2d_nutau.bincontent[base_zero_indices])>0:
            print("NuTau expectation is nonzero where baseline is zero. Problem in the way splines are built now")
            raise NotImplementedError
        else:
            h2d_correction[base_zero_indices] = 1.
        print( n.mean(h2d_correction), n.nanmax(h2d_correction))
        spline_kwargs = dict(kx=1, ky=1)#, s=0.9)#ky=3, s=0.2)
        spline_correction = interpolate.RectBivariateSpline(h2d_base.bincenters[0],
                                                            h2d_base.bincenters[1],
                                                            h2d_correction,
                                                            **spline_kwargs)
        #print spline_correction(h2d_base.bincenters[0],
        #                        h2d_base.bincenters[1])/h2d_correction

        splines[model] = spline_correction


    print "Dumping spline to file" , outfile
    pickle.dump(splines,
                open(outfile, "w"))

    ufs.SaveFig(fig_nutau, plot_dir, "NuTauCorrection_ratio", format="png")
