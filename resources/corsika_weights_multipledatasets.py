#!/usr/bin/env python

"""
Calculate weights for 5-component CORSIKA simulation.
Use with py2v3.1.1 and
/cvmfs/icecube.opensciencegrid.org/py2-v3.1.1/RHEL_7_x86_64/metaprojects/combo/V00-00-01/
"""

from icecube import icetray, dataclasses, dataio
from icecube.hdfwriter import I3HDFWriter
from I3Tray import I3Tray

import glob
from argparse import ArgumentParser
from icecube.weighting.weighting import from_simprod, NeutrinoGenerator
from icecube.weighting import CORSIKAWeightCalculator
from icecube.weighting import fluxes
import yaml
import numpy as np


parser = ArgumentParser(description=__doc__)
parser.add_argument('--outfile', '-o')
parser.add_argument('--config', '-c')
opts = parser.parse_args()

config = yaml.load(open(opts.config,"r"))

all_infiles = []
all_generators = None
for ds_key, ds_config in config.items():
    print(ds_key)
    print(ds_config)
    file_list_ds = glob.glob(ds_config["files_glob"])
    all_infiles += file_list_ds

    number_of_files_ds = len(file_list_ds)
    print(number_of_files_ds)
    ds_num = int(ds_config["ds_num"])
    ds_generator = from_simprod(ds_num)*number_of_files_ds
    if all_generators is None:
        all_generators = ds_generator
    else:
        all_generators += ds_generator

tray = I3Tray()

tray.AddModule('I3Reader', 'reader', filenamelist=all_infiles)

tray.AddModule(CORSIKAWeightCalculator, 'GaisserH4aWeight',
               Dataset=all_generators,
               Flux=fluxes.GaisserH4a())

tray.AddModule("I3Writer",
    Filename = opts.outfile,
    DropOrphanStreams=[icetray.I3Frame.DAQ,
                       icetray.I3Frame.TrayInfo,
                       icetray.I3Frame.Stream('M')],
     Streams=[icetray.I3Frame.DAQ,
              icetray.I3Frame.Physics,
              icetray.I3Frame.TrayInfo,
              icetray.I3Frame.Simulation,
              icetray.I3Frame.Stream('M')])

#tray.AddSegment(I3HDFWriter, 'scribe',
#    Output=opts.outfile,
#    Keys=['MCPrimary', 'CorsikaWeightMap', 'GaisserH3aWeight'],
#    Types=[],
#    SubEventStreams=['in_ice', "Final"],
#               )

tray.Execute()
