"""Implementation of ExpectationHandler"""
import logging
from theano import shared, function
from theano import tensor as T
from .binning import make_binned_flux

logger = logging.getLogger(__name__)


class ExpectationHandler(object):
    """
    This class creates the graph for calculating the expectation histogram
    """

    def __init__(self,
                 settings,
                 global_settings,
                 det_config):
        """
        Args:
            settings: NamedTuple(DetConfSet) containing the settings
            fluxes: dict of flux objects
            det_config: Detector config name
        """
        self._settings = settings
        self._global_settings = global_settings
        self._binning = settings.analysis_binning[0]
        self._oversampling = settings.dataset_mc.oversampling
        self._det_conf = det_config

        if self._oversampling and not settings.is_3d:
            raise RuntimeError("Oversampling only implemented for 3D")
        self._n_bins_x = len(self._binning[0])-1
        self._n_bins_y = len(self._binning[1])-1
        if len(self._binning) == 3:
            self._n_bins_z = len(self._binning[2])-1
        self._fluxes = {}
        self._indices_flattened = shared(
            self._settings.dataset_mc.flattened_indices)

        for flux_name, flux_obj in global_settings.fluxes.iteritems():
            self.add_flux(flux_name, flux_obj)

    @property
    def is_3d(self):
        """Returns true if this will create a 3d graph"""
        return self._settings.is_3d

    def make_shared_variables(self, variables):
        """Return list of shared variables"""
        shared_vars = {}
        for var in variables:
            var_t = shared(self._settings.dataset_mc.read_key_as_numpy(var),
                           borrow=True)
            #var_t = self._settings.dataset_mc.read_key(var)
            shared_vars[var] = var_t
        return shared_vars

    def add_flux(self, flux_name, flux_obj):
        """
        Add flux object

        Args:
            flux_name: Name of the flux
            flux_obj: Flux object
        """
        self._fluxes[flux_name] = flux_obj

    def make_graph_for_flux(self, flux_name):
        """Return theano graph for flux"""
        flux = self._fluxes[flux_name]
        req_pars = flux.get_list_of_req_vars()
        align_set = self._settings.alignment
        shared_vars = self.make_shared_variables(req_pars)
        return flux.make_graph(shared_vars,
                               align_set)

    def make_sys_graph_for_flux(self, flux_name):
        """Return systematics graph for flux_name"""
        req_pars = self._global_settings.det_systematics.\
                get_list_of_req_vars(self._det_conf, flux_name)
        shared_vars = self.make_shared_variables(req_pars)
        return self._global_settings.det_systematics.make_graph(
            flux_name, self._det_conf, shared_vars)


    def make_graph_and_apply_sys(self, flux, flux_uses_3d):
        """
        Return theano graph for flux with applied detector
        systematics

        Args:
            flux: Name of the flux
            flux_uses_3d: (bool) if this flux uses 3D
        """
        logger.debug("Creating graph for flux %s", flux)
        graph_f = self.make_graph_for_flux(flux)
        graph_sys = self.make_sys_graph_for_flux(flux)

        if self._global_settings.binned_systematics:
            try:
                if self._settings.is_3d:
                    graph_sys = graph_sys.reshape((self._n_bins_x,
                                                   self._n_bins_y,
                                                   1))
                else:
                    graph_sys = graph_sys.reshape((self._n_bins_x,
                                                   self._n_bins_y))
            except AttributeError:
                logger.warning("Could not reshape sys. graph. No "
                               "systematics configured?")
                graph_sys = 1
            oversampling_flag = None
            if self._oversampling:
                if flux_uses_3d:
                    oversampling_flag = "over"
                else:
                    oversampling_flag = "repeat"
            graph_f = make_binned_flux(graph_f,
                                       self._binning,
                                       self._indices_flattened,
                                       oversampling_flag)
        return graph_f*graph_sys

    def make_expectation_graph(self, binned_expectation=True):
        """
        Return the full theano graph for this expectation

        Detector systematics and livetime are applied

        Args:
            binned_expectation: (bool) Return flux binned according to analysis
                                 bins
        """
        total_graph = 0

        if binned_expectation and not self._global_settings.binned_systematics:
            for flux_name, flux in self._fluxes.iteritems():
                graph = self.make_graph_and_apply_sys(flux_name,
                                                      flux.uses_3d)
                if self._oversampling:
                    if flux.uses_3d:
                        oversampling_flag = "over"
                    else:
                        oversampling_flag = "repeat"
                else:
                    oversampling_flag = None
                total_graph += make_binned_flux(graph,
                                                self._binning,
                                                self._indices_flattened,
                                                oversampling_flag)
        else:
            # If we're using binned systematics, flux is already binned
            # otherwise returned unbinned graph
            for flux_name, flux in self._fluxes.iteritems():
                total_graph += self.make_graph_and_apply_sys(flux_name,
                                                             flux.uses_3d)
        return total_graph*self._settings.livetime
