"""Collection of functions for binning a theano graph"""
import theano.tensor as T
import scipy.stats

def make_binned_data(analysis_vars, binning, is_3d):
    if is_3d:
        data_hist, _, _ = scipy.stats.binned_statistic_dd(
            analysis_vars,
            None,
            statistic="count",
            bins=binning)
    else:
        data_hist, _, _, _ = scipy.stats.binned_statistic_2d(
            analysis_vars[0],
            analysis_vars[1],
            None,
            statistic="count",
            bins=binning[:2])
    return data_hist


def make_binned_flux(flux_graph, binedges, indices, oversampling_flag=None):
    """
    Return a binned flux

    This function is smart about producing broadcastable histrograms.
    If binedges is 3D, oversampling flag can be either:
        - None
        - "over": The indices are flattened and the resulting
          histogram divided by the size of th oversampling dimension
        - "repeat": The indices of the first oversampling iteration are used
          and the resulting histogram is summed over the third dimension and
          afterwards made broadcastable in the third dimension.
    Args:
        flux_graph: Theano graph
        binedges: 2D or 3D array of binedges
        indices: Bin index for every event in graph. Can be of shape (n_events)
        or (n_overs, n_events)
        oversampling_flag: Can be either None, or "over" or "repeat"
    """
    n_bins_x = len(binedges[0]) - 1
    n_bins_y = len(binedges[1]) - 1
    if len(binedges) == 3:
        n_bins_z = len(binedges[2]) - 1
        total_bins_3d = n_bins_x * n_bins_y * n_bins_z
        if oversampling_flag is None:
            binned_flux = T.extra_ops.bincount(
                indices, weights=flux_graph, minlength=total_bins_3d)
            binned_flux = binned_flux.reshape((n_bins_x, n_bins_y, n_bins_z))
        elif oversampling_flag == 'over':
            binned_flux = T.extra_ops.bincount(
                T.flatten(indices),
                weights=T.flatten(flux_graph),
                minlength=total_bins_3d)
            binned_flux = binned_flux.reshape((n_bins_x, n_bins_y, n_bins_z))
            binned_flux /= indices.shape[0]
        elif oversampling_flag == 'repeat':
            binned_flux = T.extra_ops.bincount(
                indices[0], weights=flux_graph, minlength=total_bins_3d)
            binned_flux = binned_flux.reshape((n_bins_x, n_bins_y, n_bins_z))
            binned_flux = binned_flux.sum(axis=-1, keepdims=True)
            binned_flux /= n_bins_z
        else:
            raise NotImplementedError(
                'Mode: {} not implemented'.format(oversampling_flag))
    else:
        total_bins_2d = n_bins_x * n_bins_y
        binned_flux = T.extra_ops.bincount(
            indices, weights=flux_graph, minlength=total_bins_2d)
        binned_flux = binned_flux.reshape((n_bins_x, n_bins_y))
    return binned_flux



