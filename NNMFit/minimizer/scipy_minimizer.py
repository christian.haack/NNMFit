"""Implementation of scipy minmizers"""
import logging
import scipy
import numpy as n
from theano import function
from theano import gradient
import theano
import theano.tensor as T
from .minimizer import Minimizer
#from theano.compile.nanguardmode import NanGuardMode
logger = logging.getLogger(__name__)

class NewtonCG(Minimizer):
    """
    Interface to Newton-CG from scipy
    """
    def __init__(self,
                 min_func,
                 params,
                 seeds,
                 bounds,
                 fixed_pars,
                 profile=False,
                 *args,
                 **kwargs):
        super(NewtonCG, self).__init__(min_func, params, seeds, bounds,
                                       fixed_pars, profile, *args, **kwargs)
        self._theano_func = None
        self._theano_grad = None
        self._theano_hessian = None

        self.hessians = []
        param_list = [param for param in
                      self.params if param.name not in
                      self.fixed_pars]

        self.hessian = [it for grad in self.grads
                        for it in T.grad(grad, param_list,
                                        disconnected_inputs="ignore")]

    def __call_theano_function(self, func, args, log=False, reshape=None):
        """
        Wrapper for calling the theano llh function.
        Takes care of parameter updates and removes fixed
        parameters from the gradient
        """
        self._update_parameters(args)

        result = func()
        var_name_value_dict = {self._input_var_ordering[i]: args[i]
                               for i in xrange(len(args))}
        result = n.asarray(result)
        if reshape is not None:
            result = result.reshape(reshape)
        if log:
            logger.debug("LLH: %s, args: %s", -result,
                         var_name_value_dict)
        return result

    def make_mini_function(self):
        """
        Helper function to build the minimization function
        """
        self._theano_func = function(
            [], [self.min_func],
            allow_input_downcast=False,
            profile=self.profile)
        self._theano_grad = function(
            [], self.grads,
            allow_input_downcast=False,
            profile=self.profile)
        npars = len(self._input_var_ordering)
        self._theano_hessian = function(
            [], self.hessian,
            allow_input_downcast=False,
            profile=self.profile)
        func = lambda args: self.__call_theano_function(self._theano_func,
                                                        args, log=True)
        func_g = lambda args: self.__call_theano_function(self._theano_grad,
                                                          args)
        func_h = lambda args: self.__call_theano_function(
            self._theano_hessian,
            args,
            reshape=(npars, npars))
        return func, func_g, func_h

    def minimize(self, **kwargs):
        min_func, grad, hessian = self.make_mini_function()
        res = scipy.optimize.fmin_ncg(min_func,
                                      self.seed_list,
                                      fprime=grad,
                                      fhess=hessian)
        res_dict = {var: res[0][i]
                    for i, var in enumerate(self._input_var_ordering)}
        if self.profile:
            self._theano_func.profile.summary()
        return res, res_dict

class LBFGSB(Minimizer):
    """
    Interface to lbfgsb from scipy
    """

    def __init__(self,
                 min_func,
                 params,
                 seeds,
                 bounds,
                 fixed_pars,
                 profile=False,
                 *args,
                 **kwargs):
        super(LBFGSB, self).__init__(min_func, params, seeds, bounds,
                                     fixed_pars, profile, *args, **kwargs)
        self._theano_func = None

    def __call_theano_function(self, func, args):
        """
        Wrapper for calling the theano llh function.
        Takes care of parameter updates
        Args:
            func : LLH theano function
            args: current set of variables
        """
        self._update_parameters(args)

        result = func() # [llh_value, grad[0], grad[1], ..])
        grad = n.asarray(result[1:], dtype=n.float64)
        # Logging
        var_name_value_dict = {self._input_var_ordering[i]: args[i]
                               for i in xrange(len(args))}
        var_name_grad_dict = {self._input_var_ordering[i]: grad[i]
                               for i in xrange(len(args))}
        logger.debug("LLH: %s\n args: %s ",
                     result[0],
                     #-result[0],
                     var_name_value_dict)
        #, var_name_grad_dict)
        return result[0], grad

    def make_mini_function(self):
        """
        Helper function to build the minimization function
        """
        '''
        #DEBUGGING: Use this snippe to get a handle if you have nans
        import numpy as np
        def inspect_inputs(i, node, fn):
            print(i, node, "input(s) value(s):", [input[0] for input in fn.inputs])
            print("###Input Nan indices: ", [np.argwhere(np.isnan(input[0])) for input in fn.inputs])
        def inspect_outputs(i, node, fn):
            print(" output(s) value(s):", [output[0] for output in fn.outputs])
            print("###Output Nan indices: ", [np.argwhere(np.isnan(output[0])) for output in fn.outputs])
        mode = theano.compile.MonitorMode(pre_func=inspect_inputs,
                                          post_func=inspect_outputs)
        '''
        self._theano_func = function([], [self.min_func] + self.grads,
                                     allow_input_downcast=False,
                                     profile=self.profile,
                                     #mode=mode
                                    )

        func = lambda args: self.__call_theano_function(self._theano_func,
                                                        args)
        return func

    def minimize(self, **kwargs):
        min_func = self.make_mini_function()
        logger.debug("Minimizer Setup: Seeds = %s, Bounds = %s ",
                     self.seed_list, self.bounds_list)
        res = scipy.optimize.fmin_l_bfgs_b(min_func,
                                           self.seed_list,
                                           bounds=self.bounds_list,
                                           approx_grad=False,
                                           factr=float(kwargs["tolerance"]),
                                           m=15,
                                           maxls=30)
        res_dict = {var: res[0][i]
                    for i, var in enumerate(self._input_var_ordering)}
        if self.profile:
            self._theano_func.profile.summary()
        return res, res_dict

    def evaluate(self, **kwargs):
        min_func = self.make_mini_function()
        res = min_func(self._input_var_ordering)
        res_dict = {var: res[0][i]
                    for i, var in enumerate(self._input_var_ordering)}
        return res, res_dict


