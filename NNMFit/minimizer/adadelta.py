"""Implementation of adadelta minimizer"""
import logging
import numpy as n
from theano import shared
from theano import function
import theano.tensor as T
from .minimizer import Minimizer
logger = logging.getLogger(__name__)



class Adadelta(Minimizer):
    """Implementation of Adadelta algorithm
    Stolen from keras
    """
    def __init__(self,
                 min_func,
                 params,
                 grads,
                 seeds,
                 bounds,
                 fixed_pars,
                 *args,
                 **kwargs):
        super(Adadelta, self).__init__(min_func, params, grads, seeds, bounds,
                                       fixed_pars, args, **kwargs)
        extra_args = {"lr": 1.0, "rho": 0.95, "epsilon":1E-8, "decay": 0}
        extra_args.update(kwargs)
        self.lr = shared(extra_args["lr"])
        self.rho = extra_args["rho"]
        self.epsilon = extra_args["epsilon"]
        self.decay = shared(extra_args["decay"])
        self.initial_decay = extra_args["decay"]

    def minimize(self, tolerance, fmax):

        updates = self.get_updates()
        func = function([], self.min_func, updates=updates)

        fval_prev = None
        nit = 0
        while True:
            fval = func()
            str_o = ""
            for var in self.params:
                str_o += "{}: {}, ".format(var.name, var.get_value())
            logger.debug("%s: %s",fval, str_o)
            if (fval_prev is not None)\
               and (n.abs((fval_prev-fval)) < tolerance):
                break_cond = 0
                break
            if nit > fmax:
                break_cond = 1
                break
            fval_prev = fval
            nit += 1
        if break_cond == 0:
            logger.info("Sucessful minimzation, f=%d", fval)

        min_par_dict = {var.name: var.get_value() for var in self.params}
        return fval, min_par_dict

    def get_updates(self):
        """
        Calculate parameter updates for each iteration as specified in
        Adadelta algorithm
        """
        # pylint: disable=invalid-name
        params = self.params
        grads = self.grads
        #grads = self.get_gradients(loss, params)
        # TODO: We implicitely require params to be scalars!
        accumulators = [shared(0.) for p in params]
        delta_accumulators = [shared(0.) for p in params]
        iterations = shared(0)
        updates = []

        lr = self.lr
        if self.initial_decay > 0:
            lr *= (1. / (1. + self.decay * iterations))
            updates.append((iterations, 1))

        for i, (p, g, a, d_a) in enumerate(
            zip(params, grads,
                accumulators, delta_accumulators)):
            if i in self._fixed_input_var_indices:
                continue
            # update accumulator
            new_a = self.rho * a + (1. - self.rho) * T.square(g)
            updates.append((a, new_a))

            # use the new accumulator and the *old* delta_accumulator
            update = g * T.sqrt(d_a + self.epsilon) / \
                    T.sqrt(new_a + self.epsilon)

            # update params
            new_p = p - lr * update
            updates.append((p, new_p))

            # update delta_accumulator
            new_d_a = self.rho * d_a + (1 - self.rho) * T.square(update)
            updates.append((d_a, new_d_a))
        return updates
