"""Implementation of minimizer base class"""
import logging
import theano.tensor as T
import numpy as n

logger = logging.getLogger(__name__)


class Minimizer(object):
    """
    Interface for minimizers
    """
    # pylint: disable=too-few-public-methods

    def __init__(self,
                 min_func,
                 params,
                 seeds,
                 bounds,
                 fixed_pars,
                 profile=False,
                 *args,
                 **kwargs):

        self.min_func = min_func

        self.seeds = seeds
        self.bounds = bounds
        self.fixed_pars = fixed_pars
        self.params = params
        self.profile = profile
        input_var_ordering = [var.name for var in params
                              if var.name not in fixed_pars]
        self._input_var_ordering = input_var_ordering
        self._fixed_input_var_indices = [i for i, var in
                                         enumerate(params) if
                                         var.name in fixed_pars]
        self._remove_from_gradient = [param for param in
                                           self.params if param.name not in
                                           self.fixed_pars]

        self.grads = T.grad(min_func, wrt=[param for param in self.params
                                           if param.name not in
                                           self.fixed_pars])

        for param in self.params:
            param.set_value(self.seeds[param.name])

        logger.info("Fitted parameters: {}".format(
                   str(self._input_var_ordering)))
        logger.info("Fixed parameters: {}".format(str(self.fixed_pars)))
        logger.info("Seeded parameters: {}".format(str(self.seeds)))
        logger.info("Gradient calculated wrt these"+\
                    "parameters:{}".format(self._remove_from_gradient))

    def _update_parameters(self, args):
        # Parameter updates
        fix_cnt = 0 # If a parameter is fixed, it will not be in args
        for i, par in enumerate(self.params):
            if i in self._fixed_input_var_indices:
                val = self.fixed_pars[par.name]
                fix_cnt += 1
            else:
                val = args[i-fix_cnt]
            par.set_value(val)

    @property
    def bounds_list(self):
        return [self.bounds[var] for var in self._input_var_ordering]

    @property
    def seed_list(self):
        return  [self.seeds[var] for var in self._input_var_ordering]

    def make_mini_function(self):
        """
        Helper function to build the minimization function
        """
        raise NotImplementedError("Needs to be implemented by subclass")

    def minimize(self, tolerance, fmax):
        """
        Minimization function

        Args:
            tolerance: Value that determines the
                       accuracy of the minimization
            fmax: Maximum number of function calls
        """
        raise NotImplementedError("Needs to be implemented by subclass")
