"""Implementation of Cache class"""
import contextlib
import os
#import xxhash

#at least on the RZ, use hashlib instead:
import hashlib
##xxhash was used as standard

class Cache(object):
    """Simple handler for cache files"""

    def __init__(self, cache_dir):
        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)
        self._cache_dir = cache_dir
        self._hashmodule = hashlib.md5()

    def clear_cache(self):
        """Clear cache"""
        for fname in os.listdir(self._cache_dir):
            f_path = os.path.join(self._cache_dir, fname)
            if os.path.isfile(f_path):
                os.unlink(f_path)

    #@staticmethod
    def generate_ident(self, data):
        """Return hex hash for data"""
        self._hashmodule.update(data)
        return self._hashmodule.hexdigest()
        #return xxhash.xxh64(data, seed=1337).hexdigest()

    def is_cached(self, ident):
        """Return if ident exists in cache folder"""
        return os.path.exists(os.path.join(self._cache_dir, ident))

    @contextlib.contextmanager
    def load_from_cache(self, ident):
        """Contextmanager for loading a cache file"""
        with open(os.path.join(self._cache_dir, ident)) as hdl:
            yield hdl

    @contextlib.contextmanager
    def save_to_cache(self, ident):
        """Contextmanager for saving a cache file"""
        with open(os.path.join(self._cache_dir, ident), "w") as hdl:
            yield hdl
