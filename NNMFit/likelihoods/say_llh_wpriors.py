"""Implementation of SAY llh w priors"""
import logging
import scipy.stats
import theano.tensor as T
from theano import shared
#from theano.scalar.basic_scipy import  gammaln as theano_gammaln
import numpy as n
from scipy.special import loggamma as scipy_loggamma
logger = logging.getLogger(__name__)


class SAYLLHwPriors(object):
    """
    Construct computation graph for modified poisson LLH

    Based on /arxiv.org/abs/1901.04645
    Arguelles, Schneider, Yuan in 2019
    Modified Poisson LLH to take limited MC statistics into account

    """

    def __init__(self, graph_builder, data_hists,
                 profile=None):

        self._det_configs = graph_builder.detector_configs
        self._profile = profile
        self._graph_builder = graph_builder

        self._datasets = data_hists
        self.say_llh = None
        self.fixed_pars = None
        prior_and_widths = graph_builder.parameter_priors

        self.priors = prior_and_widths[0]
        self.prior_widths = prior_and_widths[1]

    def make_llh(self, graphs=None, input_variables=None):
        """
        Return graph for SAY lh

        Args:
            graphs: Expectation graph for every det config
            input_variables: Input variables
        """

        prior_llh = 0
        # for all pars which have a prior, add the difference term to the llh
        for par in input_variables:
            if par.name in self.priors.keys():
                prior_llh += -1.*((par - self.priors[par.name])/ \
                                  (self.prior_widths[par.name]*n.sqrt(2)))**2.
                logger.debug("Gaussian prior on %s = %s, sigma = %s" % \
                                 (par.name, self.priors[par.name],\
                                  self.prior_widths[par.name]))


        llh = 0.
        ##TODO: Do not rebuild the graphs (with weights) but ensure that they
        ##the graph_builder is called properly in nnm_fitter
        graphs, input_variables, graphs_unbinned, indices = \
                self._graph_builder.make_expectation_graph_weights(binned_expectation=True,
                                                                   return_weights=True)

        for det_config in self._det_configs:
            _n_bins_x = len(self._graph_builder.det_conf_settings[det_config].binning[0])-1
            _n_bins_y = len(self._graph_builder.det_conf_settings[det_config].binning[1])-1
            total_bins_2d = _n_bins_x * _n_bins_y

            ## theano-friendly histogram from weights and indices
            binned_weights_sum = T.extra_ops.bincount(indices[det_config],
                                               weights=graphs_unbinned[det_config],
                                               minlength=total_bins_2d)
            binned_weights_sum = binned_weights_sum.reshape((_n_bins_x,
                                                             _n_bins_y))
            binned_weights_sqsum = T.extra_ops.bincount(indices[det_config],
                                               weights=graphs_unbinned[det_config]**2,
                                               minlength=total_bins_2d)
            binned_weights_sqsum = binned_weights_sqsum.reshape((_n_bins_x,
                                                                 _n_bins_y))

            alpha = binned_weights_sum**2. / binned_weights_sqsum + 1.0
            beta = binned_weights_sum / binned_weights_sqsum
            k = self._datasets[det_config]

            print det_config
            print "## count of nan weights"
            print sum(n.isnan(graphs_unbinned[det_config].eval()))

            print "### weights_sum"
            print binned_weights_sum.eval()
            print "#### weights_sqsum"
            print binned_weights_sqsum.eval()
            print "#### alpha"
            print alpha.eval()
            print "#### beta"
            print beta.eval()
            print "#### k"
            print k.eval()
            print "####"
            exit()
            #evaluate L as gammaPriorPoissonLikelihood(k, alpha, beta)
            llh_eff = alpha*T.log(beta)+\
                      T.gammaln(k+alpha)-\
                      T.gammaln(k+1.0)-\
                      (k+alpha)*T.log(1.+beta)-\
                      T.gammaln(alpha)

            #for bins where sum squared weights is zero, return PoissonLLH
            cond = T.gt(binned_weights_sqsum, 0.)
            llh_eff = T.switch(cond,
                               llh_eff,
                               -binned_weights_sum +k*T.log(1e-300+binned_weights_sum))
            #for bins where sum squared weights=0 and k=0, return 0
            cond2 = T.and_(T.le(binned_weights_sum, 0.), T.eq(k, 0.))
            llh_eff = T.switch(cond2,
                               0.,
                               llh_eff)
            llh += llh_eff.sum()

        del self._graph_builder
        llh += prior_llh
        return llh, input_variables
