"""Implementation of poissonian llh w priors"""
import logging
import scipy.stats
import theano.tensor as T
from theano import shared
import numpy as n
logger = logging.getLogger(__name__)


class PoissonLLHwPriors(object):
    """Construct computation graph for poisson LLH w priors"""

    def __init__(self, graph_builder, data_hists,
                 profile=None):

        self._det_configs = graph_builder.detector_configs
        self._profile = profile
        self._graph_builder = graph_builder

        self._datasets = data_hists
        self.poiss_llh = None
        self.fixed_pars = None
        prior_and_widths = graph_builder.parameter_priors

        self.priors = prior_and_widths[0]
        self.prior_widths = prior_and_widths[1]

    def sat_llh(self):
        """
        Return saturated LH (not including the priors yet)
        """
        llh = 0
        for det_config in self._det_configs:
            dataset = self._datasets[det_config]
            llh += T.sum(dataset*(-1 + T.log(dataset+1E-300)))
        return llh.eval()

    def sat_binwise_llh(self):
        """
        Return binwise saturated LH
        No priors considered.
        """
        llh = 0.
        for det_config in self._det_configs:
            dataset = self._datasets[det_config]
            llh += dataset*(-1. + T.log(dataset+1E-300))
        return llh

    def make_llh(self, graphs=None, input_variables=None):
        """
        Return graph for poissonian lh

        Args:
            graphs: Expectation graph for every det config
            input_variables: Input variables
        """
        #llh = 0.
        llh = -self.sat_llh()
        if graphs is None or input_variables is None:
            graphs, input_variables =\
                    self._graph_builder.make_expectation_graph(
                        binned_expectation=True)

        prior_llh = 0
        # for all pars which have a prior, add the difference term to the llh
        for par in input_variables:
            if par.name in self.priors.keys():
                prior_llh = prior_llh - ((par - self.priors[par.name])/ \
                                  (self.prior_widths[par.name]*n.sqrt(2)))**2.
                logger.debug("Gaussian prior on %s = %s, sigma = %s" % \
                                 (par.name, self.priors[par.name],\
                                  self.prior_widths[par.name]))

        for det_config in self._det_configs:
            expec = graphs[det_config]
            llh = llh + (-expec +
                         self._datasets[det_config] *
                         T.log(expec+1E-300)).sum()
        del self._graph_builder
        llh += prior_llh
        return llh, input_variables

    def make_binwise_llh(self, graphs=None, input_variables=None):
        """
        Return graph for binwise poissonian lh
        Careful! This does not include the prior-terms. They are added later
        to the total likelihood.
        """
        #llh = -self.sat_llh()
        llh = -self.sat_binwise_llh()
        if graphs is None or input_variables is None:
            graphs, input_variables = self._graph_builder.make_expectation_graph(
                binned_expectation=True)
        for det_config in self._det_configs:
            expec = graphs[det_config]
            llh = llh + (-expec +
                         self._datasets[det_config] *
                         T.log(expec+1E-300))
        del self._graph_builder
        return llh, input_variables

