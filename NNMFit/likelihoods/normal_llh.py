"""Implementation of gaussian llh"""
import logging
import scipy.stats
import theano.tensor as T
from theano import shared
logger = logging.getLogger(__name__)


class NormalLLH(object):
    """Construct computation graph for gaussian LLH"""

    def __init__(self, loader, data_hists,
                 profile=None):

        self._det_configs = loader.detector_configs
        self._profile = profile
        self._loader = loader

        self._datasets = data_hists
        self.poiss_llh = None
        self.fixed_pars = None
    # pylint: disable = too-many-arguments

    def sat_llh(self):
        """
        Return saturated LH
        """
        llh = 0
        return llh


    def make_llh(self):
        """
        Return graph for gaussian lh

        Args:

        """
        llh = -self.sat_llh()
        expec_hdlrs = self._loader.make_expectation_handlers()

        for det_config in self._det_configs:
            expec_graph = expec_hdlrs[det_config].\
                make_expectation_graph(binned_expectation=True)

            llh = llh - (T.sqr(self._datasets[det_config]-expec_graph)).sum()
        return llh

