"""Implementation of poissonian llh"""
import logging
import scipy.stats
import theano.tensor as T
from theano import shared, printing
logger = logging.getLogger(__name__)


class PoissonLLH(object):
    """Construct computation graph for poisson LLH"""

    def __init__(self, graph_builder, data_hists,
                 profile=None):
        self._det_configs = graph_builder.detector_configs
        self._profile = profile
        self._graph_builder = graph_builder

        self._datasets = data_hists
        self.poiss_llh = None
        self.fixed_pars = None
    # pylint: disable = too-many-arguments

    def sat_llh(self):
        """
        Return saturated LH
        """
        llh = 0
        for det_config in sorted(self._det_configs):
            dataset = self._datasets[det_config]
            llh += T.sum(dataset*(-1 + T.log(dataset+1E-300)))
        return llh.eval()


    def sat_binwise_llh(self):
        """
        Return binwise saturated LH
        """
        llh = 0
        for det_config in sorted(self._det_configs):
            dataset = self._datasets[det_config]
            llh += dataset*(-1 + T.log(dataset+1E-300))
        return llh.eval()

    def make_llh(self, graphs=None, input_variables=None):
        """
        Return graph for poissonian lh

        Args:
            graphs: Expectation graph for every det config
            input_variables: Input variables
        """
        llh = -self.sat_llh()
        if graphs is None or input_variables is None:
            graphs, input_variables = self._graph_builder.\
                    make_expectation_graph(binned_expectation=True)

        for det_config in sorted(self._det_configs):
            expec = graphs[det_config]

            llh = llh + (-expec +
                         self._datasets[det_config] *
                         T.log(expec+1E-300)).sum()
        del self._graph_builder
        return llh, input_variables

    def make_binwise_llh(self):
        """
        Return graph for binwise poissonian lh

        """
        llh = -self.sat_binwise_llh()
        graphs, input_variables = self._graph_builder.\
                make_expectation_graph(binned_expectation=True)

        for det_config in self._det_configs:
            expec_graph = graphs[det_config]
            llh += (-expec_graph +
                         self._datasets[det_config] *
                         T.log(expec_graph+1E-300))
        del self._graph_builder
        return llh, input_variables

