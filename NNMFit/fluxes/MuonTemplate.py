"""Implementation of MuonTemplate (Corsika) Flux"""
import logging
import tables
import numpy as np
import pickle
import theano.tensor as T
from .Flux import Flux
logger = logging.getLogger(__name__)


class MuonTemplate(Flux):
    """Make theano graph for corsika muon template flux"""

    def __init__(self, parameters, input_vars, **kwargs):
        super(MuonTemplate, self).__init__(parameters,
                                           input_vars,
                                           **kwargs)
        self._baseline_weight = kwargs['baseline_weights']
        self._add_req_input_vars = ['reco_energy', 'reco_zenith']#, 'TIntProbW']
        self._spline_file = kwargs["spline_file"]
        self._spline_obj = None
        logger.debug("Reading spline-file of the corsika muon-template: %s",
                     self._spline_file)
        with open(self._spline_file, "r") as spfile:
            spline_dict = pickle.load(spfile)
            self._spline_obj = spline_dict["spline_object_renormed_to_statistics"]
            #self._spline_obj = spline_dict["spline_object"]
            self._total_rate_norm = spline_dict["total_rate_normalization/Hz"]

    def make_graph(self, variables, alignment_values=None):
        """
        Make symbolic graph by multiplying baseline weights
        with reweighting factors for each parameter

        Args:
            variables: dict{var_name: shared_variables)
            alignment_values: dict of tuples (alignment_value, alignment_type)
        """

        graph = super(MuonTemplate, self).make_graph(variables, alignment_values)
        # get rid of powerlaw (i.e. baselineweights including generator and TintProbW)
        graph /= (variables[self._baseline_weight])

        #evaluate spline (rate of Corsika KDE)
        logger.debug("Evaluating spline of muon-template KDE")
        templ_weights =\
                self._spline_obj(np.log10(variables["reco_energy"].get_value()),
                                 np.cos(variables["reco_zenith"].get_value()),
                                 grid=False)
        weights = templ_weights*graph
        #renomormalize to correct rate from Corsika weighting
        #cond = T.gt(weights.sum(),0.)
        #weights = T.switch(cond, weights/weights.sum(), weights*0.0)
        #weights *= self._total_rate_norm
        logger.debug("Total rate normalization: %s",
                     np.sum(weights.eval()))

        return weights

