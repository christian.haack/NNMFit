""" Implementation of Flux class"""
import sys
import logging
import flux_hooks

logger = logging.getLogger(__name__)


class Flux(object):
    """
    Baseclass for Fluxes

    This class allows the construction of a computation graph to calculate
    a flux based on some configurable parameters.
    Needs to be subclassed.
    """
    # pylint: disable=unused-argument
    def __init__(self, parameters, input_vars, **kwargs):
        """
        Constructor

        Args:
            parameters: dict{param_name: param_obj}

        Kwargs:
            May be used in subclass
        """


        self.uses_3d = None  # Needs to be set by subclass
        self._parameters = parameters
        self._baseline_weight = None  # Needs to be set by subclass
        self._add_req_input_vars = []
        self._input_vars = input_vars
        self._hooks = kwargs["hooks"] if "hooks" in kwargs.keys() else None

    def get_list_of_req_vars(self):
        """
        Return set of variables required for the parameters.

        The parameters may depend on certain MC information, such as
        true_energy, true_zenith, ...
        This function loops through all parameters and extracts the required
        variables.
        """
        req_vars = []
        for par_name, par in self._parameters.iteritems():
            try:
                req_vars += (par.used_vars)
            except TypeError:
                print "It seems like the parameter {} didn't implement"\
                      " `used_vars`".format(par_name)
                sys.exit(1)
        req_vars.append(self._baseline_weight)
        req_vars += self._add_req_input_vars
        return set(req_vars)

    def get_input_variables(self):
        """Return list of input variables (theano scalars)"""
        return self._input_vars.values()

    def apply_hooks_for_flux(self, variables):
        '''
        Apply reweighting on baseline weights
        '''
        for hook in self._hooks:
            logging.debug("Apply hook %s to flux, Adjusting baseline weights.", hook)
            hook_cls = getattr(flux_hooks, hook)
            hook_obj = hook_cls(self._baseline_weight)
            variables = hook_obj.apply_mod(variables)
        return variables

    def make_graph(self,
                   variables,
                   alignment_values=None):
        """
        Make symbolic graph by multiplying baseline weights
        with reweighting factors for each parameter

        Args:
            variables: dict{var_name: shared_variables)
            alignment_values: dict of tuples (alignment_value, alignment_type)
        """


        if self._hooks is not None:
            variables = self.apply_hooks_for_flux(variables)

        # Starting point of the graph is the baseline weight
        graph = variables[self._baseline_weight]
        for param_name, param in self._parameters.iteritems():
            in_var_t = self._input_vars[param_name]
            if alignment_values is not None and\
               param_name in alignment_values:
                if alignment_values[param_name][1] == "additive":
                    in_var_t += alignment_values[param_name][0]
                else:
                    in_var_t *= alignment_values[param_name][0]
            logger.debug("Added parameter to graph %s", param_name)
            graph *= param.make_graph(in_var_t, variables)
        return graph
