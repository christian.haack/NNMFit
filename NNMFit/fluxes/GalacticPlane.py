"""Implementation of GalacticPlane Flux"""
import logging
import tables
import numpy as n
import healpy as hp
import theano.tensor as T
from .Flux import Flux
from ..caching import Cache
logger = logging.getLogger(__name__)


class PlaneModel(object):
    """
    Base class for galactic plane models
    """

    def __init__(self, plane_model_file, **kwargs):
        with tables.open_file(plane_model_file) as hdf:
            self.flux = hdf.root.flux.col("value")[:]

    @staticmethod
    def gal_to_healpy(lon, lat):
        """Transforms [-pi,pi], [-pi/2,pi/2] to [0,2pi],[0,pi]"""
        theta = -lat + n.pi / 2.0
        phi = T.switch(T.lt(lon, 0), lon + 2 * n.pi, lon).eval()
        return (theta, phi)

class KRAGamma(PlaneModel):
    """
    Kra_gamma template
    """
    def __init__(self, plane_model_file, **kwargs):
        super(KRAGamma, self).__init__(plane_model_file, **kwargs)
        self.nside = 128
        self.npix = 12 * self.nside ** 2
        self.n_bins_energy = 170
        self.e_min = 10
        self.e_max = 1E8
        self.e_fact = 1.1
        self.energy_binning = n.logspace(1, 8, self.n_bins_energy)

    def get_flux(self, lon, lat, energy):
        try:
            lon = lon.get_value()
            lat = lat.get_value()
            energy = energy.get_value()
        except:
            pass
        theta, phi = self.gal_to_healpy(lon, lat)
        pix = hp.ang2pix(self.nside, theta, phi)
        energy_bins = n.digitize(energy, self.energy_binning)-1

        # TODO: raise warning
        energy_bins[energy_bins == -1] = 0
        energy_bins[energy_bins == self.n_bins_energy] = self.n_bins_energy-1

        flux = self.flux[energy_bins*self.npix + pix]
        energy_bins_upper = energy_bins + 1
        energy_bins_upper[energy_bins_upper == self.n_bins_energy] = self.n_bins_energy-1
        next_flux = self.flux[energy_bins_upper*self.npix + pix]

        # ASSUME UNIFORM BINNING
        delta_loge = n.log(self.energy_binning[1]) - n.log(self.energy_binning[0])
        delta_logflux = n.log(next_flux) - n.log(flux)
        logenergy_null = n.log(self.energy_binning[energy_bins])
        interpolation = n.exp(n.log(flux) + (delta_logflux / delta_loge *
                                             (n.log(energy)-logenergy_null)))
        interpolation[~n.isfinite(interpolation)] = 0

        # RETURN PER FLAVOR FLUX
        interpolation *= 0.5

        return interpolation

class FermiHealpix(PlaneModel):
    """Fermi spatial template on healpix_grid"""

    def __init__(self, plane_model_file, **kwargs):
        super(FermiHealpix, self).__init__(plane_model_file, **kwargs)
        self.nside = 128
        self.npix = 12 * self.nside ** 2


    def get_spatial_flux(self, lon, lat):
        """Return spatial pdf evaluated at lon, lat"""
        theta, phi = self.gal_to_healpy(lon, lat)
        pix = hp.ang2pix(self.nside, theta.eval(), phi.eval())
        return self.flux[pix]


class GalacticPlaneBase(Flux):
    """Make theano graph for galactic plane flux"""

    def __init__(self, parameters, input_vars, **kwargs):
        super(GalacticPlaneBase, self).__init__(parameters,
                                                input_vars,
                                                **kwargs)
        self._baseline_weight = kwargs['baseline_weights']
        self._add_req_input_vars = ['true_lon', 'true_lat', 'true_energy']
        self.uses_3d = True

        if kwargs['plane_model'] not in globals():
            raise RuntimeError(
                'Could not find %s class', kwargs['plane_model'])
        plane_model_cls = globals()[kwargs['plane_model']]
        self._plane_model = plane_model_cls(**kwargs)
        self._precalc_spatial = {}

        self._oversampling = bool("oversampling" in kwargs)

    def make_graph(self, variables, alignment_values=None):
        """
        Make symbolic graph by multiplying baseline weights
        with reweighting factors for each parameter

        Args:
            variables: dict{var_name: shared_variables)
            split_index: index where variables are split into anti_nu/nu
            alignment_values: dict of tuples (alignment_value, alignment_type)
        """

        ident = Cache.generate_ident(
            [self.__class__.__name__, variables["true_lat"].get_value(borrow=True)])
        if ident not in self._precalc_spatial:
            logger.debug("Precalculating galactic plane spatial flux for ident"
                         " %s", ident)
            if hasattr(self._plane_model, "get_spatial_flux"):
                precalc = self._plane_model.get_spatial_flux(
                    variables["true_lon"], variables["true_lat"])
            else:
                precalc = self._plane_model.get_flux(
                    variables["true_lon"], variables["true_lat"], variables["true_energy"])
            precalc = precalc * 1E8 * variables['true_energy'] ** 2

            self._precalc_spatial[ident] = precalc
        graph = self._precalc_spatial[ident]*super(GalacticPlaneBase, self).\
            make_graph(variables, alignment_values)
        return graph

class FermiGalacticPlane(GalacticPlaneBase):
    """
    Fermi pi0 galactic plane model
    """
    def __init__(self, parameters, input_vars, **kwargs):
        super(FermiGalacticPlane, self).__init__(
            parameters, input_vars, **kwargs)

        self._baseline_norm = float(kwargs['baseline_norm'])

    def make_graph(self, variables, alignment_values=None):
        """
        Make symbolic graph by multiplying baseline weights
        with reweighting factors for each parameter

        Args:
            variables: dict{var_name: shared_variables)
            split_index: index where variables are split into anti_nu/nu
            alignment_values: dict of tuples (alignment_value, alignment_type)
        """
        graph = super(FermiGalacticPlane, self).make_graph(
            variables, alignment_values)
        graph *= self._baseline_norm
        return graph


class IngelmanSpatial(object):
    """Ingelman spatial template"""

    R_MW = 12.0  # radius galaxy / kpc
    R_SC = 8.5  # distance sun GC / kpc
    rho0 = 1.  # average ism density / nucleons / cm^3
    h0 = 0.26  # exponential density decrease / kpc
    E0 = 4.7e5

    def __init__(self, *args, **kwargs):
        pass

    @classmethod
    def rmax(cls, lon):
        """Maximum radius for longitude"""
        return n.sqrt(cls.R_MW*cls.R_MW -
                      n.sin(lon)*n.sin(lon)*cls.R_SC*cls.R_SC) +\
            cls.R_SC*n.cos(lon)

    def get_spatial_flux(self, lon, lat):
        """Return spatial pdf evaluated at lon, lat"""

        rmax_ = self.rmax(lon)
        flux = T.exp(
            T.log(self.rho0*self.h0/T.sin(abs(lat))) +
            T.log((1.-n.exp(-rmax_*T.tan(abs(lat))/self.h0))))

        flux_inner = self.rho0*rmax_
        flux_pole = self.rho0*self.h0

        flux = T.switch(abs(lat) < 1e-12, flux_inner, flux)
        flux = T.switch(abs(lat-n.pi/2.) < 1e-12, flux_pole, flux)
        return flux


class IngelmanGalacticPlane(GalacticPlaneBase):
    """
    Inglemann galactic plane model
    """
    def __init__(self, parameters, input_vars, alignment_values, **kwargs):
        super(IngelmanGalacticPlane, self).__init__(
            parameters, input_vars, alignment_values, **kwargs)

    @staticmethod
    def ingel_spectrum(self, variables):
        energy = variables["true_energy"]
        below_cutoff = 3e-6*T.pow(energy, -(1.63+1.)) / 2.
        above_cutoff = 1.9e-4*T.pow(energy, -(1.95+1.)) / 2.
        reweight = T.switch(
            T.lt(energy, IngelmanSpatial.E0),
            below_cutoff,
            above_cutoff)
        return reweight

    def make_graph(self, variables, alignment_values=None):
        """
        Make symbolic graph by multiplying baseline weights
        with reweighting factors for each parameter

        Args:
            variables: dict{var_name: shared_variables)
            split_index: index where variables are split into anti_nu/nu
            alignment_values: dict of tuples (alignment_value, alignment_type)
        """
        graph = super(IngelmanGalacticPlane, self).make_graph(
            variables, alignment_values)

        return graph*self.ingel_spectrum(variables)
