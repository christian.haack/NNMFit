"""Implementation of a predicted flux for the astrophysical component"""
import logging
import cPickle as pickle
import numpy as n
import healpy as hp
import theano.tensor as T
from .Flux import Flux
from ..caching import Cache
logger = logging.getLogger(__name__)


class AstroPrediction(Flux):
    """Make theano graph for astro fluxes"""

    def __init__(self, parameters, input_vars, **kwargs):
        super(AstroPrediction, self).__init__(parameters,
                                            input_vars,
                                            **kwargs)
        self._baseline_weight = kwargs['baseline_weights']
        self._add_req_input_vars = ['true_energy']
        self._astro_pred_cfg = kwargs['astro_pred_cfg']
        self._reference_index = float(kwargs["reference_index"])
        self.uses_3d = False
        """Read the cfg with the polynomial fit"""
        with open(self._astro_pred_cfg) as cfg:
            self.poly_cfg = pickle.load(cfg)
            self.poly_pars = self.poly_cfg["polynom_pars"]
            self.energy_range = (self.poly_cfg["energy_range"][0],
                                 self.poly_cfg["energy_range"][1])
            logger.debug("Succesfully read the loglog-polynomial fit result"
                         " %s", self._astro_pred_cfg)

    def make_graph(self, variables, alignment_values=None):
        """
        Make symbolic graph by multiplying baseline weights
        with reweighting factors for each parameter

        Args:
            variables: dict{var_name: shared_variables)
            split_index: index where variables are split into anti_nu/nu
            alignment_values: dict of tuples (alignment_value, alignment_type)
        """
        graph = super(AstroPrediction, self).make_graph(variables,
                                                        alignment_values)
        true_e = variables["true_energy"]

        #build the polynomial (which was log10(E**2 flux) vs log10(E)
        #flux = 10**(poly(logE)) / E**2
        polynomial = 0.0
        for i, coeff in enumerate(self.poly_pars[::-1]):
            polynomial +=  coeff*(T.log10(true_e)**(1.*i))

        remove_powerlaw_weights_factor = 1e8*(true_e)**self._reference_index
        pred_from_loglog_polynom = \
                remove_powerlaw_weights_factor*10**(polynomial) / (true_e**(2.))
        cond = T.and_(T.gt(true_e, self.energy_range[0]),
                      T.le(true_e, self.energy_range[1]))

        #outside the splined energy range, the flux is set to zero
        reweight = T.switch(cond, pred_from_loglog_polynom, 0.0)
        graph *= reweight
        return graph

