"""Implementation of Powerlaw Flux with limited energy range"""
from .Flux import Flux
from theano import tensor as T

class PowerlawLimRange(Flux):
    """Make theano graph for powerlaw"""

    def __init__(self, parameters, input_vars, **kwargs):
        super(PowerlawLimRange, self).__init__(parameters, input_vars, **kwargs)
        self._baseline_weight = kwargs["baseline_weights"]
        self._add_req_input_vars = ['true_energy']
        self.uses_3d = False
        self._e_lower = float(kwargs["e_lower"])
        self._e_upper = float(kwargs["e_upper"])

    def make_graph(self, variables, alignment_values=None):

        """
        Make symbolic graph by multiplying baseline weights
        with reweighting factors for each parameter

        Args:
            variables: dict{var_name: shared_variables)
            split_index: index where variables are split into anti_nu/nu
            alignment_values: dict of tuples (alignment_value, alignment_type)
        """
        graph = super(PowerlawLimRange, self).make_graph(
            variables,
            alignment_values)

        true_e = variables["true_energy"]
        graph *= T.switch(T.gt(true_e, self._e_lower), 1, 0 )
        graph *= T.switch(T.lt(true_e, self._e_upper), 1, 0 )
        return graph
