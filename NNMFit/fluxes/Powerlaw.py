"""Implementation of Powerlaw Flux"""
from .Flux import Flux


class Powerlaw(Flux):
    """Make theano graph for powerlaw"""

    def __init__(self, parameters, input_vars, **kwargs):
        super(Powerlaw, self).__init__(parameters, input_vars, **kwargs)
        self._baseline_weight = kwargs["baseline_weights"]
        self.uses_3d = False
