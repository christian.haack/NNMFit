import numpy as n
import cPickle as pickle


class OscillationsHook(object):
    """
    Class implementing a reweighting to correct for neutrino oscillations
    """

    IS_SYS_PARAM = False

    def __init__(self, baseline_weights):
        self.baseline_weights = baseline_weights
        self.spline_file = "/data/user/jstettner/software/nuCraft/NuCraft_NuMu_ptype14SurvivialProb.pickle"

    def apply_mod(self, variables_dict):
        """
        Apply the modifications to the data dict

        Args:
            data_dict: Data dict from Dataset object
        """

        with open(self.spline_file) as hdl:
            spline_dict = pickle.load(hdl)

        true_zeniths = variables_dict["true_zenith"].eval()
        true_energies = variables_dict["true_energy"].eval()

        variables_dict[self.baseline_weights] *= (spline_dict["Spline_SurvProb_NuMu"]\
                                                 (n.log10(true_energies),
                                                  true_zeniths,
                                                  grid=False))

        del true_zeniths
        del true_energies
        return variables_dict

class NuSquidsHook(object):
    """
    Class implementing a reweighting to correct for neutrino absorption
    Expects two splines: One for the survival probability assuming 1xcsms
    and one for the altered cross-section (scaled or energy dependent)
    Both should be provided in the same pickle file
    """

    IS_SYS_PARAM = False

    def __init__(self, baseline_weights):
        self.baseline_weights = baseline_weights
        self.spline_file = "/data/user/jstettner/software/nuSQuIDS/examples/Carlos_XsectionScaling/NuSquids_survivalRatio_splines.pickle"
        self.spline_alt_key = "csmsx1.2"

    def apply_mod(self, variables_dict):
        """
        Apply the modifications to the data dict
        Caution: Right now, the splines are forced to 1. at highE to avoid
        numerical problems with NuSquids
        Args:
            variables_dict: Data dict from Dataset object
        """

        with open(self.spline_file) as hdl:
            spline_obj = pickle.load(hdl)
            spline_base = spline_obj["splines"]["csmsx1.0"]
            spline_alt = spline_obj["splines"][self.spline_alt_key]

        true_zeniths_deg = 180./n.pi*variables_dict["true_zenith"].eval()
        true_energies_atDetector = variables_dict["MCPrimaryEnergyInIce"].eval()

        survival_fract_base = spline_base(true_zeniths_deg,
                                          n.log10(true_energies_atDetector),
                                          grid=False)

        survival_fract_alt = spline_alt(true_zeniths_deg,
                                        n.log10(true_energies_atDetector),
                                        grid=False)

        del true_zeniths_deg
        del true_energies_atDetector
        variables_dict[self.baseline_weights] *= (survival_fract_alt / \
                                                  survival_fract_base)

        return variables_dict

