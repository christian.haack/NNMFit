"""Implementation of PromptAtmo flux"""
from .Flux import Flux


class PromptAtmo(Flux):
    """
    Make theano graph for conv. flux
    """

    def __init__(self, parameters, input_vars, **kwargs):
        super(PromptAtmo, self).__init__(parameters, input_vars, **kwargs)
        self._baseline_weight = kwargs["baseline_weights"]
        self.uses_3d = False
