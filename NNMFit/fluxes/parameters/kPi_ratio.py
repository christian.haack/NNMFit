"""Implementation of KPiRatio"""
import numpy as n
import theano.tensor as T
from .parameter import Parameter


class KPiRatio(Parameter):
    """
    kaon/pion ratio
    """

    # pylint: disable=invalid-name, too-many-arguments
    def __init__(self, baseline_weights):
        """

        Args:
            baseline_weights: Weights of baseline flux
        """
        super(KPiRatio, self).__init__()
        self.used_vars = ["true_energy",
                          "true_zenith",
                          baseline_weights,
                          "true_ptype"]
        self._baseline_weights = baseline_weights

    @staticmethod
    def _inclination(cos_zen):
        p0 = 0.102573
        p1 = -0.068287
        p2 = 0.958633
        p3 = 0.0407253
        p4 = 0.817285
        denominator = 1 + p0**2 + p1 + p3

        cos_zen = n.abs(cos_zen)
        return T.sqrt((cos_zen**2 + p0**2 + p1 * cos_zen**p2 +
                       p3 * cos_zen**p4)/denominator)

    def _make_gaisser_formula(self, A, B, epsilon, energy, cos_zen):
        # energy is  0, zenith is 1, weight_baseline is 2
        inc = self._inclination(cos_zen)

        return A / (1. + B * inc * energy / epsilon)

    def _kaon_pion_ratio(self, variables):

        params_kaon = {"A": 2.649640e-8,
                       "A_anti": 1.36649e-8,
                       "B": 1.18,
                       "epsilon": 850.}
        params_pion = {"A": 8.90369e-8,
                       "A_anti": 7.85269e-8,
                       "B": 2.77,
                       "epsilon": 115.}

        def make_gaisser_wrapper(params, a_type, energy, cos_zen):
            """Wrapper for gaisser formula for particle type a_type"""
            return self._make_gaisser_formula(params[a_type],
                                              params["B"],
                                              params["epsilon"],
                                              energy,
                                              cos_zen)
        # pylint: disable=assignment-from-no-return
        cos_zenith = T.cos(variables["true_zenith"])
        ptype = variables["true_ptype"]
        nu_mask = T.eq(ptype, 14).nonzero()
        antinu_mask = T.eq(ptype, -14).nonzero()
        energy_nu = variables["true_energy"][nu_mask]
        cos_zenith_nu = cos_zenith[nu_mask]
        energy_anti_nu = variables["true_energy"][antinu_mask]
        cos_zenith_anti_nu = cos_zenith[antinu_mask]

        gf_pions = make_gaisser_wrapper(params_pion, "A", energy_nu,
                                        cos_zenith_nu)
        gf_anti_pions = make_gaisser_wrapper(params_pion, "A_anti",
                                             energy_anti_nu,
                                             cos_zenith_anti_nu)
        gf_kaons = make_gaisser_wrapper(params_kaon, "A", energy_nu,
                                        cos_zenith_nu)
        gf_anti_kaons = make_gaisser_wrapper(params_kaon, "A_anti",
                                             energy_anti_nu,
                                             cos_zenith_anti_nu)

        return gf_kaons / gf_pions, gf_anti_kaons / gf_anti_pions

    def make_graph(self, par_t, variables):
        """Return theano graph"""
        std_kPi_ratio_nu, std_kPi_ratio_anti_nu = self._kaon_pion_ratio(
            variables)

        std_kPi_ratio_nu = std_kPi_ratio_nu.eval()
        std_kPi_ratio_anti_nu = std_kPi_ratio_anti_nu.eval()

        def reweight_for_type(par_t, weight, std_ratio):
            """Reweighting graph for given weight"""
            reweight = (std_ratio*par_t+1) / (std_ratio + 1.)
            norm = weight.sum() / (reweight*weight).sum()
            return reweight*norm

        weight = variables[self._baseline_weights]
        ptype = variables["true_ptype"]
        nu_mask = T.eq(ptype, 14).nonzero()
        antinu_mask = T.eq(ptype, -14).nonzero()
        reweight = T.zeros_like(weight)
        reweight = T.set_subtensor(
            reweight[nu_mask],
            reweight_for_type(par_t, weight[nu_mask],
                              std_kPi_ratio_nu))
        reweight = T.set_subtensor(
            reweight[antinu_mask],
            reweight_for_type(par_t, weight[antinu_mask],
                              std_kPi_ratio_anti_nu))
        return reweight
