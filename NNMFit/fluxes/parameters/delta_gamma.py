"""Implementation of DeltaGamma"""
from ...utilities import weighted_median
from .parameter import Parameter


class DeltaGamma(Parameter):
    """
    Variation of spectral index for conv flux

    The spectral index is variied with median(true_energy)
    beeing the anchor point
    """

    def __init__(self, baseline_weights):
        super(DeltaGamma, self).__init__()
        self.used_vars = ["true_energy", baseline_weights]
        self._baseline_weights = baseline_weights

    def make_graph(self, par_t, variables):
        """
        Return theano graph
        """
        # TODO: tensor-friendly weighted median implementation
        w_median = weighted_median(
            variables["true_energy"].get_value(),
            variables[self._baseline_weights].eval())
            #variables[self._baseline_weights].get_value())
        # pylint: disable=invalid-name
        trueE_norm_median = variables["true_energy"] / w_median

        reweight = trueE_norm_median**(-par_t)
        return reweight
