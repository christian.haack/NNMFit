"""Implements base class for flux parameters"""


class Parameter(object):
    """Base class for flux parameters"""
    def __init__(self):
        self.used_vars = None  # Needs to be implemented by subclass

    def make_graph(self, par_t, variables):
        """
        Return theano graph

        Args:
            par_t: theano variable for parameters
            variables: dict of aux. variables (must contain vars. declared in
                       self._used_vars
        """
        raise NotImplementedError()
