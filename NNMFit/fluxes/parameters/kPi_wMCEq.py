"""Implementation of KPiRatio bases ond separate weights for K and pi fluxes"""
import numpy as n
import theano.tensor as T
from .parameter import Parameter


class KPiMCEq(Parameter):
    """
    kaon/pion ratio based on separate weights obtained e.g. from mceq
    """

    def __init__(self, baseline_weights,
                 rest_weights,
                 kaon_weights,
                 pion_weights):
        """

        Args:
            baseline_weights: Weights of baseline flux
            pion_weights: Weights of conv flux originating from pions
            kaon_weights: Weights of conv flux originating from kaons
            rest_weights: Weights of conv flux originating from other particles
        """
        super(KPiMCEq, self).__init__()
        self.used_vars = [baseline_weights,
                          rest_weights,
                          kaon_weights,
                          pion_weights]
        self._baseline_weights = baseline_weights
        self._rest_weights = rest_weights
        self._kaon_weights = kaon_weights
        self._pion_weights = pion_weights


    def make_graph(self, kpi_par, variables):
        """Return theano graph"""
        conv_weights = variables[self._baseline_weights]
        rest_weights = variables[self._rest_weights]
        kaon_weights = variables[self._kaon_weights]
        pion_weights = variables[self._pion_weights]

        alt_weights = rest_weights + (1.+kpi_par)*kaon_weights + (1.-kpi_par)*pion_weights
        norm_ratio = conv_weights.sum()/alt_weights.sum()

        cond = T.gt(conv_weights, 0.)
        reweight = T.switch(cond, (alt_weights/conv_weights)*norm_ratio , 0.0)

        return reweight
