"""Implementation of SPL for given energy range"""
from theano import tensor as T
from .parameter import Parameter

class SpectralIndexRestrictedE(Parameter):
    """
    Single Powerlaw in a restricted energy range (trueE)
    Binedges are defined in parameters.yaml
    """

    def __init__(self, **kwargs):
        super(SpectralIndexRestrictedE, self).__init__()
        self.used_vars = ["true_energy"]
        self.e_up = float(kwargs["binedge_up"])
        self.e_low = float(kwargs["binedge_low"])
        self.reference_index = float(kwargs["reference_index"])

    def make_graph(self, gamma_restricted, variables):
        """Return theano graph"""

        true_e = variables["true_energy"]
        powerlaw_fact = (true_e/1E5)**(self.reference_index-gamma_restricted)
        cond = T.and_(T.gt(true_e, self.e_low),T.le(true_e, self.e_up))
        reweight = T.switch(cond, powerlaw_fact, 1.0)
        return reweight
