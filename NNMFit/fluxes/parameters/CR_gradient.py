"""Implementation of a linear gradient between two CR models"""
from .parameter import Parameter
from theano import tensor as T

class CRGrad(Parameter):
    """
    Changes the flux weight linearly according to the differential between two
    CR models
    """

    def __init__(self,
                 baseline_weights,
                 alternative_weights):
        """

        Args:
            baseline_weights: CR model basline
            alternative_weights: CR model alternative
        """
        super(CRGrad, self).__init__()
        self.used_vars = [baseline_weights, alternative_weights]
        self._baseline_weights = baseline_weights
        self._alt_weights = alternative_weights

    def make_graph(self, cr_grad, variables):
        """Return theano graph"""
        base_w = variables[self._baseline_weights]
        alt_w = variables[self._alt_weights]

        # The graph includes weights_baseline per default
        updated_w = (base_w + cr_grad * (alt_w - base_w) ) / base_w

        #preserve the norm:
        #reweight = base_w.sum()/updated_w.sum()*updated_w
        return updated_w


class ThreePointInterpolate(Parameter):
    '''Interpolate between three predictions
    param = -1 -> A, 0 -> B , 1 -> C
    '''

    def __init__(self,
                 baseline_weights,
                 altA_weights,
                 altC_weights):
        """
        Args:
            baseline_weights: Baseline weights
            altA_weights: alternative weights A 
            altC_weights: alternative weights C
        """
        super(ThreePointInterpolate, self).__init__()
        self.used_vars = [baseline_weights, altA_weights, altC_weights]
        self._baseline_weights = baseline_weights
        self._altA = altA_weights
        self._altC = altC_weights

    def make_graph(self, interp_param, variables):
        """Return theano graph"""
        base_w = variables[self._baseline_weights]
        altA = variables[self._altA]
        altC = variables[self._altC]

        # The graph includes weights_baseline per default
        updated_w_gtzero = (interp_param*altC + (1.-interp_param)*base_w) / base_w
        updated_w_ltzero = (-1.*interp_param*altA + (1.+interp_param)*base_w) / base_w

        cond = T.gt(interp_param, 0.)
        reweight = T.switch(cond, updated_w_gtzero, updated_w_ltzero)
        return reweight


