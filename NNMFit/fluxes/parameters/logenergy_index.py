"""Implementation of a SpectralIndex with log(E) weighting"""
from .parameter import Parameter
from theano import tensor as T

class LogEnergyIndex(Parameter):
    """
    Variation of energy dependent spectral index for log-parabola

    The spectral index is varied with 100TeV
    beeing the anchor point
    """

    def __init__(self, **kwargs):
        super(LogEnergyIndex, self).__init__()
        self.used_vars = ["true_energy"]

    def make_graph(self, astro_parabola_b, variables):
        """Return theano graph"""
        reweight = (variables["true_energy"]/1E5)**\
                (-1.*astro_parabola_b*T.log10(variables["true_energy"]/1E5))
        return reweight
