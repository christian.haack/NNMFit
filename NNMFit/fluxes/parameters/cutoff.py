"""Implementation of cutoff parameter"""
import theano.tensor as T
from .parameter import Parameter


class Cutoff(Parameter):
    """
    Exponential Cutoff
    cutoff_pos defined as log10(E_cutoff)
    """

    def __init__(self, **_):
        super(Cutoff, self).__init__()
        self.used_vars = ["true_energy"]

    def make_graph(self, cutoff_pos, variables):
        """Return theano graph"""

        return T.exp(-variables["true_energy"]/10**cutoff_pos)
