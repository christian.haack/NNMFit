import numpy as n
import dashi as d
from itertools import product
from kde import cudakde

def eval_mirrored(kde, xs, ys, bounds, thresholds=(None,None)):
    zs = kde([xs,ys])

    bX, bY = bounds
    tX, tY = thresholds

    def makeMask(b, vals, t):
        return_vals = []
        if b:
            bLow,bUp = b
            if bLow:
                mirror_mask = n.zeros(len(vals), dtype=n.bool)
                if t and t[0]:
                    mirrored_bound = 2*bLow-t[0]

                    mirror_mask |= ((vals<mirrored_bound) & (vals > bLow))
                else:
                    mirror_mask = n.ones(len(vals), dtype=n.bool)

                mirrored_vals = 2*bLow-vals[mirror_mask]
                return_vals.append((mirror_mask,mirrored_vals))

            if bUp:
                if t and t[1]:
                    mirror_mask = n.zeros(len(vals), dtype=n.bool)
                    #Threshold-Value for bXUp
                    mirrored_bound = 2*bUp-t[1]
                    mirror_mask |= ((vals>mirrored_bound) & (vals < bUp))
                else:
                    mirror_mask &= n.ones(len(vals), dtype=n.bool)
                mirrored_vals = 2*bUp-vals[mirror_mask]
                return_vals.append((mirror_mask,mirrored_vals))
            return return_vals
        else:
            return None

    ret = makeMask(bX, xs, tX)
    if ret:
        for (mask, vals) in ret:
            zs[mask] += kde([vals,ys[mask]])

    ret = makeMask(bY, ys, tY)
    if ret:
        for (mask, vals) in ret:
            zs[mask] += kde([xs[mask],vals])
    return zs

def make_transfer_tensor(x, y, reco_x, reco_y, weights,
                         binning, binning_reco, mode="hist",
                         **kwargs):
    """
    Construct transfer tensor.

    This function constructs a transfer tensor such,
    that true_hist*tensor = reco_hist.

    Args:
        x: First true variable
        y: Second true variable
        reco_x: First reco variable
        reco_y: Second reco variable
        binning: tuple of binedges (x,y)
        binning_reco: tuple of binedges (reco_x, reco_y)
    """
    n_bins_x = len(binning[0])-1
    n_bins_y = len(binning[1])-1
    n_bins_x_reco = len(binning_reco[0])-1
    n_bins_y_reco = len(binning_reco[1])-1
    transfer_tensor = n.zeros((n_bins_x,
                               n_bins_y,
                               n_bins_x_reco,
                               n_bins_y_reco))
    bcs_reco = (0.5*(binning_reco[0][1:]+binning_reco[0][:-1]),
                0.5*(binning_reco[1][1:]+binning_reco[1][:-1]))
    binwidths_reco = (binning_reco[0][1] - binning_reco[0][0],
                      binning_reco[1][1] - binning_reco[1][0])
    indices_x = n.digitize(x,binning[0])-1
    indices_y = n.digitize(y,binning[1])-1

    mask_union = n.zeros(len(reco_x), dtype=n.bool)
    for (i,j) in product(xrange(n_bins_x),xrange(n_bins_y)):
        mask = (indices_x==i) & (indices_y==j)
        sub_reco_x = reco_x[mask]
        sub_reco_y = reco_y[mask]
        mask_union = n.logical_xor(mask_union, mask)
        if mode == "hist":
            sub_hist = d.factory.hist2d((sub_reco_x, sub_reco_y),
                                    bins = binning_reco,
                                    weights = weights[mask])

            sub_hist = sub_hist.normalized(density=False)
            transfer_tensor[i,j,...] = sub_hist.bincontent
        elif mode == "kde":
            try:
                sub_kde = cudakde.gaussian_kde((sub_reco_x, sub_reco_y),
                                                weights = weights[mask],
                                                use_cuda=True, **kwargs)
                xs, ys = n.meshgrid(*bcs_reco)

                bounds = (None,(-1, n.cos(n.radians(85))))
                thresholds = (None, (-1.3, 0.3))
                #sub_kde_eval =sub_kde((xs.ravel(), ys.ravel()))
                sub_kde_eval = eval_mirrored(sub_kde, xs.ravel(), ys.ravel(), bounds, thresholds)
                sub_kde_eval[n.isnan(sub_kde_eval)] = 0
                sub_kde_eval = sub_kde_eval.reshape((n_bins_y_reco,
                                                    n_bins_x_reco)).T
            except n.linalg.LinAlgError:
                sub_kde_eval = n.zeros((n_bins_x_reco, n_bins_y_reco))
            transfer_tensor[i,j,...] = sub_kde_eval
        else:
            raise NotImplementedError()
    return transfer_tensor
