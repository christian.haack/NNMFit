"""Implementation of Piecewise Norms (each a bin with E-2)"""
from theano import tensor as T
from .parameter import Parameter

class PiecewiseNorm(Parameter):
    """
    Variation of norm in a trueE bin of the piecewise flux
    In each bin of trueE, events are reweightes from the reference
    powerlaw (y=2 was simulated) to a powerlaw with y=index_in_bin

    Binedges are defined in parameters.yaml
    """

    def __init__(self, **kwargs):
        super(PiecewiseNorm, self).__init__()
        self.used_vars = ["true_energy"]
        self.e_up = float(kwargs["binedge_up"])
        self.e_low = float(kwargs["binedge_low"])
        self._is_per_flavor_norm = kwargs.get("per_flavor_norm", True)
        self.index_in_bin = float(kwargs["index_in_bin"])
        self.reference_index = float(kwargs["reference_index"])

    def make_graph(self, piece_norm, variables):
        """Return theano graph"""

        if not self._is_per_flavor_norm:
            """
            Weights calculated by the weighting project
            correspond to *per particle type* fluences.
            The precalculated fluxes therefore are also per particle
            type fluxes. If we want the normalization to be particle +
            antiparticle we have to multiply by 0.5
            """
            piece_norm = 0.5*piece_norm

        true_e = variables["true_energy"]
        norm_fact = piece_norm
        powerlaw_fact = (true_e/1E5)**(self.reference_index-self.index_in_bin)
        cond = T.and_(T.gt(true_e, self.e_low),T.le(true_e, self.e_up))
        reweight = T.switch(cond, norm_fact*powerlaw_fact, 1.0)
        return reweight

class RestrictedNorm(Parameter):
    """
    Variation of norm to be restrictred in trueE.
    Events outside of the energy-range are weighted to zero!
    Binedges are defined in parameters.yaml
    """

    def __init__(self, **kwargs):
        super(RestrictedNorm, self).__init__()
        self.used_vars = ["true_energy"]
        self.e_up = float(kwargs["binedge_up"])
        self.e_low = float(kwargs["binedge_low"])
        self._is_per_flavor_norm = kwargs.get("per_flavor_norm", True)

    def make_graph(self, restricted_norm, variables):
        """Return theano graph"""

        if not self._is_per_flavor_norm:
            """
            Weights calculated by the weighting project
            correspond to *per particle type* fluences.
            The precalculated fluxes therefore are also per particle
            type fluxes. If we want the normalization to be particle +
            antiparticle we have to multiply by 0.5
            """
            restricted_norm = 0.5*restricted_norm

        true_e = variables["true_energy"]
        cond = T.and_(T.gt(true_e, self.e_low),T.le(true_e, self.e_up))
        reweight = T.switch(cond, restricted_norm, 1.0)
        return reweight
