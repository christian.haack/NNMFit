"""Implementation of the spectral index, depending on the break energy"""
from theano import tensor as T
from .parameter import Parameter

class BrokenSpectralIndex(Parameter):
    """
    This parameter will be created two times. With E<Ebreak for the first
    component and vice versa. This has to be defined in the parameter settings.
    """

    def __init__(self, **kwargs):
        super(BrokenSpectralIndex, self).__init__()
        self.used_vars = ["true_energy"]
        self.e_break = float(kwargs["break_energy"])
        self.n_component = kwargs["n_component"]
        self.reference_index = float(kwargs["reference_index"])

    def make_graph(self, par_val, variables):
        """Return theano graph"""

        true_e = variables["true_energy"]
        powerlaw_fact = (true_e/1E5)**(self.reference_index-par_val)
        if self.n_component=="first":
            cond = T.le(true_e, self.e_break)
        elif self.n_component=="second":
            cond = T.gt(true_e, self.e_break)
        ## renormalize to the same anchor point
        renormalize_fact = (self.e_break/1e5)**(par_val)
        reweight = T.switch(cond, powerlaw_fact*renormalize_fact, 1.0)
        return reweight


