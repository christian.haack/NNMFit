"""Implementation of hard energy cutoff parameter"""
import theano.tensor as T
from .parameter import Parameter


class EnergyCutLow(Parameter):
    """
    Lower hard energy cut
    """

    def __init__(self, **_):
        super(EnergyCutLow, self).__init__()
        self.used_vars = ["true_energy"]

    def make_graph(self, cutoff_pos, variables):
        """Return theano graph"""

        return T.nnet.nnet.sigmoid(
            100*(T.log10(variables["true_energy"])-cutoff_pos))

class EnergyCutHigh(Parameter):
    """
    Higher hard energy cut
    """

    def __init__(self, **_):
        super(EnergyCutHigh, self).__init__()
        self.used_vars = ["true_energy"]

    def make_graph(self, cutoff_pos, variables):
        """Return theano graph"""

        return T.nnet.nnet.sigmoid(
            100*(cutoff_pos-T.log10(variables["true_energy"])))
