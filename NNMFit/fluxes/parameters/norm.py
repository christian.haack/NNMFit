"""Implementation of a general normalization parameter"""
from .parameter import Parameter

class Norm(Parameter):
    """
    Normalization of a flux
    """

    def __init__(self, **kwargs):
        super(Norm, self).__init__()
        #self.used_vars = ["true_energy", "true_zenith", "event_id"]
        self.used_vars = ["true_energy", "true_zenith"]
        #self.used_vars = ["true_energy", "true_zenith", "MCPrimaryEnergyInIce"] # MCPrimaryInIce needed for NuSquids
        self._is_per_flavor_norm = kwargs.get("per_flavor_norm", True)

    def make_graph(self, norm, *_):
        """Return theano graph"""
        if not self._is_per_flavor_norm:
            """
            Weights calculated by the weighting project
            correspond to *per particle type* fluences.
            The precalculated fluxes therefore are also per particle
            type fluxes. If we want the normalization to be particle +
            antiparticle we have to multiply by 0.5
            """
            return 0.5*norm
        return norm


class ScaledNorm(Parameter):
    """
    Normalization of a flux. The additional par is a scale factor to have the
    parameters in similar orders of magintude
    """

    def __init__(self, **kwargs):
        super(ScaledNorm, self).__init__()
        self.used_vars = ["true_energy", "true_zenith"]
        self._is_per_flavor_norm = kwargs.get("per_flavor_norm", True)
        self._scale_factor = kwargs["scale_factor"]

    def make_graph(self, norm, *_):
        """Return theano graph"""
        if not self._is_per_flavor_norm:
            """
            Weights calculated by the weighting project
            correspond to *per particle type* fluences.
            The precalculated fluxes therefore are also per particle
            type fluxes. If we want the normalization to be particle +
            antiparticle we have to multiply by 0.5
            """
            return 0.5*norm
        return norm*self._scale_factor
