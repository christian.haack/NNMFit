"""Implementation of SpectralIndex"""
from .parameter import Parameter


class SpectralIndex(Parameter):
    """
    Variation of spectral index for powerlaw

    The spectral index is varied with 100TeV
    beeing the anchor point
    """

    def __init__(self, **kwargs):
        super(SpectralIndex, self).__init__()
        self.used_vars = ["true_energy"]
        self.reference_index = float(kwargs["reference_index"])

    def make_graph(self, par_t, variables):
        """Return theano graph"""
        reweight = (variables["true_energy"]/1E5)**(self.reference_index-par_t)
        return reweight
