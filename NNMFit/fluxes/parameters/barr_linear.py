"""Implementation of a linear interpolation of the Barr parameters' effect on
the flux weight"""
from .parameter import Parameter
from theano import tensor as T


class BarrLinear(Parameter):
    """
    Changes the flux weight linearly according to the derivative that was
    calculated beforehand (at the energy and zenith of the event)
    """

    def __init__(self,
                 baseline_weights,
                 slope_per_event):
        """

        Args:
            slope_per_event: First derivative of the flux w.r.t to the Barr
            parameter
        """
        super(BarrLinear, self).__init__()
        self.used_vars = [baseline_weights, slope_per_event]
        self._slope_per_event = slope_per_event
        self._baseline_weights = baseline_weights

    def make_graph(self, barr_par, variables):
        """Return theano graph"""
        base_w = variables[self._baseline_weights]
        slope = variables[self._slope_per_event]

        # The graph follows weights_baseline per default
        cond = T.gt(base_w, 0.)
        reweight = T.switch(cond, (base_w + barr_par * slope) / base_w , 0.0)
        #reweight = (base_w + barr_par * slope) / base_w

        return reweight
