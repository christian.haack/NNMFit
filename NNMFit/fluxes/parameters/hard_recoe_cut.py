"""Implementation of hard energy cutoff parameter"""
import theano.tensor as T
from .parameter import Parameter


class RecoEnergyCutLow(Parameter):
    """
    Lower hard energy cut
    """

    def __init__(self, **_):
        super(RecoEnergyCutLow, self).__init__()
        self.used_vars = ["reco_energy"]

    def make_graph(self, cutoff_pos, variables):
        """Return theano graph"""
        return T.nnet.nnet.sigmoid(
            100*(T.log10(variables["reco_energy"])-cutoff_pos))

class RecoEnergyCutHigh(Parameter):
    """
    Higher hard energy cut
    """

    def __init__(self, **_):
        super(RecoEnergyCutHigh, self).__init__()
        self.used_vars = ["reco_energy"]

    def make_graph(self, cutoff_pos, variables):
        """Return theano graph"""

        return T.nnet.nnet.sigmoid(
            100*(cutoff_pos-T.log10(variables["reco_energy"])))
