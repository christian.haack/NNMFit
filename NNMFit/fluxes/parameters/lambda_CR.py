"""Implementation of LambdaCR"""
from .parameter import Parameter


class LambdaCR(Parameter):
    """
    Norm-preserving interpolation between two primary CR flux models
    """

    def __init__(self,
                 baseline_weights,
                 alt_weights):
        """

        Args:
            baseline_weights: Weights of baseline model
            alt_weights: Weights of alt model
        """
        super(LambdaCR, self).__init__()
        self.used_vars = [baseline_weights, alt_weights]
        self._baseline_weights = baseline_weights
        self._alt_weights = alt_weights

    def make_graph(self, l_cr, variables):
        """Return theano graph"""
        base_w = variables[self._baseline_weights]
        alt_w = variables[self._alt_weights]

        sum_weights_baseline = base_w.sum()
        sum_weights_alt = alt_w.sum()

        delta_weights = base_w - alt_w
        delta_sum = sum_weights_baseline - sum_weights_alt

        expr1 = sum_weights_baseline / (l_cr * delta_sum + sum_weights_alt)
        expr2 = (l_cr * delta_weights + alt_w)

        # The graph includes weights_baseline per default
        reweight = expr1 * expr2 / base_w

        return reweight
