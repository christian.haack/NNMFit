"""Implementation of piecewise Powerlaw Flux : in each bin fixed gamma"""
from .Flux import Flux
from theano import tensor as T

class PiecewisePowerlaw(Flux):
    """Make theano graph for piecewise powerlaw"""

    def __init__(self, parameters, input_vars, **kwargs):
        super(PiecewisePowerlaw, self).__init__(parameters, input_vars, **kwargs)
        self._baseline_weight = kwargs["baseline_weights"]
        self.uses_3d = False
