"""Implementation of a Broken Powerlaw Flux"""
from .Flux import Flux
from theano import tensor as T

class BrokenPowerlaw_fixedBreak(Flux):
    """Make theano graph for broken powerlaw, fixed break point"""

    def __init__(self, parameters, input_vars, **kwargs):
        super(BrokenPowerlaw_fixedBreak, self).__init__(parameters, input_vars, **kwargs)
        self._baseline_weight = kwargs["baseline_weights"]
        self._add_req_input_vars = ['true_energy']
        self.uses_3d = False

