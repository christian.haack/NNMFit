"""Implementation of a Log-parabola Flux: norm*(E/E0)**-(a+b*log(E/E0)) """
from .Flux import Flux

class LogParabola(Flux):
    """Make theano graph for log-parabola"""

    def __init__(self, parameters, input_vars, **kwargs):
        super(LogParabola, self).__init__(parameters, input_vars, **kwargs)
        self._baseline_weight = kwargs["baseline_weights"]
        self.uses_3d = False
