"""Collection of utility functions"""
import numpy as n


def get_n_bins_from_edges(binning):
    """Returns numbers of bins for every binedges list in binning"""
    return [len(bin_es)-1 for bin_es in binning]


def calc_indices_2d(var_x, var_y, binning):
    """Return flattened indices for 2D binning (c-ordering)"""
    n_bins = get_n_bins_from_edges(binning)
    indices_x = n.digitize(var_x, binning[0])-1
    indices_y = n.digitize(var_y, binning[1])-1
    flattened = n.asarray(indices_x * n_bins[1] +
                          indices_y, dtype=int)

    #assert n.all(flattened < n_bins[0]*n_bins[1])
    return flattened


def calc_indices_3d(var_x, var_y, var_z, binning):
    """Return flattened indices for 3D binning (c-ordering)"""
    n_bins = get_n_bins_from_edges(binning)

    indices_x = n.digitize(var_x, binning[0])-1
    indices_y = n.digitize(var_y, binning[1])-1
    indices_z = n.digitize(var_z, binning[2])-1

    flattened = n.asarray(indices_x * n_bins[1] * n_bins[2] +
                          indices_y * n_bins[2] + indices_z,
                          dtype=int)
    assert n.all(flattened <
                 n_bins[0]*n_bins[1]*n_bins[2])
    return flattened


def calc_indices_3d_oversampling(var_x, var_y, overs_ra, binning):
    """Return flattened indices for 3D binning w. oversampling (c-ordering)"""
    n_bins = get_n_bins_from_edges(binning)
    indices_x = n.digitize(var_x, binning[0])-1
    indices_y = n.digitize(var_y, binning[1])-1
    indices_z = n.digitize(overs_ra.ravel(), binning[2])-1
    indices_z = indices_z.reshape(overs_ra.shape)
    flattened = n.asarray(indices_x * n_bins[1] * n_bins[2] +
                          indices_y * n_bins[2] + indices_z,
                          dtype=int)
    assert n.all(flattened <
                 n_bins[0] * n_bins[1] * n_bins[2])
    return flattened

def _weighted_quantile_arg(values, weights, q=0.5):
    """Helper function for weighted quantile
    Returns:
	index of the weighted quantile
    """
    indices = n.argsort(values)
    sorted_indices = n.arange(len(values))[indices]
    medianidx = (weights[indices].cumsum()/weights[indices].sum()).searchsorted(q)
    if (0 <= medianidx) and (medianidx < len(values)):
        return sorted_indices[medianidx]
    else:
        return n.nan

def weighted_quantile(values, weights, q=0.5):
    """Calculate weighted quantile
    Args:
	values , data array
	weights, weight array
	q, Quantile from (0,1)
    Returns:
	weighted quantile
    """
    if len(values) != len(weights):
        raise ValueError("shape of `values` and `weights` doesn't match!")
    index = _weighted_quantile_arg(values, weights, q=q)
    if index != n.nan:
        return values[index]
    else:
        return n.nan

def weighted_median(values, weights):
    """Return median of weighted data
    Args: 
	values, data array
	weights, weights array
    Returns:
	50% quantile of the weighted data
    """
    return weighted_quantile(values, weights, q=0.5)


