"""
Script for running a fit
"""
import argparse
import configparser
from time import time
from NNMFit.analysis.nnm_fitter import NNMFitter
from NNMFit import nnm_logger
import yaml

parser = argparse.ArgumentParser()
parser.add_argument("main_config")
parser.add_argument("--fix", help="Fix parameters", nargs=2,
                    action="append", dest="fix", required=False,
                    default=[])
parser.add_argument("--override-configs", help="Override configs",
                    nargs="+", dest="override_configs", required=False)
parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)
parser.add_argument("--seed", help="Random seed for pseudoexp generation",
                    dest="rnd_seed", required=False)
args = parser.parse_args()

if args.rnd_seed is not None:
    rnd_seed = int(args.rnd_seed)
else:
    rnd_seed = None
with open(args.analysis_config) as hdl:
    analysis_config = yaml.load(hdl)

fitter = NNMFitter(args.main_config,
                   args.override_configs,
                   analysis_config=analysis_config,
                   rnd_seed=rnd_seed)
fixed_pars = None
if args.fix is not None:
    fixed_pars = {par_name: float(par_value)
                  for par_name, par_value in args.fix}

mini = fitter.setup_minimizer(fixed_pars, profile=True)
mini_func, seeds, bounds = mini.make_mini_function()

start = time()
for i in xrange(10):
    res = mini_func(seeds)
duration = time()-start
mini._theano_func.profile.summary()
print "Time per call: ",duration/10.
#fitter.do_fit(fixed_pars, profile=False)
