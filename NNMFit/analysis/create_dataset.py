#! /bin/env python
"""
Script for creating an asimov or pseudoexp dataset
"""
import argparse
import cPickle as pickle
from NNMFit import nnm_logger
from NNMFit.analysis.nnm_fitter import NNMFitter
import yaml

parser = argparse.ArgumentParser()
parser.add_argument("main_config")
parser.add_argument("--override_configs", help="Override configs",
                    nargs="+", dest="override_configs", required=False)
parser.add_argument("--filename" , "-f" ,"-o", help="name of the outputfile", \
                    required=True, dest="filename")
parser.add_argument("--seed", help="Random seed for pseudoexp generation",
                    dest="rnd_seed", required=False)
parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)

args = parser.parse_args()

if args.rnd_seed is not None:
    rnd_seed = int(args.rnd_seed)
else:
    rnd_seed = None

with open(args.analysis_config) as hdl:
    analysis_config = yaml.load(hdl)

fitter = NNMFitter(args.main_config,
                   args.override_configs,
                   analysis_config=analysis_config,
                   rnd_seed=rnd_seed)

data_hists = fitter.data_hists

if args.filename is not None:
    with open(args.filename, "w") as outfile:
        pickle.dump(data_hists, outfile)
    print "Wrote the dataset to", outfile
