#! /bin/env python
"""
Script for sampling from the posterior distribution using emcee
"""
import cPickle as pickle
import argparse
import os
from NNMFit.analysis.nnm_fitter import NNMFitter
import numpy as n
from tqdm import tqdm
import emcee
import yaml
import time

class MCMCWrapper(object):
    """
    Wrapper class for handling calls to priors and likelihood
    """
    def __init__(self, negllh_func, input_var_ordering, seed=31337):
        self._negllh_func = negllh_func
        self._priors = {}
        self._input_var_ordering = input_var_ordering
        self.rstate = n.random.RandomState(seed)
        self.sampler = None # has to be initialized by user

    @staticmethod
    def _is_in_bounds(var, bound):
        """
        Check if var is in bounds
        """
        return ((bound[1] is None) or (var <= bound[1])) &\
                    ((bound[0] is None) or (var >= bound[0]))

    @staticmethod
    def make_flat_prior_from_bound(bound):
        """
        Return a flat prior function with hard cuts at the bounds

        Args:
            bound: tuple of min, max
        """
        def _func(var, bound=bound):
            if MCMCWrapper._is_in_bounds(var, bound):
                return 0
            else:
                return -n.inf
        return _func

    def set_priors(self, prior_settings):
        """
        Set priors for parameters

        Args:
            prior_settings: dict containing prior settings for every parameter.
                            The prior settings are tuples of prior-type and
                            settings.
        """
        for param, prior_setting in prior_settings.iteritems():
            if prior_setting[0] == "flat_bounds":
                prior = self.make_flat_prior_from_bound(prior_setting[1])
            else:
                raise NotImplementedError("Prior type not implemented")
            self._priors[param] = prior


    def log_prob(self, pars):
        """
        Return log posterior pdf

        Args:
            pars: ndarray of parameters to pass to llh func.
                  Has to be in ordering of input_var_ordering.
        """
        fres = -self._negllh_func(pars)[0]
        if n.isnan(fres):
            fres = -n.inf

        prior_sum = sum([self._priors[par_name](par) for par_name, par\
                         in zip(self._input_var_ordering, pars)])
        lprob = fres + prior_sum

        return lprob

    def initialize_walker_pos_in_bounds(self,
                                        nwalkers,
                                        seeds,
                                        bounds,
                                        jitter=5E-2):
        """
        Create initial walker positions by applying gaussian jitter
        to seed positions and ensuring positions to be in bounds.

        Args:
            nwalkers: Number of walkers
            seeds: List of seed positions for every dim
            bounds: List of bounds for every dim
            jitter: Scale of jitter (sigma) to apply

        Returns:
            Array of shape (nwalkers, ndim)
        """
        ndim = len(seeds)
        pos = n.empty((nwalkers, ndim))
        for i, seed in enumerate(seeds):
            for j in xrange(nwalkers):
                pos_candidate = None
                while pos_candidate is None or\
                      not self._is_in_bounds(pos_candidate, bounds[i]):
                    pos_candidate = jitter*self.rstate.randn() + seed
                pos[j, i] = pos_candidate
        return pos

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("main_config")
    parser.add_argument("--fix", help="Fix parameters", nargs=2,
                        action="append", dest="fix", required=False,
                        default=[])
    parser.add_argument("--override_configs", help="Override configs",
                        nargs="+", dest="override_configs", required=False)
    parser.add_argument("--seed", help="Random seed for pseudoexp generation",
                        dest="rnd_seed", type=int, required=False,
                        default=31337)
    parser.add_argument("-i", "--iterations", help="Number of MCMC steps",
                        type=int, dest="iterations")
    parser.add_argument("-n", "--nwalkers", help="Number of MCMC walkers",
                        type=int, dest="nwalkers", default=50)
    parser.add_argument("--analysis_config", help="Config containing the analysis"\
                        "config", dest="analysis_config", required=True)
    parser.add_argument("--bestfit", help="Pickle file containing ML results",
                        type=str, dest="bestfit", default=None)
    parser.add_argument("-o", "--outfile", help="Output file for fit result",
                        dest="outfile", required=False)
    parser.add_argument("-c", "--continue", help="File with previous chain to\
                        continue from", dest="cont", required=False,
                        default=None)
    parser.add_argument("--burn_in", help="Burn-in phase? Enter number of\
                        steps", type=int, required=False)
    args = parser.parse_args()

    start = time.time()

    fixed_pars = None
    bf_vals = None
    outfile = os.path.join(args.outfile)
    if args.fix is not None:
        fixed_pars = {par_name: float(par_value) for par_name, par_value in args.fix}
    if args.bestfit is not None:
        data = pickle.load(open(args.bestfit))
        fixed_pars.update(data["fixed-parameters"])
        bf_vals = data["fit-result"][1]
    with open(args.analysis_config) as hdl:
        analysis_config = yaml.load(hdl)
    fitter = NNMFitter(args.main_config,
                       args.override_configs,
                       analysis_config=analysis_config,
                       rnd_seed=args.rnd_seed)
    mini = fitter.setup_minimizer(fixed_pars, False)
    negllh_func = mini.make_mini_function()
    seeds, bounds = mini.seeds, mini.bounds
    print mini._input_var_ordering
    mcmc_bounds = [bounds[par] for par in mini._input_var_ordering]
    ndim = len(mini._input_var_ordering)

    mcmc_wrapper = MCMCWrapper(negllh_func, mini._input_var_ordering,
                               args.rnd_seed)
    prior_settings = {}
    for par_name, bound in zip(mini._input_var_ordering, mcmc_bounds):
        prior_settings[par_name] = ("flat_bounds", bound)
    mcmc_wrapper.set_priors(prior_settings)

    if args.cont is not None:
        pchain = n.genfromtxt(args.cont, names=True)
        nit, mod = divmod(pchain.shape[0], args.nwalkers)
        if mod != 0:
            raise RuntimeError("Previous chain length is not divisible by "\
                               "nwalkers * ndim. Make sure the chain is compa"\
                               "tible")
        pos0 = pchain.reshape(nit, args.nwalkers)[-1]
        pos0 = pos0.view((float, len(pos0.dtype.names)))[:,2:]
        ##additional jitter before continuing the chain:
        #pos0 = pos0 * n.random.uniform(low=0.95, high=1.05, size=pos0.shape)
        if args.outfile is None or args.outfile=="continue":
            outfile = args.cont
    else:
        if bf_vals is None:
            _, bf_vals = mini.minimize(tolerance=10)
        bf_seeds = [bf_vals[par] for par in mini._input_var_ordering]
        pos0 = mcmc_wrapper.initialize_walker_pos_in_bounds(
            args.nwalkers, bf_seeds, mcmc_bounds, jitter=6E-2)

    sampler = emcee.EnsembleSampler(args.nwalkers,
                              ndim,
                              mcmc_wrapper.log_prob)

    if args.burn_in is not None:
        print("Running burn-in: {} iterations".format(args.burn_in))
        pos1 = pos0
        for i in tqdm(range(args.burn_in), total=args.burn_in):
            pos1, prob, state = sampler.run_mcmc(pos1, 1)
        sampler.reset()
    else:
        pos1 = pos0

    if args.cont is None:
        hdl = open(outfile, "w+")
        hdl.write("i LLH "+ " ".join(mini._input_var_ordering)+"\n")
        hdl.close()
    elif args.outfile is not None:
        hdl = open(outfile, "w+")
        hdl.write("i LLH "+ " ".join(mini._input_var_ordering)+"\n")
        hdl.close()
    else:
        pass

    for result in tqdm(sampler.sample(pos1,
                                      iterations=args.iterations,
                                      storechain=False,
                                      rstate0=mcmc_wrapper.rstate.get_state()),
                       total=args.iterations):
        position = result[0]
        with open(outfile, "a+") as hdl:
            for k in xrange(position.shape[0]):
                hdl.write(
                    "{0:5d} {1:.2f} {2:s}\n".format(
                        k,
                        result[1][k],
                        " ".join(map(str, position[k]))))
            tqdm.write(
                "LLH: "+ " ".join(["{:.2f}".format(res) for res in
                                   result[1]])+"\n")
    end = time.time()
    print("Elapsed time: ", end - start)


if __name__ == "__main__":
    main()
