"""
Script for fitting the BG region for alignment
"""
import os
import argparse
import cPickle as pickle
import yaml
from NNMFit.analysis.nnm_fitter import NNMFitter
from NNMFit import nnm_logger

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("main_config")
    parser.add_argument("--fix", help="Configs to fit", nargs=2,
                        action="append", dest="fix", required=False,
                        default=[])
    parser.add_argument("-b", "--bg-region", help="Configs for bg-region",
                        nargs="+", dest="bg_configs", required=True)
    parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)
    args = parser.parse_args()

    with open(args.analysis_config) as hdl:
        analysis_config = yaml.load(hdl)

    configs = analysis_config["detector_configs"]
    for det_config in configs:
        analysis_config["detector_configs"] = [det_config]
        fitter = NNMFitter(
            args.main_config,
            args.bg_configs,
            analysis_config=analysis_config,
            override={"main": {"use_alignment": "False",
                               "analysis_variables": "reco_energy,reco_zenith",
                               "oversampling": "False"}})
        fixed_pars = None
        if args.fix is not None:
            fixed_pars = {par_name: float(par_value) for par_name, par_value in args.fix}
        res, res_dict = fitter.do_fit(fixed_pars)#, profile=True)

        res_dict.update(fixed_pars)
        outdir = analysis_config["bg_fits"]
        if not os.path.exists(outdir):
            os.makedirs(outdir)
        outname = os.path.join(outdir,"bg_fit_"+det_config+".pickle")

        print "Parameters are: ", res_dict
        pickle.dump(res_dict, open(outname, "w"))
