"""
Script for running a fit
"""
import os
import argparse
import numpy as n
import matplotlib
matplotlib.use("Agg")
import seaborn as sns
sns.set_style("whitegrid")
sns.set_context("paper")
import matplotlib.pyplot as plt
from NNMFit.analysis.nnm_fitter import NNMFitter
from NNMFit import nnm_logger, loaders, logging
from NNMFit.analysis_config import AnalysisConfig
from theano import function
import yaml
logger = logging.getLogger(__name__)


parser = argparse.ArgumentParser()
parser.add_argument("main_config")
parser.add_argument("--fix", help="Fix parameters", nargs=2,
                    action="append", dest="fix", required=False,
                    default=[])
parser.add_argument("--override_configs", help="Override configs",
                    nargs="+", dest="override_configs", required=False)
parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)
parser.add_argument("--seed", help="Random seed for pseudoexp generation",
                    dest="rnd_seed", required=False)
parser.add_argument("-o", "--outfile", help="Output file for fit result",
                    dest="outfile", required=False)
parser.add_argument("--profile", help="Profile the Theano operations",
                    action="store_true")

args = parser.parse_args()

if args.rnd_seed is not None:
    rnd_seed = int(args.rnd_seed)
else:
    rnd_seed = None

with open(args.analysis_config) as hdl:
    analysis_config = yaml.load(hdl)

fitter = NNMFitter(args.main_config,
                   args.override_configs,
                   analysis_config=analysis_config,
                   rnd_seed=rnd_seed)

for param in fitter.input_variables:
    param.set_value(fitter.param_seeds[param.name])


expec_funcs = {}
for det_conf, graph in fitter.expectation_graphs:
    fun = function([], [graph], allow_input_downcast=False)
    logger.info("%s: %f", det_conf, sum(fun()))
    expec_funcs[det_conf] = fun
