#! /bin/env python
"""
Script for dumping the data events with the best-fit weights
"""

import os
## hack to get the right condor_compiledir on the cluster
if '_CONDOR_SCRATCH_DIR' in os.environ.keys():
    if 'THEANO_FLAGS' in os.environ.keys():
        os.environ["THEANO_FLAGS"] = os.environ["THEANO_FLAGS"]+"base_compiledir="+\
            os.environ['_CONDOR_SCRATCH_DIR']
    else:
        os.environ["THEANO_FLAGS"] = "base_compiledir="+os.environ['_CONDOR_SCRATCH_DIR']

import argparse
import cPickle as pickle
from NNMFit import nnm_logger
from NNMFit.analysis.nnm_fitter import NNMFitter
import yaml
import time
from NNMFit.analysis_config import AnalysisConfig
from NNMFit import likelihoods
from NNMFit import minimizer
from NNMFit import graph_builder
from NNMFit import loaders
import numpy as np
import pandas as pd

start_time = time.time()
parser = argparse.ArgumentParser()
parser.add_argument("main_config")
parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)
parser.add_argument("-o", "--outpath", help="Output path",
                    dest="outpath", required=False)
parser.add_argument("-b", "--bestfit", help="Pickle file with best-fit result",
                    dest="bestfit_file", required=True)
parser.add_argument("--override_configs", help="Override configs",
                    nargs="+", dest="override_configs", required=False)


args = parser.parse_args()

with open(args.analysis_config) as hdl:
    analysis_config = yaml.load(hdl)

config_hdl = AnalysisConfig(args.main_config,
                            analysis_config["detector_configs"],
                            args.override_configs,
                            None)
detector_configs = analysis_config["detector_configs"]
if len(detector_configs)>1:
    print("Provide analysis settings with a single det-config")
    raise NotImplementedError
det_conf = detector_configs[0]
graph_hdl = graph_builder.GraphBuilder(config_hdl,
                                       detector_configs)

#all_graphs, all_input_vars = graph_hdl.make_expectation_graph_perflux(binned_expectation=False)
all_graphs, all_input_vars, all_graphs_unbinned, all_indices, graphs_per_flux = \
        graph_hdl.make_expectation_graph_weights(binned_expectation=True,
                                                 return_weights=True,
                                                 return_per_flux=True)

#load the bestfit file
with open(args.bestfit_file,"r") as bf_file:
    bestfit = pickle.load(bf_file)["fit-result"][1]

for var in all_input_vars:
    if var.name in bestfit.keys():
        val = bestfit[var.name]
        print("Setting ", var.name, val)
        var.set_value(val)

##load the variables we want to include
variables_to_dump = ["reco_energy", "reco_zenith", "true_energy",
                     "true_zenith", "event_id", "run_id", "mjd"]
shared_vars, bin_indices, _ = graph_hdl.make_shared_variables(variables_to_dump, det_conf)

##create final array to dump:
dict_to_dump = {}
for var_name, var in shared_vars.items():
    dict_to_dump[var_name] = var.get_value()

##load the flux_weights, including all re-weightings and hooks from the best-fit
for flux_name, expec_func in graphs_per_flux[det_conf].iteritems():
    flux_weights = expec_func.eval()
    dict_to_dump[flux_name] = flux_weights

for key, var in dict_to_dump.items():
    print key, var, var.shape
pandas_MC = pd.DataFrame.from_dict(dict_to_dump)

outfile_MC = args.outpath+"/MC_wBestfit_"+det_conf+".pickle"
with open(outfile_MC, "wc") as hdl:
    pickle.dump(pandas_MC, hdl)
    print("Wrote MC events with weights to", outfile_MC)

## get data (with more info per event):
key_mapping, _  = config_hdl.get_key_mapping(det_conf)
dataset_obj, bins = loaders.load_data(config_hdl, det_conf)
dataset = dataset_obj._data_dict

outfile_data = args.outpath+"/Data_"+det_conf+".pickle"
with open(outfile_data, "w") as hdl:
    pickle.dump(dataset, hdl)
    print("Wrote Data events with to", outfile_data)

print("Full execution took %s seconds" % (time.time() - start_time))
