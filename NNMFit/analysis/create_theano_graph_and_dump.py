#! /bin/env python
"""
Script for setting up the theano graph for a given configuration set.
The full function will then be saved (serialized via pickle). 
The actual fit-script is then later able to load it and avoid a re-compilation.
"""

import os
## hack to get the right condor_compiledir on the cluster
if '_CONDOR_SCRATCH_DIR' in os.environ.keys():
    if 'THEANO_FLAGS' in os.environ.keys():
        os.environ["THEANO_FLAGS"] = os.environ["THEANO_FLAGS"]+"base_compiledir="+\
            os.environ['_CONDOR_SCRATCH_DIR']
    else:
        os.environ["THEANO_FLAGS"] = "base_compiledir="+os.environ['_CONDOR_SCRATCH_DIR']

import argparse
import cPickle as pickle
from NNMFit import nnm_logger
from NNMFit.analysis.nnm_fitter import NNMFitter
import yaml
import time
start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument("main_config")
parser.add_argument("--fix", help="Fix parameters", nargs=2,
                    action="append", dest="fix", required=False,
                    default=[])
parser.add_argument("--override_configs", help="Override configs",
                    nargs="+", dest="override_configs", required=False)
parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)
parser.add_argument("-o", "--outfile", help="Output file for fit result",
                    dest="outfile", required=False)

args = parser.parse_args()

with open(args.analysis_config) as hdl:
    analysis_config = yaml.load(hdl)

fitter = NNMFitter(args.main_config,
                   args.override_configs,
                   analysis_config=analysis_config)
fixed_pars = None
if args.fix is not None:
    fixed_pars = {par_name: float(par_value)
                  for par_name, par_value in args.fix}

llh_function, input_vars = fitter.llh_hdl.make_llh(fitter._expectation_graphs,
                                       fitter._input_variables)
todump = {"theano_function" : llh_function,
          "input_vars": input_vars,
          "settings": {"main_config": args.main_config,
                       "analysis_config": args.analysis_config,
                       "override_configs": args.override_configs,
                       "fixed_pars": args.fix}}

if args.outfile is not None:
    with open(args.outfile, "w") as hdl:
        pickle.dump(todump, hdl)
    print("Wrote theano_function to ", args.outfile)
print("Full execution took %s seconds" % (time.time() - start_time))
