#! /bin/env python
"""
Script for running a fit
"""

import os
## hack to get the right condor_compiledir on the cluster
if '_CONDOR_SCRATCH_DIR' in os.environ.keys():
    if 'THEANO_FLAGS' in os.environ.keys():
        os.environ["THEANO_FLAGS"] = os.environ["THEANO_FLAGS"]+"base_compiledir="+\
            os.environ['_CONDOR_SCRATCH_DIR']
    else:
        os.environ["THEANO_FLAGS"] = "base_compiledir="+os.environ['_CONDOR_SCRATCH_DIR']

import argparse
import cPickle as pickle
from NNMFit import nnm_logger
from NNMFit.analysis.nnm_fitter import NNMFitter
from NNMFit.analysis.nnm_fitter import NNMFitterPreCompiled
import yaml
import time
start_time = time.time()

parser = argparse.ArgumentParser()
parser.add_argument("main_config")
parser.add_argument("--fix", help="Fix parameters", nargs=2,
                    action="append", dest="fix", required=False,
                    default=[])
parser.add_argument("--override_configs", help="Override configs",
                    nargs="+", dest="override_configs", required=False)
parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)
parser.add_argument("--seed", help="Random seed for pseudoexp generation",
                    dest="rnd_seed", required=False)
parser.add_argument("-o", "--outfile", help="Output file for fit result",
                    dest="outfile", required=False)
parser.add_argument("--precompiled_graph", default=None,
                    help=" Did you precompile and save the Theano graph?",
                    dest="precompiled_graph", required=False)

args = parser.parse_args()

if args.rnd_seed is not None:
    rnd_seed = int(args.rnd_seed)
    if args.precompiled_graph is not None:
        raise NotImplementedError("Precompiled Graph can not take a seed!")
else:
    rnd_seed = None

with open(args.analysis_config) as hdl:
    analysis_config = yaml.load(hdl)

if args.precompiled_graph is not None:
    fitter = NNMFitterPreCompiled(args.main_config,
                                  args.override_configs,
                                  analysis_config=analysis_config,
                                  precompiled_graph=args.precompiled_graph)
else:
    fitter = NNMFitter(args.main_config,
                       args.override_configs,
                       analysis_config=analysis_config,
                       rnd_seed=rnd_seed)
fixed_pars = None
if args.fix is not None:
    fixed_pars = {par_name: float(par_value)
                  for par_name, par_value in args.fix}
fitres = fitter.do_fit(fixed_pars)
todump = {"fit-result" : fitres,
          "fixed-parameters" : fixed_pars}
if args.outfile is not None:
    with open(args.outfile, "w") as hdl:
        pickle.dump(todump, hdl)
    print("Wrote fit-result to ", args.outfile)
print("Full execution took %s seconds" % (time.time() - start_time))
