"""
Script for writing the results of the bg region fits to
alignmen settings
"""
import os
import argparse
import cPickle as pickle
import configparser
from NNMFit import AnalysisConfig
import yaml

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)
    parser.add_argument("main_config")
    args = parser.parse_args()
    with open(args.analysis_config) as hdl:
        analysis_config = yaml.load(hdl)
    configs = analysis_config["detector_configs"]

    config = AnalysisConfig(args.main_config, configs)


    fluxes_list = config.get_components()
    _, _, alignment_settings = config.get_flux_params()#fluxes_list)
    bg_fits_dir = analysis_config["bg_fits"]

    configs = analysis_config["detector_configs"]
    for det_config in configs:
        in_file = os.path.join(bg_fits_dir, "bg_fit_"+det_config+".pickle")
        if os.path.isfile(in_file):
            fit_res = pickle.load(open(in_file))
            for par_name, align_type in alignment_settings.iteritems():
                if par_name in fit_res and align_type is None:
                    del fit_res[par_name]
            aligned_config = configparser.ConfigParser(
                interpolation=configparser.ExtendedInterpolation()
            )
            aligned_config.optionxform = lambda option: option

            config_file = os.path.join(AnalysisConfig._config_dir(),
                                       det_config+".cfg")
            aligned_config.read_file(open(config_file))
            aligned_config.read_dict({det_config+"_alignment": fit_res})
            aligned_config.write(open(config_file, "w"))
        else:
            print "Could not load bg_fit for {}".format(det_config)
