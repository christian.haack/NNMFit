#! /bin/env python
"""
This script is meant to scan the LLH function on a fixed grid of n 
fit-parameters. If all nuisance parameters are fixed, the LLH function 
is evaluated (no fit is performed).
If a fit is performed at each point (profiling), use the submit_scan in 
/resources/condor/.
"""
import argparse
import cPickle as pickle
from NNMFit import nnm_logger
from NNMFit.analysis.nnm_fitter import NNMFitter
import yaml
import numpy as np
from itertools import product
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument("main_config")
parser.add_argument("--fix", help="Fix parameters", nargs=2,
                    action="append", dest="fix", required=False,
                    default=[])
parser.add_argument("--override_configs", help="Override configs",
                    nargs="+", dest="override_configs", required=False)
parser.add_argument("--analysis_config", help="Config containing the analysis"\
                    "config", dest="analysis_config", required=True)
parser.add_argument("--seed", help="Random seed for pseudoexp generation",
                    dest="rnd_seed", required=False)
parser.add_argument("-o", "--outfile", help="Output file for fit result",
                    dest="outfile", required=False)
parser.add_argument("-s", "--scan", help="Scan parameter", nargs=4,
                    action="append", required=True)
args = parser.parse_args()

if args.rnd_seed is not None:
    rnd_seed = int(args.rnd_seed)
else:
    rnd_seed = None

with open(args.analysis_config) as hdl:
    analysis_config = yaml.load(hdl)

fitter = NNMFitter(args.main_config,
                   args.override_configs,
                   analysis_config=analysis_config,
                   rnd_seed=rnd_seed)
fixed_pars = None
if args.fix is not None:
    fixed_pars = {par_name: float(par_value)
                  for par_name, par_value in args.fix}

param_list = {}
for (par, plow, pup, pn) in args.scan:
    param_list[par] = np.linspace(float(plow), float(pup), int(pn))

def my_product(dicts):
        return (dict(izip(dicts, x)) for x in product(*dicts.itervalues()))

df_list = []

for ieval, pars in enumerate(dict(zip(param_list, x )) for x in
             product(*param_list.itervalues())):
    print ieval
    fixed_pars_current = fixed_pars
    for par in pars:
        fixed_pars_current[par] = pars[par]
    fitres = fitter.do_fit(fixed_pars_current, profile=False)
    LLH = { "LLH" : fitres[0][1]}
    fitres = fitres[1]
    fitres.update(LLH)
    fitres.update(fixed_pars_current)
    df_temp = pd.DataFrame.from_records([fitres])#, orient='columns')
    df_list.append(df_temp)

df_fitres = pd.concat(df_list, axis=0)

if args.outfile is not None:
    with open(args.outfile, "w") as outfile:
        print "Dumping dataframe to: ", args.outfile
        pickle.dump(df_fitres, outfile)
