"""Collection of data-set histogram handlers/makers etc"""
import logging
import numpy as n
from theano import shared, function
import scipy.stats
from .. import loaders
from ..caching import Cache
#from .data_handling import modification_hooks
#from .. import utilities
#from . import fluxes
#from . import systematics
#from .expectation_handler import ExpectationHandler
#from .binning import make_binned_data

logger = logging.getLogger(__name__)

class DataHist(object):
    """
    DataHist Handler class.

    Takes care of creating datasets, i.e. histograms for a given expectation.
    """

    def __init__(self, config):
        """
        Requires a loader to access the data/mc and parameters
        """
        self._config = config
        self.cache = Cache(self._config['main']['caching_dir'])

    def make_custom_asimov_dataset(self, det_conf,
                                   fname, flux_normalizations):
        dataset_mc, binning_mask_mc = loaders.load_dataset_mc(self._config,
                                                              det_conf,
                                                              self.cache)

        bin_centers = self._loader.settings[det_conf].analysis_binning[1]

        weights = 0
        for flux, norm in flux_normalizations.iteritems():
            weights += dset.weight(flux)*norm
        weights *= self._loader.settings[det_conf].livetime

        analysis_binning = self._config.make_binning(det_config)[0]
        h_asimov = scipy.stats.binned_statistic_2d(
            dset.reco_energy,
            dset.reco_zenith,
            weights,
            statistic="count",
            bins=analysis_binning)

        return {det_conf: h_asimov}

    def make_asimov_datasets(self, param_values, param_seeds,
                             expec_graphs, input_vars):
        """
        Returns an asimov dataset for given param_values
        Args:
            dict of param_values
        Returns:
            Expectation histograms
        """

        histograms = {}
        for param in input_vars:
            if param.name in param_values:
                value = param_values[param.name]
            else:
                value = param_seeds[param.name]
            logger.debug("Setting Asimov input %s to %f", param.name,
                         value)
            param.set_value(value)
        for det_config in self._config.detector_configs:
            expec_func = function([], expec_graphs[det_config])
            histograms[det_config] = shared(expec_func(), borrow=True)
        return histograms

    def make_pseudoexp(self, seed, param_values, param_seeds,
                             expec_graphs, input_vars):
        """
        Returns an pseudoexperiment for the given param_values
        Args:
            dict of param_values, default=seeds
        Returns:
            dict of conf -> Pseudoexperiment Poisson-sampled from expectation
        """
        if seed is None:
            logger.debug("No seed and no error handling yet on standard seeds")
            raise RuntimeError('No seed specified for PseudoExp generation.')
        rnd = n.random.RandomState(seed)
        logger.debug("Seed used for pseudoexp generation: %s", seed)
        histograms = {}
        histograms = self.make_asimov_datasets(
            param_values, param_seeds, expec_graphs, input_vars)
        for det_config in self._config.detector_configs:
            pseudoexp = rnd.poisson(histograms[det_config].get_value(),
                                    histograms[det_config].get_value().shape)
            histograms[det_config].set_value(pseudoexp)
        return histograms
