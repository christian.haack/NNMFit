"""Implementation of NNMFitter class"""
import logging
logger = logging.getLogger(__name__)
from os import environ
import theano
logger.info("Theano compiledir: %s", theano.config.compiledir)

import cPickle as pickle
import gc
import numpy as n
from ..analysis_config import AnalysisConfig
from .. import likelihoods
from .. import minimizer
from .. import graph_builder
from .. import loaders
from .datahists import DataHist

class NNMFitter(object):
    """
    NNMFitter class
    """
    def __init__(self,
                 base_config_file,
                 override_configs=None,
                 override=None,
                 analysis_config=None,
                 rnd_seed=None):
        self.config_hdl = AnalysisConfig(base_config_file,
                                         analysis_config["detector_configs"],
                                         override_configs,
                                         override)
        detector_configs = analysis_config["detector_configs"]
        self.graph_builder = graph_builder.GraphBuilder(
            self.config_hdl,
            detector_configs)
        self.param_seeds, self.param_bounds = self.graph_builder.parameter_settings[:2]
        self.analysis_config = analysis_config

        self._expectation_graphs = None
        self._mini = None
        self._input_variables = None
        analysis_type = self.analysis_config["analysis_type"]
        if analysis_type == "data":
            data_hists = loaders.make_data_histograms(
                self.config_hdl,
                detector_configs)
        elif analysis_type in ["asimov"]:
            input_params = self.analysis_config["input_params"]
            datahist_handler = DataHist(self.config_hdl)
            graphs, input_variables = self.graph_builder.make_expectation_graph(
                binned_expectation=True)
            data_hists = datahist_handler.make_asimov_datasets(input_params,
                                                               self.param_seeds,
                                                               graphs,
                                                               input_variables)
            self._expectation_graphs = graphs
            self._input_variables = input_variables
        elif analysis_type == "pseudoexp":
            input_params = self.analysis_config["input_params"]
            datahist_handler = DataHist(self.config_hdl)
            graphs, input_variables = self.graph_builder.make_expectation_graph(
                binned_expectation=True)
            data_hists = datahist_handler.make_pseudoexp(rnd_seed,
                                                         input_params,
                                                         self.param_seeds,
                                                         graphs,
                                                         input_variables)
            self._expectation_graphs = graphs
            self._input_variables = input_variables
        elif analysis_type == "custom_data":
            data_hists = pickle.load(open(analysis_config["custom_dataset"]))
        else:
            raise RuntimeError("Unknown analysis type: {}".format(
                analysis_type))
        llh_class = getattr(likelihoods, self.analysis_config["llh"])
        self.llh_hdl = llh_class(self.graph_builder, data_hists)
        self.data_hists = data_hists

    @property
    def input_variables(self):
        return self._input_variables

    @property
    def expectation_graphs(self):
        return self._expectation_graphs

    def get_llh_function(self, expectation_graph, input_vars):
        llh_function, input_variables = self.llh_hdl.make_llh(
            self._expectation_graphs, self._input_variables)
        return llh_function, input_variables

    def setup_minimizer(self, fixed_pars, profile):
        """
        Helper function for setting up the minimizer
        """
        minimizer_class = getattr(
            minimizer,
            self.config_hdl["minimizer_settings"]["class"])
        llh_function, input_variables = self.get_llh_function(
            self._expectation_graphs, self._input_variables)
        #add some noise to the seeds
        self.param_seeds = {key: n.random.normal(1,0.08)*val for key, val in\
                            self.param_seeds.items()}
        mini = minimizer_class(-llh_function,
                               input_variables,
                               self.param_seeds,
                               self.param_bounds,
                               fixed_pars,
                               profile=profile
                              )
        logger.info("Running garbage-collection: %d", gc.collect())
        return mini

    def update_fitseeds(self, new_seeds):
        """
        Update the seeds of parameters in the fit, e.g if asimov is created
        Args:
            dict new_seeds
        """
        for param in new_seeds.keys():
            if param in self.param_seeds:
                self.param_seeds[param] = new_seeds[param]
            else:
                pass
                #TODO: Throw exception? or include the logger even here

    def do_fit(self, fixed_pars=None, profile=False):
        """
        Performs a fit to the data

        Args:
            fixed_pars: dict of fixed parameters

        Returns:
            tuple of fit status and fit parameters
        """
        if self._mini is None:
            self._mini = self.setup_minimizer(fixed_pars, profile)

        if len(fixed_pars) >= len(self.param_seeds):
            print("All parameters fixed. Evaluating LLH.")
            res, res_dict = self._mini.evaluate()
        else:
            res, res_dict = self._mini.minimize(**self.config_hdl["minimizer_settings"])
        return res, res_dict


class NNMFitterPreCompiled(NNMFitter):
    '''
    Class similar to the NNMFitter, but the theano graph is not built and
    compiled. Instead, the init function expects the path to the precompiled
    graph. Care has to be taken that the exact same configuration-set is
    actually used!
    '''

    def __init__(self,
                 base_config_file,
                 override_configs=None,
                 override=None,
                 analysis_config=None,
                 precompiled_graph=None):
        '''
        Initialization similar to std NNMFitter, but no rnd_seed (thus no
        pseudoexp runs). It loads the precompiled theano graph.

        Args:
            base_config_file: a.k.a main_config
            override_configs: File containing overrides of single config
            aspects
            analysis_config: Analysis specific settings
        Returns:
            Instance of the NNMFitter (based on a precompiled theano graph)
        '''
        if precompiled_graph is None:
            raise FileNotFoundError("Provide a precompiled theano graph!")
        self.config_hdl = AnalysisConfig(base_config_file,
                                         analysis_config["detector_configs"],
                                         override_configs,
                                         override)
        detector_configs = analysis_config["detector_configs"]
        self.graph_builder = graph_builder.GraphBuilder(
            self.config_hdl,
            detector_configs)
        self.param_seeds, self.param_bounds = self.graph_builder.parameter_settings[:2]
        self.analysis_config = analysis_config

        self._mini = None
        self._input_variables = None
        analysis_type = self.analysis_config["analysis_type"]

        ##load the precompiled graph:
        logger.info("Loading the precompiled Theano graph..") 
        precompiled_hdl = pickle.load(open(precompiled_graph, "r"))
        self._llh_function = precompiled_hdl["theano_function"]
        self._input_variables = precompiled_hdl["input_vars"]
        self._precompile_settings = precompiled_hdl["settings"]

        ##check that the conf. settings are the same as during precompile:
        assert precompiled_hdl["settings"]["main_config"] == base_config_file
        #assert precompiled_hdl["settings"]["analysis_config"] == analysis_config
        assert precompiled_hdl["settings"]["override_configs"] == override_configs



    @property
    def input_variables(self):
        return self._input_variables

    @property
    def expectation_graphs(self):
        raise NotImplementedError("Expecation graphs not accesible")

    def get_llh_function(self):
        '''
        return the pre-loaded llh-function (theano graph)
        '''
        return self._llh_function, self._input_variables

    def setup_minimizer(self, fixed_pars, profile):
        """
        Helper function for setting up the minimizer
        """
        minimizer_class = getattr(
            minimizer,
            self.config_hdl["minimizer_settings"]["class"])
        llh_function, input_variables = self.get_llh_function()
        mini = minimizer_class(-llh_function,
                               input_variables,
                               self.param_seeds,
                               self.param_bounds,
                               fixed_pars,
                               profile=profile
                              )
        logger.info("Running garbage-collection: %d", gc.collect())
        return mini


