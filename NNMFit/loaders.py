"""Collection of loading functions"""
import logging
import os
import cPickle as pickle
from glob import glob
import numpy as n
from theano import shared, tensor
from .data_handling import Dataset
from .data_handling import modification_hooks
from .binning import make_binned_data
logger = logging.getLogger(__name__)

def make_binned_rates(binning, flattened_indices, rates, rate_errors):
    """Return binned rates"""
    binned_rates = []
    binned_errors = []
    '''
    minlength = (len(binning[0]) - 1) * (len(binning[1]) - 1)
    for i in xrange(rates.shape[1]):
        binned_rates.append(
            tensor.extra_ops.bincount(
                flattened_indices,
                rates[:, i],
                minlength=minlength))
        binned_errors.append(
            tensor.sqrt(
                tensor.extra_ops.bincount(
                    flattened_indices,
                    rates[:, i]**2,
                    minlength=minlength)))
    '''
    minlength = (len(binning[0]) - 1) * (len(binning[1]) - 1)
    for i in xrange(rates.shape[1]):
        binned_rates.append(
            n.bincount(
                flattened_indices.eval(),
                rates[:, i],
                minlength=minlength))
        binned_errors.append(
            n.sqrt(
                n.bincount(
                    flattened_indices.eval(),
                    rates[:, i]**2,
                    minlength=minlength)))
    binned_errors = n.asarray(binned_errors)
    binned_errors[binned_errors == 0] = 1
    return (n.asarray(binned_rates).T, binned_errors.T)


def load_dataset_mc(config, det_config, cache):
    key_mapping, key_mapping_mc = config.get_key_mapping(det_config)
    baseline_ds_path = glob(os.path.join(
        config[det_config]['baseline_dataset'],
        "*.hdf"))
    logger.info("Baseline path: %s", config[det_config]['baseline_dataset'])
    logger.info("Baseline_dataset path: %s", baseline_ds_path)
    if len(baseline_ds_path) != 1:
        raise RuntimeError("Could not find baseline dataset")
    dataset_path = baseline_ds_path[0]
    oversampling = None
    if config._config.getboolean('main', 'oversampling'):
        if 'oversampling' in config[det_config]:
            logger.info('Oversampling is activated for det-config'\
                        '%s with factor %s',
                        det_config,
                        config[det_config]['oversampling'])
            oversampling = int(config[det_config]['oversampling'])
    dataset_mc = Dataset(dataset_path, key_mapping_mc,
                         oversampling, cache=cache)
    if "modification_hooks" in config[det_config]:
        hooks = config.parse_list(
            config[det_config]["modification_hooks"])
        hooks_obj = []
        for hook in hooks:
            hook_cls = getattr(modification_hooks,
                               config[hook]["class"])
            kwargs = dict(config[hook])
            del kwargs["class"]
            hooks_obj.append(hook_cls(**kwargs))
        dataset_mc.apply_hooks(hooks_obj)
    reco_vars = config.parse_list(config['main']['analysis_variables'])
    dataset_mc.set_standard_mask()
    dataset_mc.set_analysis_vars(reco_vars)
    binning = config.make_binning(det_config)[0]
    dataset_mc.set_analysis_binning(binning)
    binning_mask_mc = dataset_mc.make_binning_mask(binning)
    if "additional_mask" in config[det_config].keys():
        additional_conditions = config[det_config]["additional_mask"]
        additional_mask = dataset_mc.make_additional_mask(additional_conditions)
        total_mask_mc = (additional_mask & binning_mask_mc)
    else:
        total_mask_mc = binning_mask_mc
    dataset_mc.set_mask(total_mask_mc)
    return dataset_mc, total_mask_mc
    #return dataset_mc, binning_mask_mc

def load_kde_sys_datasets(config, det_config, component, sys_type):
    """Load evaluated kde for det_config, flux and sys_type"""
    flux = config.components[component]['baseline_weights']
    sys_datasets, sys_values = config.get_datasets_for_sys(det_config, sys_type)
    logger.debug('Loading systematics datasets %s for config %s', sys_datasets, det_config)
    syskey = config[det_config]['systematics']
    sys_type_key = config[det_config][sys_type]
    sys_folder = os.path.join(config[syskey]['systematics_path'], 'kde')
    rates = None
    rate_errors = None
    baseline_dataset = config[sys_type_key]['baseline']
    baseline_index = None
    for i, data_set in enumerate(sys_datasets):
        bootstrap_file = 'reco_kde_bootstrap_eval_{}_{}.pickle'.format(data_set, flux)
        rates_file = 'reco_kde_eval_{}_{}.npz'.format(data_set, flux)
        logger.debug('loading %s and %s', bootstrap_file, rates_file)
        _, yerr = pickle.load(open(os.path.join(sys_folder, bootstrap_file)))
        y = n.load(os.path.join(sys_folder, rates_file))['rates']
        if rates is None:
            rates = n.empty((len(y), len(sys_datasets)))
            rate_errors = n.empty(rates.shape)

        rates[:, i] = y
        rate_errors[:, i] = yerr
        if data_set == baseline_dataset:
            baseline_index = i
    if baseline_index is None:
        raise RuntimeError('Invalid baseline dataset specified')
    logger.debug("Length of sys datasets: %d", rates.shape[0])
    return (rates, rate_errors, sys_values, baseline_index)

def load_data(config, det_config):
    """
    Setup routine for loading the datasets.
    Should only be called by constructor
    """
    key_mapping, key_mapping_mc = config.get_key_mapping(det_config)
    reco_vars = config.parse_list(config['main']['analysis_variables'])
    binning = config.make_binning(det_config)[0]

    dataset_data = Dataset(config[det_config]['data'], key_mapping)
    dataset_data.set_analysis_vars(reco_vars)
    dataset_data.set_standard_mask()
    ## this was not applied before to the data:
    dataset_data.set_analysis_binning(binning)
    binning_mask = dataset_data.make_binning_mask(binning)
    if "additional_mask" in config[det_config].keys():
        additional_condition = config[det_config]["additional_mask"]
        additional_mask_data = dataset_data.make_additional_mask(additional_condition)
        total_mask_data = (additional_mask_data & binning_mask)
    else:
        total_mask_data = binning_mask
    dataset_data.set_mask(total_mask_data)
    return dataset_data, binning

def make_data_histograms(config, det_configs):
    """
    Returns a dict of binned data histograms
    """
    histograms = {}
    for det_config in sorted(det_configs):
        dataset_data, binning = load_data(config, det_config)
        is_3d = len(binning) == 3 or "oversampling" in config[det_config]
        histograms[det_config] = shared(
            make_binned_data(
                dataset_data.analysis_vars,
                binning,
                is_3d),
            borrow=True)
    return histograms
