"""
Implements GraphBuilder Class
"""
from __future__ import division
import logging
from collections import namedtuple, defaultdict
from theano import shared, tensor
from .binning import make_binned_flux
from .caching import Cache
from . import fluxes
from . import systematics
from . import loaders
from .binning import make_binned_data
import numpy as n

logger = logging.getLogger(__name__)
DetConfSettings = namedtuple("DetConfSettings", [
    "alignment",
    "binning",
    "oversampling"])

class GraphBuilder(object):
    """
    Construct parameters, fluxes, book data and construct shared variables
    """

    def __init__(self, config, detector_configs):
        self._config = config
        self.detector_configs = detector_configs
        self.cache = Cache(self._config['main']['caching_dir'])

        self._input_variables = {}

        self.fluxes_dict = self.make_parameters_and_fluxes()

        param_tuple = self._config.get_params_and_bounds( detector_configs)
        self.det_conf_settings = {}
        for det_conf in sorted(detector_configs):
            self.det_conf_settings[det_conf] = DetConfSettings(**{
                "alignment": param_tuple[2][det_conf],
                "binning": self._config.make_binning(det_conf)[0],
                "oversampling": "oversampling" in self._config[det_conf]})
        self.parameter_settings = param_tuple[:2]
        self.parameter_priors = self._config.get_param_priors()
        self.use_binned_sys = self._config['main'].getboolean('binned_systematics')

    def make_sys_par_object(self,
                            flux_name,
                            det_conf,
                            sys_t,
                            binned,
                            binning_mask_mc,
                            flattened_indices):
        """
        Factory function for creating systematics parameter objects

        Args:
            flux_name: Name of the flux component
            det_conf: Name of the detector config
            sys_t: Name of the systematics type
            binned: binned systematic
            binning_mask_mc : mask on MC events corresponding to the binning
            flattened_indices: bin-index for each event

        Returns:
            systematics object
        """
        sys_cls = getattr(systematics, self._config[sys_t]['class'])

        if hasattr(sys_cls, "IS_SYS_PARAM") and sys_cls.IS_SYS_PARAM:
            # This is a systematics nuisance parameter
            rates, rate_errors, values, baseline_index =\
                    loaders.load_kde_sys_datasets(self._config,
                                                  det_conf,
                                                  flux_name,
                                                  sys_t)
            mask = binning_mask_mc
            if mask is not None:
                rates = rates[mask, :]
                rate_errors = rate_errors[mask, :]
            if binned:
                binned_rates, binned_errors = loaders.make_binned_rates(
                    self.det_conf_settings[det_conf].binning,
                    flattened_indices,
                    rates,
                    rate_errors)
                sys_rates_t = shared(binned_rates)
                sys_errors_t = shared(binned_errors)
            else:
                sys_rates_t = shared(rates)
                sys_errors_t = shared(rate_errors)
            sys_obj = sys_cls(values,
                              sys_rates_t,
                              sys_errors_t,
                              values[baseline_index],
                              sys_rates_t[:,baseline_index])
        else:
            # This is not a sys. nuisance parameter.
            # eg. baseline correction

            add_set_sys = {'baseline_weights':
                           self._config[flux_name]['baseline_weights']}
            if 'additional_settings' in self._config[sys_t]:
                add_set_sec = self._config[sys_t]['additional_settings']
                add_set_sys.update(self._config[add_set_sec])
            sys_obj = sys_cls(**add_set_sys)
        return sys_obj

    def make_parameters_for_flux(self, flux_name):
        """
        Create parameters objects and theano scalars
        """
        param_obj_dict = self._config.parse_params_for_flux(flux_name)

        input_vars = {}
        for param_name, (param_class, add_set) in param_obj_dict.iteritems():
            if param_name not in self._input_variables:
                 # Create a theano scalar if needed.                 # This
                 # will effectively link parameters with identical names
                 # # configured for different fluxes
                self._input_variables[param_name] = shared(0.0, param_name)

            input_vars[param_name] = self._input_variables[param_name]
            param_obj_dict[param_name] = param_class(**add_set)

        return (param_obj_dict, input_vars)

    def make_parameters_and_fluxes(self):
        """
        Read fluxes and configured parameters from config and return
        dictionary of flux objects
        """
        fluxes_dict = {}
        comps = self._config.components
        logger.debug('Found %d fluxes in config: %s', len(comps), repr(comps.keys()))
        for flux_name, flux_set in comps.iteritems():
            logger.debug('Flux, flux_settings : %s , %s ' , flux_name, flux_set)
            param_obj_dict, input_vars = self.make_parameters_for_flux(flux_name)
            add_set_flux = {'baseline_weights': flux_set['baseline_weights']}
            add_set_flux.update(flux_set.get("additional", {}))
            if "hooks" in add_set_flux.keys():
                add_set_flux["hooks"] = self._config.parse_list(add_set_flux["hooks"])
            flux_cls = getattr(fluxes, flux_set['class'])
            fluxes_dict[flux_name] = flux_cls(param_obj_dict, input_vars, **add_set_flux)
        return fluxes_dict

    def collect_required_variables(self, det_conf, det_sys_handler):
        """Return set of required variables for given det_conf"""
        req_pars = set()
        for _, flux in self.fluxes_dict.iteritems():
            req_pars.update(flux.get_list_of_req_vars())
        #req_pars.add(det_sys_handler.get_list_of_req_vars(det_conf, flux_name))
        return req_pars

    def make_shared_variables(self, variables, det_config):
        """Return list of shared variables"""
        dataset_mc, binning_mask_mc = loaders.load_dataset_mc(self._config,
                                                              det_config,
                                                              self.cache)
        shared_vars = {}
        mem_footprint = 0
        for var in variables:
            dtemp = dataset_mc.read_key_as_numpy(var)
            mem_footprint +=dtemp.nbytes
            var_t = shared(dtemp)
            shared_vars[var] = var_t
        dtemp = dataset_mc.flattened_indices
        mem_footprint += dtemp.nbytes
        indices_flattened = shared(dtemp)
        logger.debug("Memory footprint of shared data variables: %f",
                    mem_footprint / (1024)**3)
        return shared_vars, indices_flattened, binning_mask_mc

    def make_graph_for_flux(self, flux_name, shared_vars, det_conf):
        """Return theano graph for flux"""
        flux = self.fluxes_dict[flux_name]
        return flux.make_graph(shared_vars,
                               self.det_conf_settings[det_conf].alignment)

    def make_graph_and_apply_sys(self,
                                 flux,
                                 flux_uses_3d,
                                 shared_vars,
                                 det_conf,
                                 det_sys_handler,
                                 indices_flattened,
                                 return_sys_graph=False):
        """
        Return theano graph for flux with applied detector
        systematics

        Args:
            flux: Name of the flux
            flux_uses_3d: (bool) if this flux uses 3D
            shared_vars: the analysis variables
            det_conf: Detector config
            det_sys_handler: systematics handler
            flattened_indices: bin-index for each event
            return_sys_graph: Return the sys_graph as well
        """
        logger.debug("Creating graph for flux %s", flux)
        graph_f = self.make_graph_for_flux(flux, shared_vars, det_conf)
        graph_sys = det_sys_handler.make_graph(flux,
                                               det_conf,
                                               shared_vars)
        _n_bins_x = len(self.det_conf_settings[det_conf].binning[0])-1
        _n_bins_y = len(self.det_conf_settings[det_conf].binning[1])-1


        if self.use_binned_sys:
            try:
                if flux_uses_3d:
                    graph_sys = graph_sys.reshape((_n_bins_x,
                                                   _n_bins_y,
                                                   1))
                else:
                    graph_sys = graph_sys.reshape((_n_bins_x,
                                                   _n_bins_y))
            except AttributeError:
                logger.warning("Could not reshape sys. graph. No "
                               "systematics configured?")
                graph_sys = 1
            oversampling_flag = None
            dconf = self.det_conf_settings[det_conf]
            if dconf.oversampling:
                if flux_uses_3d:
                    oversampling_flag = "over"
                else:
                    oversampling_flag = "repeat"
            graph_f = make_binned_flux(graph_f,
                                       dconf.binning,
                                       indices_flattened,
                                       oversampling_flag)
        if return_sys_graph:
            return graph_f*graph_sys, graph_sys

        return graph_f*graph_sys

    def make_systematics_handler(self):
        sys_handler = systematics.SystematicsHandler()
        for det_conf in sorted(self.detector_configs):
            sys_key = self._config[det_conf]['systematics']
            sys_types = self._config.parse_list(
                self._config[sys_key]['systematics'])
            alignment_values = self.det_conf_settings[det_conf].alignment
            for flux in self.fluxes_dict:
                for sys_t in sys_types:
                    sys_cls = getattr(systematics, self._config[sys_t]['class'])
                    sys_handler.add_systematic(
                        det_conf, flux, sys_t, sys_cls, alignment_values)
        return sys_handler

    def make_expectation_graph(self, binned_expectation=True):
        """
        Return the full theano graph for this expectation
        Detector systematics and livetime are applied
        Args:
            binned_expectation: (bool) Return flux binned according to analysis
                                 bins
        """
        # We need to load the variables at this point

        all_graphs = {}
        sys_handler = self.make_systematics_handler()

        for det_conf in sorted(self.detector_configs):
            logger.debug("Building graph for det-conf: %s", det_conf)
            req_pars = self.collect_required_variables(det_conf, sys_handler)
            shared_vars, indices_flattened,\
                    binning_mask_mc = self.make_shared_variables(req_pars,
                                                                 det_conf)

            # Have to construct sys handler differently. Must be able to see
            # required vars before sys_t are constructed

            sys_key = self._config[det_conf]['systematics']
            sys_types = self._config.parse_list(self._config[sys_key]['systematics'])
            alignment_values = self.det_conf_settings[det_conf].alignment
            for flux_name in self.fluxes_dict:

                for sys_t in sys_types:
                    sys_obj = self.make_sys_par_object(
                        flux_name, det_conf, sys_t,
                        self.use_binned_sys,
                        binning_mask_mc,
                        indices_flattened)
                    sys_handler.add_systematic_object(
                        det_conf, flux_name, sys_t, sys_obj)

            total_graph = 0
            
            if binned_expectation and not self.use_binned_sys:
                for flux_name, flux in self.fluxes_dict.iteritems():
                    graph = self.make_graph_and_apply_sys(flux_name,
                                                          flux.uses_3d,
                                                          shared_vars,
                                                          det_conf,
                                                          sys_handler,
                                                          indices_flattened)
                    if self.det_conf_settings[det_conf].oversampling:
                        if flux.uses_3d:
                            oversampling_flag = "over"
                        else:
                            oversampling_flag = "repeat"
                    else:
                        oversampling_flag = None
                    total_graph += make_binned_flux(
                        graph,
                        self.det_conf_settings[det_conf].binning,
                        indices_flattened,
                        oversampling_flag)
            else:
                # If we're using binned systematics, flux is already binned
                # otherwise returned unbinned graph
                for flux_name, flux in self.fluxes_dict.iteritems():
                    total_graph += self.make_graph_and_apply_sys(
                        flux_name,
                        flux.uses_3d,
                        shared_vars,
                        det_conf,
                        sys_handler,
                        indices_flattened)
            all_graphs[det_conf] = total_graph*float(self._config[det_conf]['livetime'])

        all_input_vars = self._input_variables.values() +\
                sys_handler.get_all_input_vars().values()
        return all_graphs, all_input_vars

    def make_expectation_graph_perflux(self,
                                       binned_expectation=True,
                                       return_sys_graph=False):
        """
        Return the theano graphs for every flux without summing
        Detector systematics and livetime are applied
        Args:
            binned_expectation: (bool) Return flux binned according to analysis
                                 bins
        """

        all_graphs = defaultdict(dict)
        sys_handler = self.make_systematics_handler()

        for det_conf in self.detector_configs:
            logger.debug("Building graph for det-conf: %s", det_conf)

            livetime = float(self._config[det_conf]['livetime'])
            req_pars = self.collect_required_variables(det_conf, sys_handler)
            shared_vars, indices_flattened, binning_mask_mc = self.\
                    make_shared_variables(req_pars,
                                          det_conf)

            # Have to construct sys handler differently. Must be able to see
            # required vars before sys_t are constructed

            sys_key = self._config[det_conf]['systematics']
            sys_types = self._config.parse_list(self._config[sys_key]['systematics'])
            alignment_values = self.det_conf_settings[det_conf].alignment
            for flux_name in self.fluxes_dict:
                for sys_t in sys_types:
                    sys_obj = self.make_sys_par_object(
                        flux_name, det_conf, sys_t,
                        self.use_binned_sys,
                        binning_mask_mc,
                        indices_flattened)
                    sys_handler.add_systematic_object(
                        det_conf, flux_name, sys_t, sys_obj)

            if binned_expectation and not self.use_binned_sys:
                for flux_name, flux in self.fluxes_dict.iteritems():
                    graph, sys_graph = self.make_graph_and_apply_sys(flux_name,
                                                                     flux.uses_3d,
                                                                     shared_vars,
                                                                     det_conf,
                                                                     sys_handler,
                                                                     indices_flattened,
                                                                     True)
                    if self.det_conf_settings[det_conf].oversampling:
                        if flux.uses_3d:
                            oversampling_flag = "over"
                        else:
                            oversampling_flag = "repeat"
                    else:
                        oversampling_flag = None

                    binned_graph = make_binned_flux(
                        graph,
                        self.det_conf_settings[det_conf].binning,
                        indices_flattened,
                        oversampling_flag)*livetime
 
                    if return_sys_graph:
                        binned_sys_graph = make_binned_flux(
                        sys_graph,
                        self.det_conf_settings[det_conf].binning,
                        indices_flattened,
                        oversampling_flag)*livetime
 
                        all_graphs[det_conf][flux_name] = binned_graph, binned_sys_graph
                    all_graphs[det_conf][flux_name] = binned_graph
            else:
                # If we're using binned systematics, flux is already binned
                # otherwise returne unbinned graph
                for flux_name, flux in self.fluxes_dict.iteritems():
                    graph, sys_graph = self.make_graph_and_apply_sys(
                        flux_name,
                        flux.uses_3d,
                        shared_vars,
                        det_conf,
                        sys_handler,
                        indices_flattened,
                        True)
                    if return_sys_graph:
                        all_graphs[det_conf][flux_name] = graph*livetime, sys_graph
                    else:
                        all_graphs[det_conf][flux_name] = graph*livetime

        all_input_vars = self._input_variables.values() +\
                sys_handler.get_all_input_vars().values()
        return all_graphs, all_input_vars

    def count_simulation(self):
        """
        Return the number of simulation statistics per bin
        Args:
        """

        sim_counts = {}
        sys_handler = self.make_systematics_handler()

        for det_conf in self.detector_configs:
            logger.debug("Building graph for det-conf: %s", det_conf)
            req_pars = self.collect_required_variables(det_conf, sys_handler)
            shared_vars, indices_flattened,\
                    binning_mask_mc = self.make_shared_variables(req_pars,
                                                                 det_conf)
            oversampling_flag = None
            ##oversampling does not change the simulation statistic

            sim_counts[det_conf] = make_binned_flux(
                tensor.ones_like(indices_flattened),
                self.det_conf_settings[det_conf].binning,
                indices_flattened,
                oversampling_flag)

        return sim_counts

    def make_expectation_graph_weights(self, binned_expectation=True,
                                       return_weights=False,
                                       return_per_flux=False):
        """
        Return the full theano graph for this expectation
        Detector systematics and livetime are applied
        Args:
            binned_expectation: (bool) Return flux binned according to analysis
                                 bins
        """
        # We need to load the variables at this point

        all_graphs = {}
        all_graphs_unbinned = {}
        all_indices = {}
        graphs_per_flux = {}
        sys_handler = self.make_systematics_handler()

        for det_conf in sorted(self.detector_configs):
            logger.debug("Building graph for det-conf: %s", det_conf)
            req_pars = self.collect_required_variables(det_conf, sys_handler)
            shared_vars, indices_flattened,\
                    binning_mask_mc = self.make_shared_variables(req_pars,
                                                                 det_conf)

            # Have to construct sys handler differently. Must be able to see
            # required vars before sys_t are constructed

            sys_key = self._config[det_conf]['systematics']
            sys_types = self._config.parse_list(self._config[sys_key]['systematics'])
            alignment_values = self.det_conf_settings[det_conf].alignment
            for flux_name in self.fluxes_dict:
                for sys_t in sys_types:
                    sys_obj = self.make_sys_par_object(
                        flux_name, det_conf, sys_t,
                        self.use_binned_sys,
                        binning_mask_mc,
                        indices_flattened)
                    sys_handler.add_systematic_object(
                        det_conf, flux_name, sys_t, sys_obj)

            total_graph = 0
            total_graph_unbinned = 0
            graphs_per_flux[det_conf] = {}

            if binned_expectation and not self.use_binned_sys:
                for flux_name, flux in self.fluxes_dict.iteritems():
                    graph = self.make_graph_and_apply_sys(flux_name,
                                                          flux.uses_3d,
                                                          shared_vars,
                                                          det_conf,
                                                          sys_handler,
                                                          indices_flattened)
                    if self.det_conf_settings[det_conf].oversampling:
                        if flux.uses_3d:
                            oversampling_flag = "over"
                        else:
                            oversampling_flag = "repeat"
                    else:
                        oversampling_flag = None
                    total_graph_unbinned += graph
                    if return_per_flux:
                        graphs_per_flux[det_conf][flux_name] = graph
                    total_graph += make_binned_flux(
                        graph,
                        self.det_conf_settings[det_conf].binning,
                        indices_flattened,
                        oversampling_flag)
            else:
                # If we're using binned systematics, flux is already binned
                # otherwise returned unbinned graph
                for flux_name, flux in self.fluxes_dict.iteritems():
                    graph = self.make_graph_and_apply_sys(
                        flux_name,
                        flux.uses_3d,
                        shared_vars,
                        det_conf,
                        sys_handler,
                        indices_flattened)
                    if return_per_flux:
                        graphs_per_flux[det_conf][flux_name] = graph
                    total_graph += graph
            all_indices[det_conf] = indices_flattened
            all_graphs[det_conf] = total_graph*\
                    float(self._config[det_conf]['livetime'])
            all_graphs_unbinned[det_conf] = total_graph_unbinned*\
                    float(self._config[det_conf]['livetime'])
            if return_per_flux:
                for flux_name in self.fluxes_dict.keys():
                    graphs_per_flux[det_conf][flux_name] *= float(self._config[det_conf]['livetime'])

        all_input_vars = self._input_variables.values() +\
                sys_handler.get_all_input_vars().values()

        if return_per_flux:
            return all_graphs, all_input_vars, all_graphs_unbinned, all_indices, graphs_per_flux
        else:
            return all_graphs, all_input_vars, all_graphs_unbinned, all_indices


