import sys,os
from kde.cudakde import gaussian_kde, bootstrap_kde
#from kde.pykde import gaussian_kde, bootstrap_kde
from scipy import interpolate
from numpy import *
import cPickle as pickle
import warnings
import numpy as n

def create_path(outpath):
    try:
        dir = os.path.dirname(outpath)
        os.makedirs(dir)
    except OSError as oserr:
        print "Directories already exists: %s" %(oserr.filename,)

class make_kde:
    def __init__(self, energy, zenith, weights,
                 bounds=(None, None),
                 thresholds=(None, None),
                 ):

        self.recoLogE = n.log10(energy)
        self.recoCosZ = n.cos(zenith)

        saneMask = (~n.isinf(self.recoLogE) &
            (~n.isinf(self.recoCosZ)))
        self.recoLogE = self.recoLogE[saneMask]
        self.recoCosZ = self.recoCosZ[saneMask]
        self.weights = weights[saneMask]


        self._bounds = bounds
        self._thresholds = thresholds
        self.flux_total = sum(self.weights)

    def eval_mirrored(self, xs, ys):
        zs = self._kernel([xs,ys])
        bX, bY = self._bounds
        tX, tY = self._thresholds

        def make_mask(bound, vals, thresholds):
            return_vals = []

            if bound:
                bLow,bUp = bound
                if bLow:
                    mirror_mask = n.zeros(len(vals), dtype=n.bool)
                    if thresholds and thresholds[0]:
                        mirrored_bound = 2*bLow-thresholds[0]
                        mirror_mask |= ((vals<mirrored_bound) & (vals > bLow))
                    else:
                        mirror_mask = n.ones(len(vals), dtype=n.bool)

                    mirrored_vals = 2*bLow-vals[mirror_mask]
                    return_vals.append((mirror_mask, mirrored_vals))
                if bUp:
                    mirror_mask = n.zeros(len(vals), dtype=n.bool)
                    if thresholds and thresholds[1]:
                        mirrored_bound = 2*bUp-thresholds[1]
                        mirror_mask |= ((vals>mirrored_bound) & (vals < bUp))
                    else:
                        mirror_mask = n.ones(len(vals), dtype=n.bool)
                    mirrored_vals = 2*bUp-vals[mirror_mask]
                    return_vals.append((mirror_mask, mirrored_vals))
                return return_vals
            else:
                return None

        ret = make_mask(bX, xs, tX)
        if ret:
            for (mask, vals) in ret:
                mirror_eval = self._kernel([vals, ys[mask]])
                if isinstance(zs, tuple):
                    zs[0][mask] += mirror_eval[0]
                    zs[1][mask] = n.sqrt(zs[1][mask]**2 + mirror_eval[1]**2)
                else:
                    zs[mask] += mirror_eval

        ret = make_mask(bY, ys, tY)
        if ret:
            for (mask, vals) in ret:
                mirror_eval = self._kernel([xs[mask], vals])
                if isinstance(zs, tuple):
                    zs[0][mask] += mirror_eval[0]
                    zs[1][mask] = n.sqrt(zs[1][mask]**2 + mirror_eval[1]**2)
                else:
                    zs[mask] += mirror_eval
        return zs

    def make_kernel(self, kde_values=None, useNevents=None, use_cuda=False,
                niter=10, bootstrap=False, adaptive=True,
                weight_adaptive_bw=False, alpha=0.3, bw_method="silverman"):

        if useNevents != None:
            data = n.array(zip(self.recoLogE,self.recoCosZ,self.weights))
            n.random.shuffle(data)
            recoLogE = data[:, 0][:useNevents]
            recoCosZ = data[:, 1][:useNevents]
            weights  = data[:, 2][:useNevents]
        else:
            recoLogE = self.recoLogE
            recoCosZ = self.recoCosZ
            weights  = self.weights
        self.bootstrap = bootstrap
        self.rescale_factor = n.sum(weights)/self.flux_total

        if self.bootstrap:
            if kde_values != None:
                raise NotImplementedError("`kde_values` is not supported for using with the option bootstrap.")
            self._kernel = bootstrap_kde(n.vstack([recoLogE,recoCosZ]),
                                            weights=weights, niter=niter,
                                            use_cuda=use_cuda, adaptive=adaptive,
                                            weight_adaptive_bw=weight_adaptive_bw,
                                            alpha=alpha, bw_method=bw_method)
            self.kernel = lambda x: tuple(values * self.rescale_factor for values in self._evaluate_bootstrap(x))
            self.func_rate_density = lambda x: tuple(values * self.flux_total * self.rescale_factor for values in self._evaluate_bootstrap(x))
        else:
            self._kernel = gaussian_kde(n.vstack([recoLogE,recoCosZ]), weights=weights,
                                        kde_values=kde_values, use_cuda=use_cuda,
                                        adaptive=adaptive,
                                        weight_adaptive_bw=weight_adaptive_bw,
                                        alpha=alpha, bw_method=bw_method)
            self.kernel = lambda x: self._evaluate(x) * self.rescale_factor
            self.func_rate_density = lambda x: self._evaluate(x) * self.flux_total * self.rescale_factor

    def _evaluate(self,x):
        return self.eval_mirrored(x[0], x[1])
    def _evaluate_bootstrap(self,x):
        return self.eval_mirrored(x[0], x[1])

    def make_spline(self,x=None,y=None,
                    nsteps=10,method="interp2d",
                    **kwargs):
        """
        Parameters
        ----------
        x,y : array_like
            1-D arrays of coordinates in strictly ascending order.
        """
        if x is None:
            xmin = self.recoLogE.min()
            xmax = self.recoLogE.max()
            xsteps = nsteps
            x = n.linspace(xmin,xmax,xsteps)
        else:
            x = n.asarray(x)
        if y is None:
            ymin = self.recoCosZ.min()
            ymax = self.recoCosZ.max()
            ysteps = nsteps
            y = n.linspace(ymin,ymax,ysteps)
        else:
            y = n.asarray(y)
        rate_density = self.func_rate_density
        X,Y = n.meshgrid(x,y)
        XYpos = n.vstack([X.ravel(),Y.ravel()])
        Zvalues = n.reshape(rate_density(XYpos).T, X.shape)
        if method == "interp2d":
            if not kwargs.has_key("kind"):
                kwargs["kind"] = "linear"
            if not kwargs.has_key("bounds_error"):
                kwargs["bounds_error"] = True
            self.spline = interpolate.interp2d(x,y,Zvalues,**kwargs)
        elif method == "RectBivariateSpline":
            self.spline = interpolate.RectBivariateSpline(x,y,Zvalues.T,**kwargs)
        else:
            raise AttributeError("`{0}` is not a valid method!".format(method))
        return self.spline
