from gaisser_class import gaisser

import numpy as n
from glob import glob

import tables
import ConfigParser
import argparse
import os
import time

# Input arguments
parser = argparse.ArgumentParser()
parser.add_argument("--det_config", type=str)
parser.add_argument("--use_std_atmos", action='store_true')
args = parser.parse_args()

print("Load config ....")
cfg = ConfigParser.RawConfigParser()
cfg.optionxform = str
cfg.read("config.cfg")
for sec in ["samples","eff_temp_calculation"]:
    for param in cfg.options(sec):
        exec("%s=%s" %(param, cfg.get(sec,param)))
        print param,eval(param)
print("Done!\n")

year = args.det_config#.split("_")[0]
start_date = start_date[year]
end_date = end_date[year]

outfile = "resources/effective_temperature/%s_eff_temp" %(args.det_config)
if args.use_std_atmos:
    outfile += "_std_atmosphere.hdf"
else:
    outfile += ".hdf"

# Input data
input_dir = "%s/resources" %os.path.dirname(os.path.abspath(__file__))
temp_data_dir = "/data/user/schoenen/software/python-modules/temp_variation/resources"
#input_dir = "/data/user/schoenen/software/python-modules/temp_variation/resources"
files = { 
    "effective_area":   "%s/effective_areas/%s_eff_area.txt" %(input_dir,args.det_config),
    "temperature":      sorted(glob("%s/data/temperature/*temp*daily.txt"
                                    %temp_data_dir)),
    "altitude":         sorted(glob("%s/data/altitude/*alti*daily.txt"
                                    %temp_data_dir)),
    "std_temperature":  "%s/data/temperature_std_atmos.txt" %temp_data_dir,
    "std_altitude":     "%s/data/altitude_std_atmos.txt" %temp_data_dir,
}

print("Loading effective areas ....")
effective_area = n.loadtxt(files["effective_area"], comments="#")[:,:1:-1]
print("Done!\n")

print("Loading data ....")
if args.use_std_atmos:
    dates = ["std_atmos"]
    temp = [n.atleast_2d(n.loadtxt(files["std_temperature"], comments="#", usecols=range(3,len(press_list)+3))[::-1]).repeat(180,axis=0)]
    alti = [n.atleast_2d(n.loadtxt(files["std_altitude"], comments="#", usecols=range(3,len(press_list)+3))[::-1]).repeat(180,axis=0)]
else:
    with open(files["temperature"][0], 'r') as f:
        dates = [(x.split()[0]) for i,x in enumerate(f.readlines()) if i > 0]
        idx_start,idx_end = [i for i in xrange(len(dates))\
            if time.strptime(dates[i], "%Y/%m/%d") == time.strptime(start_date,"%Y-%m-%d") or\
            time.strptime(dates[i], "%Y/%m/%d") == time.strptime(end_date,"%Y-%m-%d")]
        dates = dates[idx_start:idx_end]

    temp = n.zeros((len(dates),180,len(press_list))) # 180 breitengrade
    alti = n.zeros((len(dates),180,len(press_list))) # 180 breitengrade
    for ifile in xrange(len(files["temperature"])):
        tempdata = n.loadtxt(files["temperature"][ifile], comments="#", usecols=range(3,len(press_list)+3))[idx_start:idx_end]
        altidata = n.loadtxt(files["altitude"][ifile], comments="#", usecols=range(3,len(press_list)+3))[idx_start:idx_end]
        for iday in xrange(len(dates)):
            for ipress in xrange(len(press_list)):
                temp[iday][ifile][-(ipress+1)] = tempdata[iday][ipress]
                alti[iday][ifile][-(ipress+1)] = altidata[iday][ipress] 
print("Done!\n")

funcs = gaisser(Ebins,Zbins,press_list)
print("Calculate effective temperatures ....")
eff_temp = []
for i in xrange(len(dates)):
    if i%20 == 0:
        if i < len(dates)-20:
            print("%s - %s" %(dates[i],dates[i+19]))
        else:
            print("%s - %s" %(dates[i],dates[-1]))
    xtemp,costh,h = funcs.get_temp_local_costh(alti[i],temp[i]) # temp. and alti. for zenith angle (seen from icecube)
    x,dx = funcs.calc_slant_x_and_dx(costh,h) # slant_depth (integrated) and slant depth for pressure-costh bin
    weigth = funcs.get_weight_temp(x,dx,costh,xtemp,effective_area) # integral(weights(X) T(X) dX)
    teff_th = funcs.get_teff_temp_th(weigth,x,dx,xtemp) # effective temperature
    eff_temp.append(teff_th)
print("Done!\n")

# Now average over the whole year. Actually one has to take into account the real livetime for a day.
print("Save data ....")
hdfFile = tables.open_file(outfile, "w")
gcolumns = hdfFile.create_group(hdfFile.root, args.det_config, "Detector configuration")
hdfFile.create_array(gcolumns, "effective_temperature", n.array(eff_temp), "Effective temperatures for a day")
hdfFile.create_array(gcolumns, 'dates', dates, "Date for row")
hdfFile.create_array(gcolumns, "cosZ", Zbins, "cosine theta values")
hdfFile.create_array(gcolumns, "Enu", funcs.Emu, "neutrino energies in GeV")
print hdfFile
hdfFile.close()
print("Done!\n")
