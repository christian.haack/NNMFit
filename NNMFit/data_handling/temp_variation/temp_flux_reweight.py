import numpy as n

import cPickle as pickle
import numexpr

import os


"""
det_confs =  ["IC2009_Aachen,"IC2010_Aachen","IC2011_Aachen"]
"""

def inclination(cosZ):
    """
    Calculation of the inclination of a primary particle in dependency
    of the zenith angle with respect to IceCube.
    The formula takes into account the interaction height.
    For more information see: arXiv:hep-ph/0407078

    Parameters
    ----------
    cosZ : ndarray
        The cosine of the zenith angle of the particle.
    """

    p0 = 0.102573
    p1 = -0.068287
    p2 = 0.958633
    p3 = 0.0407253
    p4 = 0.817285
    denominator = 1 + p0**2 + p1 + p3

    cosZ = abs(cosZ)
    return numexpr.evaluate("sqrt((cosZ**2 + p0**2 + p1 * cosZ**p2 + p3 * cosZ**p4)/denominator)")

warned = False
def calc_temp_reweight_fact(det_conf,energy,coszen,verbose=True): 
    """
    Calculation of the temperature reweighting factor.
    For more information see: [internal-report:blahblahblah]

    Parameters
    ----------
        det_conf: str
            Detector Configuration ["IC59_diffuse","IC79_diffuse","IC86_diffuse"]
        energy: float
            Energy of the neutrino in GeV
        coszen: float
            cos(zenith) of the neutrino

        verbose: bool
            Toggle verbosity
    """

    cur_path = os.path.dirname(os.path.realpath(__file__))
    eff_temps_std = pickle.load(open(os.path.join(cur_path,"resources/splines/eff_temp_std_splines.pickle"),"r")) # Eff. Temperature for US-Standard-Atmosphere
    eff_temps_sat = pickle.load(open(os.path.join(cur_path,"resources/splines/eff_temp_sat_splines.pickle"),"r")) # Eff. Temperature from satellite data
    alpha = pickle.load(open(os.path.join(cur_path,"resources/splines/alpha_splines.pickle"),"r")) #Correlation Coefficient


    def calc_dr(det_conf,energy,coszen):
        """
        This function calculates the relative change in rate dr/r
        """

        dt_t = (eff_temps_sat[det_conf](coszen) - eff_temps_std[det_conf](coszen))/ eff_temps_std[det_conf](coszen)
        dr_r = alpha[det_conf](inclination(coszen)*energy) * dt_t
        return dr_r

    def warn_invalid_coszen():
        print "WARNING: Weight requested for invalid coszen. " \
              "Temperature data is only available up to coszen={0}. " \
              "Using coszen={0} for invalid points.".format(valid_up_to_coszen)


    valid_up_to_coszen = 0.1 # Angles < 85deg not supported
    dr_r = calc_dr(det_conf,energy,coszen)
    weights = 1.+dr_r

    #print coszen, coszen.shape, type(coszen)
    #Check if user requested weights for angles < 85deg, and set weights to weight at 85deg
    if type(coszen) == n.ndarray:
        mask = coszen>valid_up_to_coszen
        if n.any(mask):
            if verbose:
                warn_invalid_coszen()
            #indices = n.arange(len(coszen))[mask]
            if type(energy) == n.ndarray:
                weights[mask] = 1+calc_dr(det_conf,energy[mask],valid_up_to_coszen)
            else:
                weights[mask] = 1+calc_dr(det_conf,energy,valid_up_to_coszen)
    else:
        if coszen > valid_up_to_coszen:
            if verbose:
                warn_invalid_coszen()
            weights = 1+calc_dr(det_conf,energy,valid_up_to_coszen)
    print weights
    return weights
