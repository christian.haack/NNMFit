import matplotlib
matplotlib.use("Agg")

import numpy as n
import pylab as p
from scipy.interpolate import UnivariateSpline

import cPickle as pickle
import tables
import argparse
import os

from temp_flux_reweight import inclination

parser = argparse.ArgumentParser()
parser.add_argument("--rkpi", type=float, help="K/Pi-ratio", required=False, default=0.149, dest="rkpi")
parser.add_argument("--use_photospline", action="store_true", dest="use_photospline")
args = parser.parse_args()

if args.use_photospline:
    plot_suffix = "photospline"
    try:
        from icecube.photospline import spglam,splinefitstable
    except ImportError:
        raise ImportError("IceCube software environment not loaded.")
else:
    plot_suffix = "scipy"

dir_alpha = os.path.join(os.path.dirname(__file__),"resources/correlation_alpha")
dir_temp  = os.path.join(os.path.dirname(__file__),"resources/effective_temperature")

samples = {
    "IC2009_Aachen" : {
                "eff_temp"      : "IC2009_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2009_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2009_Aachen_alphas.hdf",
    },
    "IC2010_Aachen" : {
                "eff_temp"      : "IC2010_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2010_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2010_Aachen_alphas.hdf",
    },
    "IC2011_Aachen" : {
                "eff_temp"      : "IC2011_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2011_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2011_Aachen_alphas.hdf",
    },
    "IC2012_Aachen" : {
                "eff_temp"      : "IC2012_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2012_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2012_Aachen_alphas.hdf",
    },
    "IC2013_Aachen" : {
                "eff_temp"      : "IC2013_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2013_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2013_Aachen_alphas.hdf",
    },
    "IC2014_Aachen" : {
                "eff_temp"      : "IC2014_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2014_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2014_Aachen_alphas.hdf",
    },
    "IC2015_Aachen" : {
                "eff_temp"      : "IC2015_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2015_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2015_Aachen_alphas.hdf",
    },
    "IC2012-13-14_Aachen" : {
                "eff_temp"      : "IC2012-13-14_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2012-13-14_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2012-13-14_Aachen_alphas.hdf",
    },
    "IC2012-13-14-15_Aachen" : {
                "eff_temp"      : "IC2012-13-14-15_Aachen_eff_temp.hdf",
                "eff_temp_std"  : "IC2012-13-14-15_Aachen_eff_temp_std_atmosphere.hdf",
                "alpha"         : "IC2012-13-14-15_Aachen_alphas.hdf",
    },
}


def get_eff_temp(config):
    global dir_temp,samples
    hdf_sat = tables.open_file(os.path.join(dir_temp,samples[config]["eff_temp"]))
    node_sat = hdf_sat.get_node("/%s" %config)
    hdf_std = tables.open_file(os.path.join(dir_temp,samples[config]["eff_temp_std"]))
    node_std = hdf_std.get_node("/%s" %config)

    cosZ = node_sat.cosZ.read()
    dates_sat = node_sat.dates.read()
    print "start day = %s" %dates_sat[0]
    print "end day = %s" %dates_sat[-1]
   
    eff_temps_sat = node_sat.effective_temperature.read()
    eff_temps_sat_ma = n.ma.masked_array(eff_temps_sat,eff_temps_sat==0)
    Teff_sat = n.mean(eff_temps_sat_ma,axis=0) 
    
    Teff_std = node_std.effective_temperature.read()[0]

    hdf_sat.close()
    hdf_std.close()
    
    return cosZ, Teff_sat, Teff_std

def get_alphas(config):
    global dir_alpha,samples
    hdf = tables.open_file(os.path.join(dir_alpha,samples[config]["alpha"]))
    node = hdf.get_node("/%s" %config)

    Enu = node.Enu.read()
    cosZ = node.cosZ.read()
    rkpi = node.rkpi.read()
    alphas = node.alphas.read()

    hdf.close()

    return Enu, cosZ, rkpi, alphas


effTemps_sat = {}
effTemps_std = {}
alpha_splines = {}
for config in sorted(samples.keys()):
    print "config: %s" %config
    cosZ, Teff_sat, Teff_std = get_eff_temp(config)
    mask =  Teff_sat.mask 
    
    #SAT MISSES COSTHETA = -1    
    #ct_fix =  cosZ[:-1]    
   
    if args.use_photospline:
        order = 2
        smooth = 1e-15
        w = n.ones(Teff_sat[~mask][::-1].shape)
        knots = n.linspace(-2.,0.6,50)

        Teff_sat_spline = spglam.fit(Teff_sat[~mask][::-1], w, [cosZ[~mask][::-1]], [knots], order, smooth)
        Teff_sat_func = lambda theta: spglam.grideval(Teff_sat_spline, [theta])
        Teff_std_spline = spglam.fit(Teff_std[::-1], w, [cosZ[::-1]], [knots], order, smooth)
        Teff_std_func = lambda theta: spglam.grideval(Teff_std_spline, [theta])

        splinefitstable.write(Teff_sat_spline, "resources/splines/%s_eff_temp_sat_spline.fits" %config)
        splinefitstable.write(Teff_std_spline, "resources/splines/%s_eff_temp_std_spline.fits" %config)
    else:
        order = 2
        smooth = 0.2
        Teff_sat_spline = UnivariateSpline(cosZ[~mask][::-1], Teff_sat[~mask][::-1], k=order, s=smooth)
        Teff_sat_func = lambda theta: Teff_sat_spline(theta)
        Teff_std_spline = UnivariateSpline(cosZ[::-1], Teff_std[::-1], k=order, s=smooth)
        Teff_std_func = lambda theta: Teff_std_spline(theta)

        effTemps_sat[config] = Teff_sat_spline
        effTemps_std[config] = Teff_std_spline

    p.figure()
    p.plot(cosZ,Teff_sat,"b.",label="Sat")
    thetas = n.linspace(cosZ.max(),cosZ.min(),100)
    p.plot(thetas,Teff_sat_func(thetas),"r-",linewidth=2,label="sat. spline")
    p.grid()
    p.ylim(210,230)
    p.legend(loc="best")
    p.savefig("resources/plots/eff_temps_sat_%s_%s.png" %(config,plot_suffix))
   
    p.figure()
    p.plot(cosZ,Teff_std,"b.",label="Std")
    thetas = n.linspace(cosZ.max(),cosZ.min(),100)
    p.plot(thetas,Teff_std_func(thetas),"r-",linewidth=2,label="std. spline")
    p.grid()
    p.ylim(220,240)
    p.legend(loc="best")
    p.savefig("resources/plots/eff_temps_std_%s_%s.png" %(config,plot_suffix))
    
    ebins,cosZ,r_k_pi,alphas = get_alphas(config)
    idx = n.arange(len(r_k_pi))[r_k_pi == args.rkpi][0]
    costheta_star0 = inclination(cosZ[0])

    if args.use_photospline:
        order = 1
        smooth = 1e-15
        w = n.ones(alphas[idx][0].shape)
        knots = n.logspace(n.log10(0.5*ebins.min()),n.log10(2.*ebins.max()),50)

        alpha_spline = spglam.fit(alphas[idx][0], w, [ebins*costheta_star0], [knots], order, smooth)
        alpha_func = lambda x: spglam.grideval(alpha_spline, [x])

        splinefitstable.write(alpha_spline, "resources/splines/%s_alpha_spline.fits" %config)
    else:
        order = 1
        smooth = 0
        alpha_spline = UnivariateSpline(ebins*costheta_star0, alphas[idx][0], k=order, s=smooth)
        alpha_func = lambda x: alpha_spline(x)

        alpha_splines[config] = alpha_spline

    p.figure()
    for i in xrange(len(alphas[idx])):
        if i%20 == 0:
            p.plot(inclination(cosZ[i])*ebins, alphas[idx][i],
                   label="$\\cos\\theta^* = %.2f$" %inclination(cosZ[i]))
    En = n.logspace(n.log10(ebins.min()),n.log10(ebins.max()),100)
    x = En*costheta_star0
    p.plot(x,alpha_func(x),label="spline")
    p.legend(loc="best")
    p.semilogx()
    p.savefig("resources/plots/alpha_%s_rkpi_%.3f_%s.png" %(config,args.rkpi,plot_suffix))

if not args.use_photospline:
    pickle.dump(effTemps_std,open("resources/splines/eff_temp_std_splines.pickle","w"))
    pickle.dump(effTemps_sat,open("resources/splines/eff_temp_sat_splines.pickle","w"))
    pickle.dump(alpha_splines,open("resources/splines/alpha_splines.pickle","w"))
