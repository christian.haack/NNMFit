import numpy as n
import ConfigParser
import os

class gaisser:
    def __init__(self,Ebins,Zbins,press_list):
        self.Zbins = Zbins
        self.press_list = press_list


        print("Load constants ....")
        cfg = ConfigParser.RawConfigParser()
        cfg.optionxform = str
        cfg.read("%s/constants.cfg" %(os.path.dirname(__file__)))
        const = {}
        for sec in cfg.sections():
            for param in cfg.options(sec):
                const[param] = eval(cfg.get(sec,param))
                print param,const[param]
        self.rE = const["rE"]
        print("Done!\n")

        self.raddeg = 180./n.pi

        # number of cosine zenith bins
        self.nbinsZ = len(Zbins)
        # number of energy bins
        self.nbinsE = len(Ebins)-1
        # bincenters of effective areas in energy (not log energy!!!!)
        self.Emu = 10**(Ebins[:-1] + 0.5*n.diff(Ebins))
        # binwidths of effective areas in energy
        self.dEmu = n.diff(10**Ebins)
        # number of slant depth bins
        self.npress = len(press_list)

        #############################################################################################
        def epsilon(T, mass, lifetime):
            return (const["R"] * T)/(const["M"] * const["g"]) * mass/(const["c"] * lifetime)

        self.get_epsilon_pi = lambda T: epsilon(T,const["m_pi"],const["tau_pi"])
        self.get_epsilon_K  = lambda T: epsilon(T,const["m_K"],const["tau_K"])

        #############################################################################################
        def A_xmu(X, BR_xmu, Z_Nx, r_x):
            return BR_xmu * (Z_Nx/const["lambda_N"]) * ((1. - r_x)**const["gamma"])/(const["gamma"] + 1.) * n.exp(-X/const["Lambda_N"])

        def Z_Nx(type,rkpi):
            if type == "pion":
                return 0.0908/(1. + rkpi)
            elif type == "kaon":
                return -0.0908/(1. + rkpi) + 0.0908
            else:
                raise AttributeError("Unknown type: %s" %type)

        self.get_A_pimu = lambda X,rkpi=const["rkpi"]: A_xmu(X, const["BR_pimu"], Z_Nx("pion",rkpi), const["r_pi"])
        self.get_A_Kmu  = lambda X,rkpi=const["rkpi"]: A_xmu(X, const["BR_Kmu"], Z_Nx("kaon",rkpi), const["r_K"])

        #############################################################################################
        def B_xmu(X, r_x, Lambda_x):
            B_xmu = (const["gamma"] + 2.)/(const["gamma"] + 1.) * 1./(1. - r_x) * (Lambda_x - const["Lambda_N"])/(Lambda_x * const["Lambda_N"])
            B_xmu *= X * n.exp(-X/const["Lambda_N"])/(n.exp(-X/Lambda_x) - n.exp(-X/const["Lambda_N"]))
            return B_xmu

        self.get_B_pimu = lambda X: B_xmu(X, const["r_pi"], const["Lambda_pi"])
        self.get_B_Kmu  = lambda X: B_xmu(X, const["r_K"], const["Lambda_K"])

        #############################################################################################
        def get_flux(E):
            primary = 1.8e4 * E**(-(const["gamma"]+1.0))
            # take into account the knee
            if (E > 3e6) :
                primary *= (3e6/E)**0.30
            return primary

        # primary CR flux at each bincenter
        self.flux = n.array([get_flux(En) for En in self.Emu])

    #############################################################################################
    def calc_slant_x_and_dx(self,loc_costh_list, h, curvature_correction=True):
        # temp profiles are vertical -> calc slant depth by integration
        x = n.zeros((self.nbinsZ,self.npress))
        dx = n.zeros((self.nbinsZ,self.npress))

        # get slant depth X
        for ith in range(self.nbinsZ):
            xdepth = 0.
            for ipress in range(self.npress):
                if ipress == 0:
                    dpress = self.press_list[ipress]
                else:
                    dpress = self.press_list[ipress] - self.press_list[ipress-1]

                if loc_costh_list[ith][ipress] != 0.:
                    if curvature_correction:
                        # some geometric equation that takes into account the curvature of the earth in order to calculate the path distance in one atmospheric pressure level
                        # dh/dl = ...
                        dl = n.sqrt(loc_costh_list[ith][ipress]**2 +(2.*h[ith][ipress])*(1.-loc_costh_list[ith][ipress]**2)/self.rE)
                        # h = cosTheta l -> dh/dl = cosTheta
                        # dl = loc_costh_list[ith][ipress] 
                        # 1/(gravitational const) = 1./9.81 s^2/m = 1.019e-3 s^2/cm
                        xdepth += 1.019e-3 * (dpress*1e3)/dl
                    else:
                        xdepth += 1.019e-3 * (dpress*1e3)/loc_costh_list[ith][ipress] 

                x[ith][ipress] = xdepth

        # get dx
        # average the differential slant depth
        for ith in range(self.nbinsZ):
            for ipress in range(self.npress):
                if ipress == 0: #from top (0hPa)
                    dx[ith][ipress] = n.sqrt(x[ith][ipress+1] * x[ith][ipress])
                elif(ipress==self.npress-1):# surface (1000hPa)
                    dx[ith][ipress] = x[ith][ipress] - n.sqrt(x[ith][ipress] * x[ith][ipress-1])
                else:
                    dx[ith][ipress]= n.sqrt(x[ith][ipress+1] * x[ith][ipress]) - n.sqrt(x[ith][ipress] * x[ith][ipress-1])
        return x,dx

    #############################################################################################
    def get_temp_local_costh(self,alti,temp):
        # init h, costh, xtemp
        h = n.zeros((self.nbinsZ,self.npress))
        costh_loc = n.zeros((self.nbinsZ,self.npress))
        xtemp = n.zeros((self.nbinsZ,self.npress))

        xtheta = n.arccos(self.Zbins) #+pi/2.
        xcolat = 2*n.pi - 2*xtheta #!colat at surface
        # colatitude at the surface
        ilat0 = (xcolat*self.raddeg).astype(int) #0 to 179 each -1 used as indices
        # if ilat0 > 179 dann setze ilat0 = 179
        for i in range(len(ilat0)):
            if ilat0[i] > 179:
                ilat0[i] = 179
        
        for ith in range(self.nbinsZ): 
            for ipress in range(self.npress): 
                if  (alti[ilat0[ith]][ipress]!=-1):
                    costh_loc[ith][ipress] = n.sqrt(1. - (self.rE/(self.rE+alti[ilat0[ith]][ipress])*n.sqrt(1.-self.Zbins[ith]**2))**2)
                else: 
                    continue
                    
                h[ith][ipress] = alti[ilat0[ith]][ipress]
                
                xcolat[ith] = self.raddeg*(n.arccos(costh_loc[ith][ipress]) - xtheta[ith]) + 180. #ok
                ilat = int(xcolat[ith]) #0 to 179 each -1 used as indices
                if ilat > 179:
                    ilat = 179
                if temp[ilat][ipress] != -1:
                    xtemp[ith][ipress] = temp[ilat][ipress]
                else: 
                    continue
        return xtemp,costh_loc, h

    #############################################################################################
    def get_weight_temp(self,x,dx,xcosth,xtemp,aeff):
        # pion / kaon part in gaisser formula
        W_pi = n.zeros((self.nbinsZ,self.npress))
        W_K = n.zeros((self.nbinsZ,self.npress))
        # sum weights pion / kaon
        sW_pi = n.zeros(self.nbinsZ)
        sW_K = n.zeros(self.nbinsZ)
        # sum pion + kaon in gaisser formula (normalized to 1)
        weight = n.zeros((self.nbinsZ,self.npress))
        # critical energies
        eps_pi = self.get_epsilon_pi(xtemp)
        eps_K = self.get_epsilon_K(xtemp)

        ratio_nbins_costh = int(float(self.nbinsZ)/len(aeff[0]))
        for ith in range(self.nbinsZ):
            for ipress in range(self.npress): 
                # epsilon is zero when temperature is zero
                if (xcosth[ith][ipress] !=0  and xtemp[ith][ipress]!=0):
                    P_pimu = 0.
                    P_Kmu = 0.

                    Api = self.get_A_pimu(x[ith][ipress])                
                    AK = self.get_A_Kmu(x[ith][ipress])

                    Bpi = self.get_B_pimu(x[ith][ipress])
                    BK = self.get_B_Kmu(x[ith][ipress])
                    
                    if ith == self.nbinsZ-1:
                        i_aeff = len(aeff[0]) - 1
                    else:
                        i_aeff = ith/ratio_nbins_costh

                    for iEn in range(self.nbinsE):    
                        P_pimu += self.dEmu[iEn]*self.flux[iEn]*Api*aeff[iEn][i_aeff]/(1.0 + (Bpi*self.Emu[iEn]*xcosth[ith][ipress])/eps_pi[ith][ipress])
                        P_Kmu += self.dEmu[iEn]*self.flux[iEn]*AK *aeff[iEn][i_aeff]/(1.0 + (BK *self.Emu[iEn]*xcosth[ith][ipress])/eps_K[ith][ipress] )

                    W_K[ith][ipress] = P_Kmu    
                    W_pi[ith][ipress] = P_pimu

                    sW_pi[ith] += W_pi[ith][ipress]*dx[ith][ipress]
                    sW_K[ith] += W_K[ith][ipress]*dx[ith][ipress]
        
        W_piK = W_pi + W_K
        sW_piK = sW_K + sW_pi
        for ith in range(self.nbinsZ):
            for ipress in range(self.npress):
                if sW_piK[ith] != 0.:
                    weight[ith][ipress] = W_piK[ith][ipress]/(sW_piK[ith]) 
        return weight

    #############################################################################################
    def get_teff_temp_th(self,weight,x,dx,temp):
        sum1 = n.zeros(self.nbinsZ)
        sum2 = n.zeros(self.nbinsZ)
        teff_th = n.zeros(self.nbinsZ)
        for ith in range(self.nbinsZ):
            for ipress in range(self.npress):
                sum1[ith] += temp[ith][ipress]*weight[ith][ipress]*dx[ith][ipress]
                sum2[ith] += weight[ith][ipress]*dx[ith][ipress] 

        for i in range(self.nbinsZ):
            if sum2[i] != 0:
                teff_th[i] = sum1[i]/sum2[i]
        return teff_th

    ##############################################################################################
    def calc_alpha(self,temp,alti,rkpi=0.149,curvature_correction=True):
        alphaE = n.zeros((self.nbinsZ,self.nbinsE))

        xtemp,xcosth,h = self.get_temp_local_costh(alti,temp)
        x,dx = self.calc_slant_x_and_dx(xcosth,h,curvature_correction=curvature_correction)

        # critical energies
        eps_pi = self.get_epsilon_pi(xtemp)
        eps_K = self.get_epsilon_K(xtemp)

        for ith in range(self.nbinsZ):
            for iE in range(self.nbinsE):
                xsum_nu = 0.
                xsum_de = 0.
                for ipress in range(self.npress):
                    Api = self.get_A_pimu(x[ith][ipress],rkpi)                
                    AK = self.get_A_Kmu(x[ith][ipress],rkpi)

                    Bpi = self.get_B_pimu(x[ith][ipress])
                    BK = self.get_B_Kmu(x[ith][ipress])

                    B_pimu_x_2 = Bpi * xcosth[ith][ipress] * self.Emu[iE] / eps_pi[ith][ipress]
                    B_Kmu_x_2 = BK * xcosth[ith][ipress] * self.Emu[iE] / eps_K[ith][ipress]
            
                    xsum_nu += dx[ith][ipress] * (Api*B_pimu_x_2/(1.+B_pimu_x_2)**2 + AK*B_Kmu_x_2/(1.+B_Kmu_x_2)**2)   
                    xsum_de += dx[ith][ipress] * (Api/(1.+B_pimu_x_2) + AK/(1.+B_Kmu_x_2))

                alphaE[ith][iE] = self.dEmu[iE]*self.flux[iE]*xsum_nu
                fluxE = self.dEmu[iE]*self.flux[iE]*xsum_de
                if fluxE != 0.:
                    alphaE[ith][iE] /= fluxE
        return alphaE
