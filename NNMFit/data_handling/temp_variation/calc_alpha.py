from gaisser_class import gaisser

import numpy as n
import tables
from glob import glob

import ConfigParser
import argparse
import os

# Input arguments
parser = argparse.ArgumentParser()
parser.add_argument("--det_config", type=str)
parser.add_argument("--use_std_atmos", action='store_true')
parser.add_argument("--cols", type=int, nargs=2, default=None)
args = parser.parse_args()

year = args.det_config.split("_")[0]
outfile = "resources/correlation_alpha/%s_alphas" %(args.det_config)
if args.use_std_atmos:
    outfile += "_std_atmosphere.hdf"
else:
    outfile += ".hdf"

#input data
#input_dir = "%s/resources" %os.path.dirname(os.path.abspath(__file__))
input_dir = "/data/user/schoenen/software/python-modules/temp_variation/resources"
files = {
    "temperature":      sorted(glob("%s/data/temperature_average/%s/airs_amsu_temp_average_pol_???.txt"  %(input_dir,year))),
    "altitude":         sorted(glob("%s/data/altitude_average/%s/airs_amsu_alti_average_pol_???.txt"     %(input_dir,year))),
    "std_temperature":  "%s/data/temperature_std_atmos.txt" %input_dir,
    "std_altitude":     "%s/data/altitude_std_atmos.txt" %input_dir,
}

print("Load config ....")
cfg = ConfigParser.RawConfigParser()
cfg.optionxform = str
cfg.read("config.cfg")
for sec in ["samples","alpha_calculation"]:
    for param in cfg.options(sec):
        exec("%s=%s" %(param, cfg.get(sec,param)))
        print param,eval(param)
print("Done!\n")

print("Loading data ....")
if args.use_std_atmos:
    temp = n.atleast_2d(n.loadtxt(files["std_temperature"], comments="#", usecols=range(3,len(press_list)+3))[::-1]).repeat(180,axis=0)
    alti = n.atleast_2d(n.loadtxt(files["std_altitude"], comments="#", usecols=range(3,len(press_list)+3))[::-1]).repeat(180,axis=0)
else:
    temp = n.zeros((180,len(press_list))) # 180 breitengrade
    alti = n.zeros((180,len(press_list))) # 180 breitengrade
    for ifile in xrange(len(files["temperature"])):
        if args.cols is None:
            usecols = [2]
            temp[ifile] = n.loadtxt(files["temperature"][ifile], comments="#", skiprows=1, usecols=usecols)
            alti[ifile] = n.loadtxt(files["altitude"][ifile], comments="#", skiprows=1, usecols=usecols)
        elif n.diff(args.cols)[0] == 0.:
            usecols = [args.cols[0]]
            temp[ifile] = n.loadtxt(files["temperature"][ifile], comments="#", skiprows=1, usecols=usecols)
            alti[ifile] = n.loadtxt(files["altitude"][ifile], comments="#", skiprows=1, usecols=usecols)
        else:
            usecols = range(args.cols[0],args.cols[1]+1)
            temp[ifile] = n.mean(n.loadtxt(files["temperature"][ifile], comments="#", skiprows=1, usecols=usecols),axis=1)
            alti[ifile] = n.mean(n.loadtxt(files["altitude"][ifile], comments="#", skiprows=1, usecols=usecols),axis=1)
print("Done!\n")

funcs = gaisser(Ebins,Zbins,press_list)
print("Calculate correlation coefficients alpha ....")
alphas = []
for i,r in enumerate(rkpis):
    if i == 0:
        print("std_rpik = %f" %r)
    elif (i-1)%10 == 0:
        print("rpik = %f" %r)
    alphas.append(funcs.calc_alpha(temp,alti,rkpi=r,curvature_correction=True))
print("Done!\n")

print("Save data ....")
hdfFile = tables.open_file(outfile, "w")
gcolumns = hdfFile.create_group(hdfFile.root, args.det_config, "Detector configuration")
hdfFile.create_array(gcolumns, "alphas", n.array(alphas), "Correlation coefficients")
hdfFile.create_array(gcolumns, "rkpi", rkpis, "pion to kaon ratios")
hdfFile.create_array(gcolumns, "cosZ", Zbins, "cosine theta values")
hdfFile.create_array(gcolumns, "Enu", funcs.Emu, "neutrino energies in GeV")
print hdfFile
hdfFile.close()
print("Done!\n")
