import matplotlib
matplotlib.use("Agg")
import pylab as plt
import numpy as n
import cPickle as pickle
import ConfigParser

import sys,os
sys.path = [os.path.join(os.path.dirname(os.path.abspath(__file__)),"..")]+sys.path

from temp_flux_reweight import calc_temp_reweight_fact

def plot_reweight_fact(det_conf):
    """
    Plot reweighting factor
    
    Parameters
    ----------
        det_conf: str
            Detector Configuration ["IC2009_Aachen","IC2010_Aachen","IC2011_Aachen"]

    """
    fig=plt.figure()

    energies = n.logspace(2,8,5)
    zeniths = n.linspace(-1,0.5,1E4)

    for e in energies:               
        plt.plot(zeniths,calc_temp_reweight_fact(det_conf,e,zeniths),label=r"$E_\nu$={0:.2e}GeV".format(e))
    plt.legend(loc="best",ncol=2,fontsize=10)
    plt.ylim(0.9,1.1)
    plt.grid()
    plt.title(det_conf)
    plt.xlabel("cos(Zenith)")
    plt.ylabel(r"$\Delta R / R$")
    return fig
    
    

def plot_dt(det_conf):
    """
    Plot relative temperature change dT/T
    
    Parameters
    ----------
        det_conf: str
            Detector Configuration ["IC2009_Aachen","IC2010_Aachen","IC2011_Aachen"]

    """
   
    cur_path = os.path.dirname(os.path.realpath(__file__))
    eff_temps_std = pickle.load(open(os.path.join(cur_path,"splines/eff_temp_std_splines.pickle"),"r")) # Eff. Temperature for US-Standard-Atmosphere
    eff_temps_sat = pickle.load(open(os.path.join(cur_path,"splines/eff_temp_sat_splines.pickle"),"r")) # Eff. Temperature from satellite data
    alpha = pickle.load(open(os.path.join(cur_path,"splines/alpha_splines.pickle"),"r")) #Correlation Coefficient
   
   
    coszen = n.linspace(-1,0.1,1E4)
    dt = (eff_temps_sat[det_conf](coszen) - eff_temps_std[det_conf](coszen))       
    dt_t = dt / eff_temps_std[det_conf](coszen) 
    
    fig1 = plt.figure()
    plt.ylim(-20,10)
    plt.grid()
    plt.plot(coszen,dt)
    plt.title(det_conf)
    plt.xlabel("cos(Zenith)")
    plt.ylabel(r"$\Delta T$")
    
    fig2 = plt.figure()
    plt.ylim(-0.1,0.1)
    plt.grid()
    plt.plot(coszen,dt_t)
    plt.title(det_conf)
    plt.xlabel("cos(Zenith)")
    plt.ylabel(r"$\Delta T / T_{\mathrm{std}}$")
    return fig1,fig2
    
if __name__ == "__main__":
    cfg = ConfigParser.RawConfigParser()
    cfg.optionxform = str
    cfg.read("../config.cfg")
    det_configs = eval(cfg.get("samples","det_configs"))
    for config in det_configs:
        fig=plot_reweight_fact(config)
        fig.savefig("plots/dr_r_{0}.png".format(config))

        fig1,fig2 = plot_dt(config)
        fig1.savefig("plots/dt_{0}.png".format(config))
        fig2.savefig("plots/dt_t{0}.png".format(config))

    print calc_temp_reweight_fact("IC2011_Aachen",n.logspace(1,8,10),n.linspace(-1,0.3,10))
