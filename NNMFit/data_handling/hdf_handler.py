import tables
import pandas as pd
import numpy as n
from collections import defaultdict

class HDFICData2(object):
    """
    Class for reading HDF files made from I3-Files
    """

    def __init__(self, path, feature_dict, query):
        """
        Constructor

        Parameters:
            -----------
            path: Path of the hdf file
            feature_dict: dict containing the features to be read

        """

        self.__feature_dict = feature_dict
        self.__path = path
        self.__data_dict = {}
        self.__query = query
        self.__data_frame = None
        self.__read_from_hdf()


    @property
    def feature_dict(self):
        return self.__feature_dict

    @property
    def data_frame(self):
        return self.__data_frame

    @data_frame.setter
    def data_frame(self, new_data_frame):
        self.__data_frame = new_data_frame

    def __iadd__(self, other):
        self.data_frame = pd.concat([self.data_frame, other.data_frame])
        return self

    def __radd__(self, other):
        if other is None:
            return self

    def __add__(self, other):
        return self.__iadd__(other)

    def __combine_data_frames(self, data_dict, query=None):
        """
        Convert a feature dict of arrays to a 2D array
        of shape (n_samples, n_features)

        """

        new_data_dict = pd.concat(data_dict.values(), axis=1, join="inner")

        for key, identifier in self.__feature_dict.iteritems():
            identifier, trafo = identifier
            if trafo is not None:
                new_data_dict[key] = trafo(new_data_dict[key])
                if query is not None:
                    new_data_dict = new_data_dict.query(query)

        return new_data_dict

    def __read_from_hdf(self):
        data_dict = {}
        with tables.open_file(self.__path) as handle:
            root_node = handle.root
            cols = defaultdict(list)
            keys = defaultdict(list)
            for key, identifier in self.__feature_dict.iteritems():
                identifier, _ = identifier
                node_name, col_name = identifier.split(".")
                cols[node_name].append(col_name)
                keys[node_name].append(key)
            for node_name in cols.keys():
                try:
                    node = root_node._f_get_child(node_name)
                    data = node.read()
                    # Convert to index columns to int64, since pytables
                    # doesnt like unsigned ints...
                    ind = pd.MultiIndex.from_arrays(
                        [n.asarray(data["Run"], dtype=n.int64),
                         n.asarray(data["Event"], dtype=n.int64),
                         n.asarray(data["SubEvent"], dtype=n.int64)])
                    exclude = [name for name in data.dtype.names
                               if name not in cols[node_name]]

                    node_frame = pd.DataFrame.from_records(data,
                                                           index=ind,
                                                           exclude=exclude)
                    ren_dict = {cols[node_name][i]: keys[node_name][i]
                                for i in xrange(len(cols[node_name]))}

                    node_frame = node_frame.rename(columns=ren_dict)

                    for key in keys[node_name]:
                        data_dict[key] = node_frame[key]
                except tables.exceptions.NoSuchNodeError:
                    print("##########", node_name)
                    print("At least one node does not exist. Skipping file")
                    return -1
            self.data_frame = self.__combine_data_frames(data_dict, self.__query)
