"""
Implementation of modification hooks
"""
import logging
import string
import cPickle as pickle
from scipy.special import erfc
import numpy as n
from temp_variation.temp_flux_reweight import calc_temp_reweight_fact
logger = logging.getLogger(__name__)

#pylint: disable=too-few-public-methods

class DataModHook(object):
    """
    Baseclass for data modification hooks
    """
    def apply_mod(self, data_dict):
        """
        Apply the modifications to the data dict

        Args:
            data_dict: Data dict from Dataset object
        """
        raise NotImplementedError("Needs to be implemented by subclass")

class EnergyScaling(DataModHook):
    """
    Implemention of a scaling factor on trueE
    """

    def __init__(self, scale_factor):
        self.scale_factor = float(scale_factor)

    def apply_mod(self, data_dict):
        """
        Apply the modifications to the data dict

        Args:
            data_dict: Data dict from Dataset object
        """
        logger.debug("Applying energy scaling: %s",
                     self.scale_factor)
        #data_dict["true_energy"] = n.where(data_dict["true_energy"]>3e5, ##this is the transition between low and medium energy sets
        #                                   data_dict["true_energy"],
        #                                   self.scale_factor*data_dict["true_energy"])
        #data_dict["true_energy"] = self.scale_factor*data_dict["true_energy"]
        data_dict["reco_energy"] = self.scale_factor*data_dict["reco_energy"]
        return data_dict



class BaselineCorrection2012(DataModHook):
    """
    Implemention of 2012 baseline correction
    The correction is obtained by reweighting the baseline MC to the
    systematics baseline MC using a sigmoid function
    """

    def __init__(self, corr_file):
        self.corr_file = corr_file

    def apply_mod(self, data_dict):
        """
        Apply the modifications to the data dict

        Args:
            data_dict: Data dict from Dataset object
        """
        with open(self.corr_file) as hdl:
            corr_params_dict = pickle.load(hdl)
        for flux, params in corr_params_dict.iteritems():
            logger.debug("Applying baseline correction for %s", flux)
            cor_func = lambda x, ps=params: ps["a"]*\
                    erfc(ps["b"]*(x - ps["c"])) + ps["d"]
            data_dict[flux] /= cor_func(n.log10(data_dict["reco_energy"]))
        return data_dict

class TemperatureCorrection(DataModHook):
    """
    Implementation of temperature correction

    Takes into account yearly changes of the atmosphere compared to
    the US standard atmosphere
    https://internal.icecube.wisc.edu/reports/data/icecube/2015/05/001/icecube_201505001_v2.pdf
    Needs temp_flux_reweight module
    """

    def __init__(self, year_ident, fluxes):
        """
        Args:
            year_ident: Identifier of the season to use for reweighting
            fluxes: Comma-separated list of flux names reweighting is
                    applied to.
        """
        self.year_ident = year_ident
        self.fluxes = map(string.strip, fluxes.split(","))

    def apply_mod(self, data_dict):
        """
        Apply the modifications to the data dict

        Args:
            data_dict: Data dict from Dataset object
        """
        for flux in self.fluxes:
            logger.debug("Applying temperature correction for %s", flux)
            data_dict[flux] *= calc_temp_reweight_fact(
                self.year_ident,
                n.array(data_dict["true_energy"].values),
                n.cos(data_dict["true_zenith"].values), verbose=False)
        return data_dict


class NuTauCorrection(object):
    """
    Class implementing the correciton for missing tau neutrinos in MC
    The correction is obtained by reweighting the baseline MC with a spline
    produced beforehand, see script at /resources/make_nutau_corrections.py
    """

    IS_SYS_PARAM = False

    def __init__(self, spline_file):
        self.spline_file = spline_file

    def apply_mod(self, data_dict):
        """
        Apply the modifications to the data dict

        Args:
            data_dict: Data dict from Dataset object
        """

        with open(self.spline_file) as hdl:
            spline_dict = pickle.load(hdl)

        for flux in ["powerlaw"]:
            logger.debug("Applying nutau/nue correction for %s", flux)

            zen_e_arr = data_dict.loc[:, ["reco_zenith", "reco_energy"]].values
            data_dict.loc[:, flux] *=  (1.+spline_dict[flux](zen_e_arr[:, 0],
                                                             zen_e_arr[:, 1],
                                                             grid=False))

        return data_dict

class NuTauCorrectionRatio(object):
    """
    Class implementing the correciton for missing tau neutrinos in MC
    The correction is obtained by reweighting the baseline MC with a
    correction-spline
    produced beforehand, see script at
    /resources/make_nutau_nue_corrections_ratio.py
    """

    IS_SYS_PARAM = False

    def __init__(self, spline_file):
        self.spline_file = spline_file

    def apply_mod(self, data_dict):
        """
        Apply the modifications to the data dict

        Args:
            data_dict: Data dict from Dataset object
        """

        with open(self.spline_file) as hdl:
            spline_dict = pickle.load(hdl)

        for flux in ["powerlaw"]:
            logger.debug("Applying nutau correction for %s", flux)
            logger.debug("Spline file (ratio base+tau / base)  %s",
                         self.spline_file)
            zen_e_arr = data_dict.loc[:, ["reco_zenith", "reco_energy"]].values
            data_dict.loc[:, flux] *=  (spline_dict[flux](zen_e_arr[:, 0],
                                                          n.log10(zen_e_arr[:,1]),
                                                          grid=False))

        return data_dict

