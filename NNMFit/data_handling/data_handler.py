"""
Collection of classes for handling data
"""
import logging
import tables
import pandas as pd
import operator
import numpy as n
from .. import utilities
import re
logger = logging.getLogger(__name__)


class HDFDataset(object):  # pylint: disable=too-few-public-methods
    """
    Proxy class for hdf files
    """
    def __init__(self, f_name):
        self._hdl = None
        self._f_name = f_name

    def __enter__(self):
        self._hdl = tables.open_file(self._f_name)
        return self

    def read_var(self, var_name):
        """
        Read var from first group in hdf file

        Args:
            var_name: name of variable to read
        """
        if self._hdl:
            node = self._hdl.list_nodes("/")[0]
            return node.col(var_name)[:]
        else:
            raise RuntimeError("Use as contextmanager `with`")

    def __exit__(self, *args):
        self._hdl.close()


class Dataset(object):  # pylint: disable=too-many-public-methods
    """
    Data handler for data/mc datasets
    """
    def __init__(self, f_name, key_mapping,
                 ra_oversampling=None,
                 cache=None,
                 random_seed=1337,
                 cols_to_read=None):
        self._fname = f_name
        self._data_dict = {}
        self._alias_dict = {}
        self._cols_to_read = cols_to_read
        self._read(key_mapping)
        self._random_state = n.random.RandomState(random_seed)
        self._cache = cache
        self.flattened_indices = None
        self.total_bins = None
        self.analysis_vars_keys = None
        self.n_events = len(self._data_dict)
        self._oversampled_ra = None
        self._oversampled_lat = None
        if ra_oversampling is not None:
            self.oversampling = True
            self.make_oversampled_ra(ra_oversampling)
            self.oversampling_factor = ra_oversampling
        else:
            self.oversampling = False

    def _read(self, key_mapping):
        logger.debug("Reading file: %s",self._fname)
        if self._cols_to_read is not None:
            logger.debug("Ignoring all keys except for : %s", self._cols_to_read)
            dframe = pd.read_hdf(self._fname, columns=self._cols_to_read)
        else:
            dframe = pd.read_hdf(self._fname)
        dframe.rename(columns=key_mapping, inplace=True)
        if "IC59" in self._fname:
            special_keys = ["reco_energy", "reco_zenith", "reco_ra"]
            special_keys_tt0 = [key + "_TT0" for key in special_keys]
            special_keys_tt0_exists = ["reco_dirTT0_exists"
                                       if "reco_ra" in key or "reco_zenith" in
                                       key else key + "_exists" for key in
                                       special_keys_tt0]
            for sk, sk_tt0, sk_tt0_ex in zip(special_keys, special_keys_tt0,
                                             special_keys_tt0_exists):
                # df.where returns self if condition is true
                dframe[sk].where(dframe[sk_tt0_ex]==0, dframe[sk_tt0],
                                 inplace=True)

            dframe["reco_energy_fit_status"] = n.zeros_like(
                dframe["reco_energy"])
        if "true_ptype" in dframe:
            dframe.loc[dframe["true_ptype"]==68, "true_ptype"] = 14
            dframe.loc[dframe["true_ptype"]==69, "true_ptype"] = -14
        self._data_dict = dframe

    def calc_n_events(self):
        self.n_events = len(self._data_dict)
        logger.debug("n_events in Dataset after masks: "+(str(self.n_events)))

    def apply_hooks(self, hooks):
        """
        Apply data modification hooks

        Args:
            hooks: list of DataModHooks
        """
        for hook in hooks:
            self._data_dict = hook.apply_mod(self._data_dict)

    def weight(self, key):
        """
        Return weight stored in `key`
        """
        return self._data_dict[key]

    def read_key_as_numpy(self, key):
        """
        Return a key as numpy array
        """
        if hasattr(self, key):
            """
            First try the attribute data acess. This way we make sure
            that special variables (ie. oversamples ones) can recieve
            special treatment
            """
            ret = getattr(self, key)
        else:
            """
            Fall back to _read_key method if no attribute exists
            """
            ret = self._read_key(key)
        try:
            """
            Convert to ndarray if ret is DataFrame
            """
            ret = ret.values
        except AttributeError:
            pass
        return ret

    def _read_key(self, key):
        """
        Return data stored in `key`
        """
        if key in self._alias_dict:
            print(key)
            key = self._alias_dict[key]
        return self._data_dict[key]

    def set_alias(self, key_name, alias_name):
        """Set an alias for a key"""
        self._alias_dict[alias_name] = key_name

    def set_analysis_vars(self, analysis_vars):
        logger.info("Setting analysis vars: %s", analysis_vars)
        self.analysis_vars_keys = list(analysis_vars)
        #self.analysis_vars = map(self._read_key, analysis_vars)

    def set_analysis_binning(self, binning):
        """
        Set analysis variables and binning

        Args:
            analysis_vars: List of variables names for x,y,(z)
            binning: Binedges for every variable
        """

        '''
        if self.oversampling and len(binning) != 3:
            raise RuntimeError(
                "Oversampling is only supported for 3 dimensions")
        '''
        if len(binning) not in [2, 3]:
            raise RuntimeError("Only 2 or 3 dimensions supported")

        if self.oversampling:
            if len(binning) == 3:
                logger.debug("Oversampling enabled, replacing reco with oversampled key")
                binning_f = utilities.calc_indices_3d_oversampling
                #self.analysis_vars_keys[2] = self.analysis_vars_keys[2].replace("reco", "oversampled")
                self.analysis_vars_keys[2] = "oversampled_ra"
            else:
                binning_f = utilities.calc_indices_2d
        else:
            if len(binning) == 2:
                binning_f = utilities.calc_indices_2d
            else:
                binning_f = utilities.calc_indices_3d
        self.flattened_indices = binning_f(*self.analysis_vars,
                                           binning=binning)
        self.total_bins = reduce(operator.mul,
                                 [len(bins)-1 for bins in binning])

    def memory_usage(self):
        total = 0
        for key, val in self._data_dict.iteritems():
            usage = val.nbytes/(1024**2)
            print("{} using {} MB".format(key, usage))
            total += usage
        print("Total usage: {} MB".format(total))

    def order_data(self, sorted_args):
        """
        Apply in-place sorting of all data

        Args:
            sorted_args: index array
        """
        # pylint: disable=consider-iterating-dictionary
        raise NotImplementedError("Sorting is deprecated")

    def set_mask(self, mask):
        """
        Apply mask in-place to every key
        """
        # pylint: disable=consider-iterating-dictionary

        self._data_dict = self._data_dict[mask]
        self.n_events = len(self._data_dict)
        if self.flattened_indices is not None:
            self.flattened_indices = self.flattened_indices[mask]
        if self.oversampling:
            mask_bv = n.asarray(mask, dtype=n.bool)
            self._oversampled_lat = self._oversampled_lat[:, mask_bv]
            self._oversampled_lon = self._oversampled_lon[:, mask_bv]
            self._oversampled_ra = self._oversampled_ra[:, mask_bv]

        logger.debug("Applying mask, new n_events: "\
                     +(str(mask.sum())))

    def set_standard_mask(self):
        """
        Apply standard mask to every key


        The standard mask checks the existance and fit-status
        of the energy and direction reconstruction
        """
        if "IC59" not in self._fname:
            mask = (self._read_key("reco_energy_exists") == 1) & \
                   (self._read_key("reco_energy_fit_status") == 0)
            mask &= (self._read_key("reco_dir_exists") == 1) & \
                    (self._read_key("reco_dir_fit_status") == 0)
            self.set_mask(mask)
        else:
            logger.info("Using IC59 datasets, not setting standard mask")

    def make_binning_mask(self, binning):
        """
        Apply reco energy mask given binning to every key

        Args:
            binning: tuple of binedges
        """
        mask = n.ones(self.n_events, dtype=n.bool)
        for i, key in enumerate(self.analysis_vars_keys):
            mask &= self._read_key(key) > binning[i].min()
            mask &= self._read_key(key) < binning[i].max()
        logger.debug("Found %d events in binning", mask.sum())
        #self.set_mask(mask)
        return mask

    def make_additional_mask(self, condition_str):
        """
        Apply additional cut, defined in the cfg file before

        Args:
            condition: var, opertator, cut
        """
        def parse_condition(cond_string):
            regex = re.compile(r"\((.+?),(.+?),(.+?)\)")
            res = regex.findall(cond_string)
            conditions = [[item.strip(" ") for item in condlist] for condlist in res]
            return conditions

        conditions = parse_condition(condition_str)
        def mask_from_cond(condition):
            (var_key, operator, cut) = condition
            if operator=="<":
                mask = self._read_key(var_key) < float(cut)
            elif operator==">":
                mask = self._read_key(var_key) > float(cut)
            elif operator=="==":
                mask = self._read_key(var_key) == float(cut)
            elif operator=="=":
                mask = self._read_key(var_key) == float(cut)
            elif operator=="in":
                cut = cut.strip("(").strip(")").split(",")
                mask = self._read_key(var_key) > float(cut[0])
                mask &= self._read_key(var_key) < float(cut[1])
            elif operator=="notin":
                cut = cut.strip("(").strip(")").split(",")
                mask = self._read_key(var_key) > float(cut[0])
                mask &= self._read_key(var_key) < float(cut[1])
                mask = ~mask
            elif operator=="IceFlow":
                mask1 =  ((self._read_key(var_key) > 1.47) &\
                         (self._read_key(var_key) < 3.04))
                mask2 = ((self._read_key(var_key) > 5.4015) &\
                         (self._read_key(var_key) < 6.186))
                mask = n.ma.mask_or(mask1, mask2)
                if cut=="along":
                    mask = mask
                elif cut=="orthogonal":
                    mask = ~mask
            elif operator=="mask_bins":
                # sticking to the scheme above, now it
                # contains the path to a pickle list of bins
                maskbins_file = cut
                with open(maskbins_file) as hdl:
                    import cPickle as pickle
                    bins_to_mask = pickle.load(hdl)["list_of_bins_to_mask"]
                    mask = n.isin(self.flattened_indices, bins_to_mask)
                    if var_key=="notin":
                        mask = ~mask
            else:
                logger.debug("Unknown operator used for cut. Please implement it")
                raise NotImplemented
            return mask

        mask = n.ones(self.n_events, dtype=n.bool)
        for condition in conditions:
            mask &= mask_from_cond(condition)
        logger.debug("After applying additional cuts, %d events", mask.sum())
        #self.set_mask(mask)
        return mask

    def sort_ptype(self):
        """
        Sort all keys by true_ptype in place
        """

        raise NotImplementedError("Sorting by ptype is deprecated")

    def flux_is_cached(self):
        """If self has no Caching object, return (False, None). Otherwise
        return (caching_status, ident)"""

        if self._cache is None:
            return False, None
        ident = self._cache.generate_ident(self.oversampled_ra)
        if self._cache.is_cached(ident):
            return True, ident
        else:
            return False, ident

    def load_cached_coords(self, ident):
        """Load cached coords for ident"""
        with self._cache.load_from_cache(ident) as hdl:
            cached = n.load(hdl)
            self._oversampled_lon = cached["lon"]
            self._oversampled_lat = cached["lat"]

    def cache_coords(self, ident):
        """Cache flux flux for ident"""
        with self._cache.save_to_cache(ident) as hdl:
            n.savez_compressed(hdl, lon=self.true_lon,
                               lat=self.true_lat)

    @staticmethod
    def calc_overs_gal_coords(dec, ra):  # pylint: disable=invalid-name

        """Calculate galactic latitude and longitude from oversampled mjd,
        zenith and ra"""
        from icecube.astro import I3Equatorial, I3GetGalacticFromEquatorial

        def equa_to_gal(ra, dec):
            ra = n.float64(ra)
            dec = n.float64(dec)
            eq = I3Equatorial(ra, dec)
            gal = I3GetGalacticFromEquatorial(eq)
            return gal.l, gal.b
        equa_to_gal = n.vectorize(equa_to_gal, otypes=[n.float32, n.float32])
        lat, lon = equa_to_gal(ra, dec)
        return lat, lon

    def make_oversampled_ra(self, n_over):
        """Calculate oversampled RA and update gal. coordinates"""
        ovrs_ra_ar = n.empty((n_over, self.n_events), dtype=n.float32)
        logger.debug("Size of oversample ra in memory: %f MB",
                     ovrs_ra_ar.nbytes/(1024**2))
        ovrs_ra_ar[:] = self._random_state.uniform(
            0, 2*n.pi, (n_over, self.n_events))
        self._oversampled_ra = ovrs_ra_ar
        is_cached, ident = self.flux_is_cached()
        if is_cached:
            logger.debug("Found cached coords with ident %s", ident)
            self.load_cached_coords(ident)
        else:
            logger.debug("Calculating oversampled gal. coordinates")

            lon, lat = Dataset.calc_overs_gal_coords(self.true_dec,
                                                     self.oversampled_ra)
            self._oversampled_lon = lon
            self._oversampled_lat = lat

            if ident is not None:
                logger.debug("Caching coords with ident %s", ident)
                self.cache_coords(ident)

    @property
    def analysis_vars(self):
        """
        Return list of analysis variables
        """
        return map(lambda var: getattr(self, var),
                   self.analysis_vars_keys)

    @property
    def true_energy(self):
        """
        Return true energy
        """
        return self._read_key("true_energy")

    @property
    def true_ptype(self):
        """
        Return true particle
        """
        return self._read_key("true_ptype")

    @property
    def true_dec(self):
        """
        Return true declination
        """
        return self._read_key("true_dec")

    @property
    def true_ra(self):
        """
        Return true right-ascension
        """
        return self._read_key("true_dec")

    @property
    def true_lat(self):
        """
        Return true latitude
        """
        if self.oversampling:
            return self._oversampled_lat
        return self._read_key("true_lat")

    @property
    def true_lon(self):
        """
        Return true longitude
        """
        if self.oversampling:
            return self._oversampled_lon
        return self._read_key("true_lon")

    @property
    def true_zenith(self):
        """
        Return true zenith
        """
        return self._read_key("true_zenith")

    @property
    def true_azimuth(self):
        """
        Return true azimuth
        """
        return self._read_key("true_azimuth")

    @property
    def reco_energy(self):
        """
        Return reco energy
        """
        return self._read_key("reco_energy")

    @property
    def reco_zenith(self):
        """
        Return reco zenith
        """
        return self._read_key("reco_zenith")

    @property
    def reco_lat(self):
        """
        Return reco lat
        """
        return self._read_key("reco_lat")

    @property
    def reco_lon(self):
        """
        Return reco lon
        """
        return self._read_key("reco_lon")

    @property
    def reco_ra(self):
        """
        Return reco ra
        """
        return self._read_key("reco_ra")

    @property
    def oversampled_lat(self):
        """
        Return oversampled lat
        """
        return self._oversampled_lat

    @property
    def oversampled_lon(self):
        """
        Return oversampled lon
        """
        return self._oversampled_lon
    @property
    def oversampled_ra(self):
        """
        Return oversampled ra
        """
        return self._oversampled_ra
