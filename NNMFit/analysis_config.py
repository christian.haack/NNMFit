"""
Implementation of AnalysisConfig class
"""
import os
import string
import re
import logging
from collections import defaultdict
import configparser
import numpy as n
from . import fluxes
import yaml

logger = logging.getLogger(__name__)

class ConfigurationError(Exception):
    """
    Exception for errors in the configuration
    """
    pass


class RawDatasetConfig(object):
    """
    Class holding the raw dataset config
    """

    def __init__(self):
        self._config = configparser.ConfigParser(
            interpolation=configparser.ExtendedInterpolation())
        self._config.optionxform = lambda option: option

        config_dir = AnalysisConfig._config_dir()
        cfile = os.path.join(config_dir, "datasets.cfg")
        self._config.read_file(open(cfile))

    def datasets(self, det_config):
        """
        Return configured datasets for detector config `det_config`
        """
        return sorted(AnalysisConfig.parse_list(
            self._config[det_config]["datasets"]))

    def hdf_vars(self, det_config):
        """
        Return hdf_vars for detector config `det_config`
        """
        return dict(
            self._config[self._config[det_config]["reco_variables"]])

    def hdf_vars_mc(self, det_config):
        """
        Return MC hdf_vars for detector config `det_config`
        """
        return dict(
            self._config[self._config[det_config]["mc_variables"]])

    def get_feature_dicts(self, det_config):
        """
        Return feature and mc_feature dicts for config `det_config`
        """
        hdf_vars = self.hdf_vars(det_config)
        hdf_vars_mc = self.hdf_vars_mc(det_config)
        feature_dict = {key:(val, None)
                        for key, val in hdf_vars.iteritems()}
        feature_dict_mc = {key:(val, None)
                           for key, val in hdf_vars_mc.iteritems()}
        return feature_dict, feature_dict_mc

    def __getitem__(self, key):
        return self._config[key]

    def idents(self, det_config):
        """
        Return all identifiers for config `det_config`
        """

        return set([ident for dataset in self.datasets(det_config)  for ident in
                AnalysisConfig.parse_list(self[dataset]["identifier"])])

class AnalysisConfig(object):
    """
    Class holding the configuration
    """
    def __init__(self, configs,
                 detector_configs,
                 override_configs=None,
                 override=None,
                 input_config=None):
        config_dir = self._config_dir()
        self._config = configparser.ConfigParser(
            interpolation=configparser.ExtendedInterpolation())
        self._config.optionxform = lambda option: option

        self.detector_configs = detector_configs 
        if not isinstance(configs, list):
            configs = [configs]
        for config in configs:
            logger.debug("Loading config: %s", config)
            cfile = os.path.join(config_dir, config)
            self._config.read_file(open(cfile))
        """
        We want to apply the dict override before loading the detector
        configs, because the dict override might set different detector
        configs to load. 
        """
        if override is not None:
            logger.debug("Overriding config with: %s", override)
            self._config.read_dict(override)
        self._loaded_det_configs = []
        self.components = {}
        if override_configs is not None:
            logger.debug("Overriding config with: %s", override_configs)
            for config in override_configs:
                logger.debug("Loading config: %s", config)
                cfile = os.path.join(config_dir, config)
                self._config.read_file(open(cfile))
        self._load_detector_configs(config_dir, detector_configs)
        self._load_component_configs(config_dir)
        self._load_detector_systematics(config_dir)
        self._alignment_bool = self._config.getboolean("main", "use_alignment")

        """
        Ugly hack: the override_configs are applied afterwards again to make
        sure that changes to the det_configs etc take effect, too!
        TODO: Make this more elegant...
        """ 
        if override is not None:
            logger.debug("Overriding config with: %s", override)
            self._config.read_dict(override)
        if override_configs is not None:
            logger.debug("Overriding config with: %s", override_configs)
            for config in override_configs:
                logger.debug("Loading config: %s", config)
                cfile = os.path.join(config_dir, config)
                self._config.read_file(open(cfile))


    def __getitem__(self, key):
        return self._config[key]

    def _load_component_configs(self, config_dir):
        """Load the configs for all components configured in `components` in
        section main"""
        components = self.parse_list(self["main"]["components"])
        comp_settings = yaml.load(open(os.path.join(config_dir, "components.yaml")))
        param_settings = yaml.load(open(os.path.join(config_dir, "parameters.yaml")))
        for comp in components:
            logger.debug("Reading config for %s", comp)
            self.components[comp] = comp_settings[comp]
            parameters = comp_settings[comp]["parameters"]
            self.components[comp]["parameters"] = {}
            for param in parameters:
                self.components[comp]["parameters"][param] = param_settings[param]

    def _load_detector_configs(self, config_dir, det_configs):
        """Load the config files for the detector configs specified in section
        `detector_configs`"""
        for det_config in det_configs:
            logger.debug("Reading config for %s", det_config)
            if ".cfg" not in det_config:
                cfile = os.path.join(config_dir, det_config+".cfg")
            else:
                cfile = os.path.join(config_dir, det_config)
            self._config.read_file(open(cfile))
            self._loaded_det_configs.append(det_config)

    def _load_detector_systematics(self, config_dir):
        """Load the detector systematics file"""
        cfile = os.path.join(config_dir,
                             self["main"]["systematics_config"]+".cfg")
        logger.debug("Reading det. sys. config: %s", cfile)
        self._config.read_file(open(cfile))

    @staticmethod
    def _config_dir():
        return os.path.join(
            os.path.dirname(
                os.path.abspath(__file__)),
            "..", "resources", "configs")

    @staticmethod
    def parse_list(cs_string):
        """Static helper to split a comma-separated list"""
        split = cs_string.split(",")
        split = [sp for sp in split if sp != ""]
        return map(string.strip, split)

    def get_additional_settings(self, section):
        """
        Check if sections contains `additional_settings` and return the
        so specified section
        """
        if "additional_settings" in self[section]:
            key = self[section]["additional_settings"]
            return dict(self[key])
        else:
            return {}

    def get_fluxmodels(self):
        """
        Return fluxmodels defined in `main`
        """
        return self.parse_list(self["main"]["fluxmodels"])

    def get_key_mapping(self, det_config):
        """
        Return key mapping for data and mc

        Args:
            det_config: detector config

        The key mapping is required for the Datahandler
        """
        var_mapping_str = self[det_config]["var_mapping"]
        var_mapping_mc_str = self[det_config]["var_mapping_mc"]

        fluxmodels = map(string.strip, self["main"]["fluxmodels"].split(","))
        key_mapping = dict(self[var_mapping_str])

        key_mapping_mc = dict(key_mapping)
        key_mapping_mc.update(dict(self[var_mapping_mc_str]))

        for flux_model in fluxmodels:
            key_mapping_mc[flux_model] = flux_model

        # For DataFrame.rename we need a mapping of old_val: new_val    
        key_mapping = {val: key for key, val in key_mapping.iteritems()}
        key_mapping_mc = {val: key for key, val in key_mapping_mc.iteritems()}

        return key_mapping, key_mapping_mc

    def get_flux_params(self):
        """
        Return parameters and settings

        Returns:
            params, param_seeds, param_bounds, alignment_settings
        """

        param_seeds = {}
        param_bounds = {}
        alignment_settings = {}

        for flux, comp_settings in self.components.iteritems():
            # Get seeds and bounds form param
            for param, param_settings in comp_settings["parameters"].iteritems():
                param_seeds[param] = param_settings["default"]
                param_bounds[param] = param_settings["range"]
                if self._alignment_bool:
                    alignment_settings[param] = param_settings.get("alignment", None)
                    logger.debug(
                        "Found parameter %s with alignment %s for flux %s",
                        param, alignment_settings[param], flux)
        if not self._alignment_bool:
            alignment_settings = {}

        return param_seeds, param_bounds, alignment_settings


    def get_param_priors(self):
        """
        Return parameter priors 

        Args:
            fluxes_list: list of fluxes (components)

        Returns:
            params, priors
        """
        param_priors = {}
        param_prior_widths = {}
        for flux, comp_settings in self.components.iteritems():
            for param, param_settings in comp_settings["parameters"].iteritems():
                if "prior" in param_settings.keys():
                    param_priors[param] = param_settings["prior"]
                    param_prior_widths[param] = param_settings["prior_width"]
                    logger.debug(
                        "Found parameter %s with prior %s for flux %s",
                        param, param_priors[param], flux)

        return [param_priors, param_prior_widths]

    def get_asimov_params(self):
        """
        Return those params which were set in the input_config. Only thos components considered which are enabled in "main"
        Resturns:
            dict of pars : values which were set in input_config
        """
        asimov_params = {}
        for comp in self.get_components():
            self._input_config.keys()
            for par in self._input_config[comp]:
                asimov_params[par] = float(self._input_config[comp][par])
        return asimov_params

    def get_sys_params(self, det_config):
        """
        Return settings for detector systematics parameters
        """
        param_seeds = {}
        param_bounds = {}
        alignment_settings = {}
        sys_params = self.parse_systematics_params(det_config)
        if len(sys_params) > 0:
            for sys_type, s_params in sys_params.iteritems():
                for s_param in s_params:
                    param_config_key = "{}_{}".format(sys_type, s_param)

                    param_seeds[s_param] = float(
                        self[param_config_key]["default"])
                    bounds_str = self[param_config_key]["range"]
                    param_bounds[s_param] = eval(bounds_str)
                    if self._alignment_bool:
                        alignment_settings[s_param] =\
                            self[param_config_key]["alignment"]

        return param_seeds, param_bounds, alignment_settings

    def parse_alignment_settings(self, det_config, alignment_settings):
        """
        Return parsed alignment settings
        """
        alignment_values = {}
        alignment_config_key = self[det_config]["alignment"]
        alignment_pars = self[alignment_config_key].keys()

        for a_par in alignment_pars:
            if a_par not in alignment_settings:
                logger.warning("Alignment configured for param %s but not"
                               "enabled", a_par)
                continue
            align_value = float(self[alignment_config_key][a_par])

            align_config = tuple([
                align_value,
                alignment_settings[a_par]])

            alignment_values[a_par] = align_config
        return alignment_values

    def get_det_configs(self):
        """
        Return loaded det_configs
        """
        return self._loaded_det_configs

    def get_components(self):
        """
        Return list components
        """
        return self.parse_list(self["main"]["components"])

    def get_params_and_bounds(self, det_configs):
        """
        Return parameter classes, seeds, bounds, and alignment values


        Args:
            fluxes_list: List of flux names
            det_configs: List of detector config names

        Return:
            param_seeds
            param_bounds
            alignment_values

        """
        # Get the parameters and settings for the fluxes
        param_seeds, param_bounds, alignment_settings = self.get_flux_params()
        # First get parameters for all fluxes

        all_alignment_values = defaultdict(list)
        alignment_values = defaultdict(dict)
        for det_config in det_configs:
            # Parameters for detector systematics
            sys_params_tuple = self.get_sys_params(det_config)
            param_seeds.update(sys_params_tuple[0])
            param_bounds.update(sys_params_tuple[1])
            alignment_settings.update(sys_params_tuple[2])

            # Alignment
            if self._alignment_bool:
                avals_conf = self.parse_alignment_settings(det_config,
                                                       alignment_settings)
                alignment_values[det_config] = avals_conf
                for a_par, (align_value, _) in avals_conf.iteritems():
                    all_alignment_values[a_par].append(align_value)
        # Loop through all params that should be aligned and correct param
        # bounds and seeds
        for a_par, values in all_alignment_values.iteritems():
            bounds = list(param_bounds[a_par])
            maxval = max(values)
            if bounds[0] is not None:
                if alignment_settings[a_par] == "additive":
                    bounds[0] -= maxval
                else:
                    bounds[0] /= maxval
            if bounds[1] is not None:
                if alignment_settings[a_par] == "additive":
                    bounds[1] -= maxval
                else:
                    bounds[1] /= maxval
            if bounds[1] is not None and param_seeds[a_par] > bounds[1]:
                param_seeds[a_par] -= maxval
                logger.debug(
                    "Parameter seed for %s would be outside of aligned"
                    " bounds, adjusting to %s", a_par, param_seeds[a_par])
            param_bounds[a_par] = tuple(bounds)

        # If alignment is disabled return empty dict
        return param_seeds, param_bounds, alignment_values

    def make_binning(self, det_config):
        """
        Return bin edges and bin centers for detector config
        """
        binning = []
        bin_centers = []

        def parse_binning(binning_str):
            """
            Helper function to group binning for each dim
            """
            regex = re.compile(r"\((.+?),(.+?),(.+?),(.+?)\)")
            res = regex.findall(binning_str)
            return res

        def cosspace(start, stop, num):
            """Helper function to create grid evenly spaced in cos"""
            return n.arccos(n.linspace(start, stop, num))

        binning_str = self[det_config]["analysis_binning"]
        binning_parsed = parse_binning(binning_str)
        n_dims = len(self.parse_list(self["main"]["analysis_variables"]))
        for i in xrange(n_dims):
            binning_dim = binning_parsed[i]
            if "log"==binning_dim[3].strip():
                bin_edges = n.logspace(
                    float(binning_dim[0]),
                    float(binning_dim[1]),
                    int(binning_dim[2]))
            elif "cos"==binning_dim[3].strip():
                bin_edges = cosspace(
                    float(binning_dim[0]),
                    float(binning_dim[1]),
                    int(binning_dim[2]))
            elif "lin"==binning_dim[3].strip():
                bin_edges = n.linspace(
                    float(binning_dim[0]),
                    float(binning_dim[1]),
                    int(binning_dim[2]))
            elif "gap-log"==binning_dim[3].strip():
                def part_bins(binning_str):
                    low, high, nbins = binning_str.split("-")
                    bin_edges_part = n.logspace(float(low),
                                                float(high),
                                                int(nbins))
                    return list(bin_edges_part)
                bin_edges = []
                for binning_str in [binning_dim[0], binning_dim[1]]:
                    bin_edges += part_bins(binning_str)
                bin_edges = n.array(bin_edges)
            else:
                raise NotImplementedError("Binning scheme not known")

            bin_edges = n.sort(bin_edges)
            centers = 0.5*(bin_edges[1:]+bin_edges[:-1])
            binning.append(bin_edges)
            bin_centers.append(centers)
        logger.debug("Histogram has %d dimensions ", len(binning))
        return binning, bin_centers

    def get_datasets_for_sys(self, det_config, sys_type):
        """
        Return list of dataset identifiers and corresponding param. values
        for detector config and systematic type
        """
        sys_type_key = self[det_config][sys_type]
        sys_datasets = self.parse_list(self[sys_type_key]["datasets"])
        # TODO: Get rid of eval..
        sys_values = n.asarray(eval(self[sys_type_key]["par_values"]))
        return sys_datasets, sys_values

    def parse_systematics_params(self, det_config):
        """
        Return dict of parameter names for every systematic
        """
        sys_key = self[det_config]["systematics"]
        sys_types = self.parse_list(self[sys_key]["systematics"])

        parameters = {}
        for sys_type in sys_types:
            if "parameters" in self[sys_type]:
                pars = self[sys_type]["parameters"]
                parameters[sys_type] = self.parse_list(pars)
        return parameters

    def parse_params_for_flux(self, flux_str):
        """
        Return dict of tuple(paramater classes, settings) for flux
        """
        par_dict = {}
        comp_set = self.components[flux_str]
        specifications = comp_set.get("param_specifications", {})
        for par, par_settings in comp_set["parameters"].iteritems():
            pclass = getattr(fluxes.parameters, par_settings["class"])
            additional = {}
            additional.update(par_settings.get("additional", {}))
            additional.update(specifications.get(par, {}))
            par_dict[par] = (pclass, additional)
        return par_dict

    def get_plot_folder(self):
        """
        Create and return plotting folder
        """
        if not os.path.exists(self["analysis"]["plot_dir"]):
            os.makedirs(self["analysis"]["plot_dir"])
        return self["analysis"]["plot_dir"]
