"""Implementation of systematics class"""
from collections import defaultdict
import logging
from theano import shared
logger = logging.getLogger(__name__)

class SystematicsBase(object):
    """
    Base class for systematics
    """

    def __init__(self):
        self.used_vars = []
        self.input_variables = []

class SystematicsHandler(object):
    """
    Class for managing DetectorSystematics for multiple
    detector configurations
    """
    def __init__(self):
        self._input_var_table = {}
        self._systematics = {}

    def get_all_input_vars(self):
        """
        Returns a dict of all input variables
        """
        res = {arg.name: arg for args in
               self._input_var_table.values() for arg in args}
        return res

    def get_list_of_req_vars(self, det_conf, flux):
        """Return list of required variables for all fluxes"""
        if det_conf in self._systematics:
            return self._systematics[det_conf].get_list_of_req_vars(flux)
        else:
            return []

    def make_input_variables(self, sys_name, sys_class):
        """
        Create theano scalars for systematics object if
        not already done

        Args:
            sys_name: Systematics name
            sys_class: Systematics class
        """
        if sys_name not in self._input_var_table:
            in_vars = [shared(0., name=in_var) for in_var in
                       sys_class.INPUT_VARIABLES]
            self._input_var_table[sys_name] = in_vars

    def add_systematic(self,
                       det_conf,
                       flux,
                       sys_name,
                       sys_class,
                       alignment):
        """
        Add detector systematic

        Args:
            det_conf: Detector config
            flux: Name of component
            sys_name: Name of systematic
            alignment (dict or None): Alignment settings
        """
        logger.debug("Adding systematic %s for flux %s and det-conf %s",
                     sys_name,
                     flux,
                     det_conf)
        if det_conf not in self._systematics:
            det_sys = DetectorSystematics()
            self._systematics[det_conf] = det_sys
        det_sys = self._systematics[det_conf]
        in_var_dict = {}
        if hasattr(sys_class, "IS_SYS_PARAM") and sys_class.IS_SYS_PARAM:
            self.make_input_variables(sys_name, sys_class)
            in_vars = self._input_var_table[sys_name]
            in_var_dict = {var.name: var for var in in_vars}
            if alignment is not None:
                for in_var_name in in_var_dict:
                    if in_var_name in alignment:
                        in_var = in_var_dict[in_var_name]
                        logger.debug("Aligning %s for config %s",
                                     in_var_name, det_conf)
                        if alignment[in_var_name][1] == "additive":
                            in_var += alignment[in_var_name][0]
                        elif alignment[in_var_name][1] == "multiplicative":
                            in_var *= alignment[in_var_name][0]
                        else:
                            raise NotImplementedError
                        in_var_dict[in_var_name] = in_var
        det_sys.add_systematic(sys_name, in_var_dict)

    def add_systematic_object(self, det_conf, flux, sys_name, sys_obj):
        logger.debug("Adding systematics-object %s for flux %s and det-conf %s",
                     sys_name,
                     flux,
                     det_conf)
        self._systematics[det_conf].add_systematic_object(sys_name,
                                                          sys_obj,
                                                          flux)

    def make_graph(self, flux, det_conf, req_vars):
        """Return theano graph"""
        if len(self._systematics) == 0:
            logger.warn("No systematics added to handler, returning 1")
            return 1.
        else:
            return self._systematics[det_conf].make_graph(flux, req_vars)

class DetectorSystematics(object):
    """
    Class for managing detector systematics for a single
    detector config
    """

    def __init__(self):
        self._systematics = defaultdict(dict)
        self._input_var_table = {}

    def add_systematic(self,
                       sys_name,
                       input_variables):
        """Add systematic object"""
        self._input_var_table[sys_name] = input_variables

    def add_systematic_object(self, sys_name, sys_obj, flux):
        self._systematics[flux][sys_name] = sys_obj

    def get_input_vars(self):
        """Return list of input variables for all configured parameters"""
        input_vars = []
        for _, input_var in self._input_var_table.iteritems():
            input_vars += input_var.values()
        return input_vars
    """
    def get_list_of_req_vars(self, flux):
        req_vars = set()
        for sys_name, sys_obj in self._systematics[flux].iteritems():
            req_vars |= set(sys_obj.used_vars)
        return req_vars
    """
    def make_graph(self, flux, req_vars):
        """Make theano graph"""
        graph = 1.
        # in_vars = {}
        for sys_name, sys_obj in self._systematics[flux].iteritems():
            in_vars = dict(self._input_var_table[sys_name])
            in_vars["variables"] = req_vars
            logger.debug("Adding systematic {} to graph".format(sys_name))
            graph *= sys_obj.make_graph(**in_vars)
        return graph
