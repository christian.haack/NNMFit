from theano.tensor import dscalar, dvector, dmatrix
import theano.tensor as T
from theano import function, shared
import numpy as n


class lin_interp(object):
    """
    Linear interpolation with chi2 fit
    """

    def __init__(self):
        self.par_T = None

    def fit(self, x_T, y_T, y_err_T):
        """
        Linear chi^2 fit to log10(data)

        Args:
            x_T: x values for all data rows. Shape: (1,M)
            y_T: y values. Shape: (N,M)
            y_err_T: yerror values. Shape (N,M)
        """

        def function_graph(x_T, y_T, y_err_T):
            y_T = T.log10(y_T)
            y_err_T = y_err_T / (y_T*T.log(10))
            yerr_sq = y_err_T**2
            Sy  = T.sum(y_T/yerr_sq, axis=1)
            Sx  = T.sum(x_T/yerr_sq, axis=1)
            Sxy = T.dot(x_T,(y_T/yerr_sq).transpose())
            Sxx = T.sum(x_T**2/yerr_sq, axis=1)
            S   = T.sum(1./yerr_sq, axis=1)

            denominator = Sxx*S - Sx*Sx
            slopes = (Sxy*S - Sy*Sx)/denominator
            interceptions = (Sy*Sxx - Sx*Sxy)/denominator
            return slopes, interceptions

        func = function([], function_graph(x_T, y_T, y_err_T))
        slopes_eval, intercep_eval = func()
        self.par_T = shared(slopes_eval, borrow=True), \
                     shared(intercep_eval, borrow=True) \

    def eval(self, val_T):
        return 10**self.log_eval(val_T)

    def log_eval(self, val_T):
        return self.par_T[0]*val_T + self.par_T[1]

class quad_interp(object):
    """
    Quadratic interpolation with chi2 fit
    """

    def __init__(self):
        self.par_T = None

    def fit(self, x_T, y_T, y_err_T):
        """
        Quadratic chi^2 fit to log10(data)

        Args:
            x_T: x values for all data rows. Shape: (1,M)
            y_T: y values. Shape: (N,M)
            y_err_T: yerror values. Shape (N,M)
        """

        def function_graph(x_T, y_T, y_err_T):
            y_T = T.log10(y_T)
            y_err_T = y_err_T / (y_T*T.log(10))
            yerr_sq = y_err_T**2

            Sy  = T.sum(y_T/yerr_sq, axis=1)
            Sx  = T.sum(x_T/yerr_sq, axis=1)
            Sxy = T.dot(x_T,(y_T/yerr_sq).transpose())
            Sxx = T.sum(x_T**2/yerr_sq, axis=1)
            Sxxx = T.sum(x_T**3/yerr_sq, axis=1)
            Sxxxx = T.sum(x_T**4/yerr_sq, axis=1)
            Sxxy = T.dot(x_T**2,(y_T/yerr_sq).transpose())
            S   = T.sum(1./yerr_sq, axis=1)

            denominator=S*Sxx*Sxxxx-S*Sxxx**2-Sx**2*Sxxxx+2*Sx*Sxx*Sxxx-Sxx**3
            a=(S*Sxx*Sxxy-S*Sxxx*Sxy-Sx**2*Sxxy+Sx*Sxx*Sxy+Sx*Sxxx*Sy-Sxx**2*Sy)/denominator
            b=-(S*Sxxx*Sxxy-S*Sxxxx*Sxy-Sx*Sxx*Sxxy+Sx*Sxxxx*Sy+Sxx**2*Sxy-Sxx*Sxxx*Sy)/denominator
            c=(Sx*Sxxx*Sxxy-Sx*Sxxxx*Sxy-Sxx**2*Sxxy+Sxx*Sxxx*Sxy+Sxx*Sxxxx*Sy-Sxxx**2*Sy)/denominator

            '''
            zero_ind = n.argwhere(denominator.eval()!=0.)[0:10]
            print y_T.eval()[zero_ind,:]
            print y_err_T.eval()[zero_ind,:]
            print "#########"
            zero_ind = n.argwhere(denominator.eval()==0.)[0:10]
            print y_T.eval()[zero_ind,:]
            print y_err_T.eval()[zero_ind,:]
            '''
            return a,b,c

        func = function([], function_graph(x_T, y_T, y_err_T))
        a_eval, b_eval, c_eval = func()
        self.par_T = shared(a_eval, borrow=True), \
                     shared(b_eval, borrow=True), \
                     shared(c_eval, borrow=True)

    def eval(self, val_T):
        return 10**self.log_eval(val_T)

    def log_eval(self, val_T):
        return self.par_T[0]*val_T**2 + self.par_T[1]*val_T + self.par_T[2]
