"""Implementation of Holeice P0 Class"""
import theano.tensor as T
from .eventwise_reweighting import lin_interp, quad_interp
from .systematics import SystematicsBase

class IceHoleP0(SystematicsBase):
    """
    Variation of the p0 hole-ice parameter by linear interpolation
    p0 refers to the angsens_unified model by peller
    """
    IS_SYS_PARAM = True
    INPUT_VARIABLES = ["ice_holep0"]

    def __init__(self,
                 holeice_values_T,
                 holeice_rates_T,
                 holeice_errors_T,
                 baseline_value,
                 baseline_rates):
        super(IceHoleP0, self).__init__()

        self._interpolator = lin_interp()
        #self._interpolator = quad_interp()
        self._interpolator.fit(holeice_values_T,
                               holeice_rates_T,
                               holeice_errors_T)

        self._baseline_value = baseline_value
        self._baseline_rate = baseline_rates


    def make_graph(self, **kwargs):
        """Return theano graph"""
        hole_ice = kwargs["ice_holep0"]
        zeros = T.zeros_like(self._baseline_rate)
        deff = self._interpolator.eval(hole_ice) / self._baseline_rate
        return T.switch(T.eq(self._baseline_rate, zeros), zeros, deff)
