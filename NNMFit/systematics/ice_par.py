"""Implementation of ice-par uncertainties"""

import theano.tensor as T
import numpy as n
from .eventwise_reweighting import quad_interp, lin_interp
from .systematics import SystematicsBase
# pylint: disable=too-few-public-methods, too-many-arguments

class IceAbs(SystematicsBase):
    """
    Variation of Ice-Absorption by linear interpolation
    """
    IS_SYS_PARAM = True
    INPUT_VARIABLES = ["ice_abs"]

    def __init__(self,
                 sys_values_T,
                 sys_rates_T,
                 sys_errors_T,
                 baseline_value,
                 baseline_rates):
        super(IceAbs, self).__init__()

        #self._interpolator = quad_interp()
        self._interpolator = lin_interp()
        self._interpolator.fit(sys_values_T,
                               sys_rates_T,
                               sys_errors_T)

        self._baseline_value = baseline_value
        self._baseline_rate = baseline_rates

    def make_graph(self, **kwargs):
        """Return theano graph"""
        dom_eff = kwargs["ice_abs"]
        zeros = T.zeros_like(self._baseline_rate)
        deff = self._interpolator.eval(dom_eff) / self._baseline_rate
        return T.switch(T.eq(self._baseline_rate, zeros), zeros, deff)

class IceScat(SystematicsBase):
    """
    Variation of Ice-Scatter by linear interpolation
    """
    IS_SYS_PARAM = True
    INPUT_VARIABLES = ["ice_scat"]

    def __init__(self,
                 sys_values_T,
                 sys_rates_T,
                 sys_errors_T,
                 baseline_value,
                 baseline_rates):
        super(IceScat, self).__init__()

        self._interpolator = lin_interp()
        #self._interpolator = quad_interp()
        self._interpolator.fit(sys_values_T,
                               sys_rates_T,
                               sys_errors_T)

        self._baseline_value = baseline_value
        self._baseline_rate = baseline_rates

    def make_graph(self, **kwargs):
        """Return theano graph"""
        dom_eff = kwargs["ice_scat"]
        zeros = T.zeros_like(self._baseline_rate)
        deff = self._interpolator.eval(dom_eff) / self._baseline_rate
        return T.switch(T.eq(self._baseline_rate, zeros), zeros, deff)


class IceModel(SystematicsBase):
    """ Interpolation between two ice models"""

    IS_SYS_PARAM = True
    INPUT_VARIABLES = ["lambda_ice"]
    def __init__(self,
                 ice_model_values_t,
                 ice_model_rates_t,
                 ice_model_errors_t,
                 baseline_value,
                 baseline_rates):
        super(IceModel, self).__init__()

        self._ice_interpolator = lin_interp()
        self._ice_interpolator.fit(ice_model_values_t,
                                   ice_model_rates_t,
                                   ice_model_errors_t)

        self._baseline_value = baseline_value
        self._baseline_rate = baseline_rates


    def make_graph(self, **kwargs):
        """Return theano graph"""
        lambda_ice = kwargs["lambda_ice"]
        zeros = T.zeros_like(self._baseline_rate)
        ice_model_rate = self._ice_interpolator.eval(lambda_ice)
        return T.switch(T.eq(self._baseline_rate, zeros),
                        zeros,
                        ice_model_rate / self._baseline_rate)

class IceModel2(IceModel):
    """
    Ugly workaround to allow the IceModel to be used twice with different
    models.
    TODO: Rework handling of sys. parameters to deal with this
    """

    INPUT_VARIABLES = ["lambda_ice2"]

    def __init__(self, *args, **kwargs):
        super(IceModel2, self).__init__(*args, **kwargs)

    def make_graph(self, **kwargs):
        lambda_ice2 = kwargs["lambda_ice2"]
        kwargs["lambda_ice"] = lambda_ice2
        return super(IceModel2, self).make_graph(**kwargs)

class IceParCombinedAbsSca(SystematicsBase):
    """
    Variation of absorption/scattering by event-wise interpolation

    Scattering and absorption are variied simultaneously and are
    interpolated quadratically between -10% and +10%
    """

    IS_SYS_PARAM = True
    INPUT_VARIABLES = ["ice_abs_sca"]

    def __init__(self,
                 ice_par_values_t,
                 ice_par_rates_t,
                 ice_par_errors_t,
                 baseline_value,
                 baseline_rates):
        super(IceParCombinedAbsSca, self).__init__()

        self._abs_sca_interpolator = quad_interp()
        #self._abs_sca_interpolator = lin_interp()
        print ice_par_errors_t
        self._abs_sca_interpolator.fit(ice_par_values_t,
                                       ice_par_rates_t,
                                       ice_par_errors_t)

        self._baseline_value = baseline_value
        self._baseline_rate = baseline_rates

        self._required_vars = []

    def make_graph(self, **kwargs):
        """Return theano graph"""
        ice_abs_sca = kwargs["ice_abs_sca"]
        zeros = T.zeros_like(self._baseline_rate)
        abs_sca_rate = self._abs_sca_interpolator.eval(ice_abs_sca)
        return T.switch(T.eq(self._baseline_rate, zeros),
                        zeros,
                        abs_sca_rate / self._baseline_rate)


class IcePar(SystematicsBase):
    """
    Variation of absorption/scattering by event-wise interpolation

    Scattering is interpolated linearily between baseline and +10%.
    Absorption is interpolated quadratically between -8% and +10%.
    Since point at -8% is a mixed dataset (-8% sca, -8% abs) the effect
    of scattering is divided out by extrapolating scattering to -8%.

    """

    IS_SYS_PARAM = True
    INPUT_VARIABLES = ["ice_abs", "ice_sca"]

    def __init__(self,
                 ice_par_values_T,
                 ice_par_rates_T,
                 ice_par_errors_T,
                 baseline_value,
                 baseline_rates):
        super(IcePar, self).__init__()
        # ice_par_values is a 2D array with (abs/scat)

        self._sca_interpolator = lin_interp()
        sca_mask = [1, 3]
        self._sca_interpolator.fit(ice_par_values_T[sca_mask, 1],
                                   ice_par_rates_T[:, sca_mask],
                                   ice_par_errors_T[:, sca_mask])
        mixed_value = ice_par_values_T[0, 1]
        # Divide the mixed abs/scat dataset rates
        # by scat evaluated at mixed val
        sca_rate_mixed = ice_par_rates_T[:, 0] /\
            self._sca_interpolator.eval(mixed_value)
        sca_rate_mixed *= ice_par_rates_T[:, 1]

        uncorr_abs = T.set_subtensor(ice_par_rates_T[:, 0], sca_rate_mixed)
        uncorr_abs = n.empty((ice_par_rates_T.get_value().shape[0], 3))
        uncorr_abs[:, 0] = sca_rate_mixed.eval()
        uncorr_abs[:, 1:] = ice_par_rates_T[:, [1, 2]].eval()

        #self._abs_interpolator = lin_interp()
        self._abs_interpolator = quad_interp()
        abs_mask = [0, 1, 2]
        self._abs_interpolator.fit(ice_par_values_T[abs_mask, 0],
                                   uncorr_abs,
                                   ice_par_errors_T[:, abs_mask])

        self._baseline_value = baseline_value
        self._baseline_rate = baseline_rates

        self._required_vars = []

    def make_graph(self, **kwargs):
        """Return theano graph"""
        ice_abs = kwargs["ice_abs"]
        ice_sca = kwargs["ice_sca"]
        abs_rate = self._abs_interpolator.log_eval(ice_abs)
        sca_rate = self._sca_interpolator.log_eval(ice_sca)
        zeros = T.zeros_like(self._baseline_rate)
        return T.switch(T.eq(self._baseline_rate, zeros),
                        zeros,
                        10**(abs_rate+sca_rate) / self._baseline_rate**2)
