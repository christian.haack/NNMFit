"""Implementation of DomEff Class"""
import theano.tensor as T
from .eventwise_reweighting import lin_interp, quad_interp
from .systematics import SystematicsBase
import numpy as np

class DomEff(SystematicsBase):
    """
    Variation of DomEff by interpolation
    """
    IS_SYS_PARAM = True
    INPUT_VARIABLES = ["dom_eff"]

    def __init__(self,
                 domeff_values_T,
                 domeff_rates_T,
                 domeff_errors_T,
                 baseline_value,
                 baseline_rates):
        super(DomEff, self).__init__()

        #self._interpolator = lin_interp()
        self._interpolator = quad_interp()
        self._interpolator.fit(domeff_values_T,
                               domeff_rates_T,
                               domeff_errors_T)

        self._baseline_value = baseline_value
        self._baseline_rate = baseline_rates

    def make_graph(self, **kwargs):
        """Return theano graph"""
        dom_eff = kwargs["dom_eff"]
        zeros = T.zeros_like(self._baseline_rate)
        deff = self._interpolator.eval(dom_eff) / self._baseline_rate
        return T.switch(T.eq(self._baseline_rate, zeros), zeros, deff)
