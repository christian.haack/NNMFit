# -*-coding:utf8-*-

import os
from setuptools import setup, find_packages
with open(os.path.join(os.path.dirname(__file__), "README.md")) as of:
    desc = "\n".join(of.readlines())

datadir = os.path.join('resources','configs')
datafiles = [(d, [os.path.join(d,f) for f in files])
                 for d, folders, files in os.walk(datadir)]

print find_packages(".")
setup(name="NNMFit",
      version="0.1",
      description="NNMFit - framework for diffuse analyses",
      long_description=desc,
      author="Christian Haack",
      author_email="haack@physik.rwth-aachen.de",
      download_url="https://git.rwth-aachen.de/christian.haack/NNMFit",
      packages=find_packages("."),
      install_requires=["numpy>=1.9.0",
                        "healpy",
                        "scipy>=0.14",
                        "xxhash",
                        "theano>=0.8",
                        "kde",
                        "pydashi",
                        "configparser",
                        "tabulate",
                        "pandas",
                        "pyyaml"],
      license="MIT",
      dependency_links=
      ["http://icecube:skua@code.icecube.wisc.edu/svn/sandbox/schoenen/kde/releases/V00-01-01/#egg=kde",
       "http://github.com/IceCube-SPNO/dashi/zipball/master#egg=pydashi"],
      data_files=datafiles
     )
