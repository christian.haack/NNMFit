import pytest


@pytest.fixture
def config(monkeypatch):
    from NNMFit import AnalysisConfig
    import os

    thispath = os.path.dirname(os.path.abspath(__file__))
    configs_path = os.path.join(thispath, "resources")

    def mockreturn(_):
        return configs_path

    monkeypatch.setattr(AnalysisConfig, '_config_dir', mockreturn)
    return AnalysisConfig(["default.cfg", "IC86-2011.cfg"])


def test_parse_list(config):

    dummy_list = "a,b,,c,d  ,e:;,"
    expected = ["a", "b", "c", "d", "e:;"]

    assert config._parse_list(dummy_list) == expected


def test_get_fluxmodels(config):
    expected = ["honda2006_gaisserH4a_elbert_v2",
                "honda2006_polygonato_mod_elbert_v2", "powerlaw"]
    assert config.get_fluxmodels() == expected

def test_get_key_mapping(config):

